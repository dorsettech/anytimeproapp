import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TPartyTokens {
  static const stripePublishableKey =
      // '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';
      'pk_live_51LZv0QIub08rxlKr7Ey60pqCLa7UfJxNt1MHadsFPhLvCKhzs1l9eGzqwlb9hqOof5bbYzsHtZPKYRKDE8XOj3VL00ldC0bpTI';
  static const AGORA_AppID = 'c0a9dcc7ffeb4d8fb859ba2e613fa555';
  static const agoraToken = '9ca7441a0f9646b8a243883a158eb21c';

  ///used for Cloud Recording
  static const AgoraAuthenticationKey = "Basic ZTY1ZTU0MDVkYWQ5NDUxMmI1NTNhODBlOThmODRiOGM6NWZlNGVhYmZmMjQyNDQ2Zjk0NGMxMzg1MWE0NjhmOTE=";
  static const AgoraBaseUrl = "https://api.agora.io/v1/apps/$AGORA_AppID/cloud_recording";
  static const AgoraAcquireUrl = "$AgoraBaseUrl/acquire";
  static const AgoraStartStopBaseUrl = "$AgoraBaseUrl/resourceid/";
  static const AgoraStartStopEndUrl = "/mode/mix";
  static const AgoraCloudChannelName = "test";
  static const GoogleCloudStorageAccessKey = "GOOGZWDQ6IS62MJ4YIKO3TB6";
  static const GoogleCloudStorageSecretKey = "Bkzn5dpZ6zFEpLdb5+AJs5TQcM0lhdYzVlk+XT9T";
  static const GoogleCloudStorageBucketName = "anytimepro";
  static const GoogleCloudStorageProjectName="dorsettechhosting";
}

class KColors {
  static const Color lightBlue = Color(0xFF323370);
  static const Color darkBlue = Color(0xFF0e1d3c);
  static const Color starColor = Color(0xFF40e0d0);
  static const Color lightishGrey = Color(0xffCFD8DD);
  static const Color darkishGrey = Color(0xff5F7D8B);
  static const Color DARK_GREY = Color(0xFFAAAAAA);
  static const Color BG = Color(0xFFD6D6D6);
  static const Color DARKEST_NAVY = Color(0xFF273677);
  static const Color PENDING = Color(0xFFF0AD4E);
  static const Color ACCEPT = Color(0xFF5CB85C);
  static const Color DECLINED = Color(0xFFD9534F);
  static Color SHADOW_COLOR = Color(0xFF919EAB).withOpacity(0.3);
}

class KTextStyle {
  static TextStyle f20w5 =
      TextStyle(fontSize: 20.sp, fontWeight: FontWeight.w500);
  static TextStyle f13w5 =
      TextStyle(fontSize: 13.sp, fontWeight: FontWeight.w500);
  static TextStyle f12w4 =
      TextStyle(fontSize: 12.sp, fontWeight: FontWeight.w400);
  static TextStyle f14w4 =
      TextStyle(fontSize: 14.sp, fontWeight: FontWeight.w400);
  static TextStyle f16w5 =
      TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w500);
  static TextStyle f18w6 =
      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w600);
}

class KDynamicWidth {
  static double width20 = Get.width / 17;
  static double width16 = Get.width / 25;
  static double width10 = Get.width / 40;
  static double width50 = Get.width / 9;
}

class KApiTexts {
  static const BASE_URL = 'https://www.anytimepro.io/wp-json/wp/v2/';
  // static const BASE_URL = 'https://anytime.honesttech.co.uk/wp-json/wp/v2/';
  static const LOGIN = 'user-login';
  static const SIGNUP = 'user-register';
  static const profile = 'user-profile';
  static const deleteAccount = 'user-delete';
  static const coachOnboard = 'coach-stripe-onboard';
  static const coachStripeStatus = 'coach_stripe_status';
  static const home = 'home-new';
  static const allSlots = 'all-slots';
  static const forgotPassword = 'forgot-password';
  static const updateProfile = 'update-profile';
  static const savePaymentMethodFuture = 'save-paymentmethod-future';
  static const getSavedCard = 'get-saved-card';
  static const saveCardFuture = 'save-card-future';
  //static const coachList = 'all-coach-list';
  static const coachList = 'all-coach-list-new';
  static const bookLesson = 'book-lesson';
  static const coachLessonList = 'coach-lesson-list-new';
  static const voucherPayment = 'voucher-payment';
  static const lessonAction = 'lesson-action';
  // static const postCoachAvailability = 'coach-availability-new';
  //static const postCoachAvailability = 'set-time-slot';
  static const postCoachAvailability = 'set-time-slot-multi';
  static const addPayments = 'add-payment';
  //static const quickBook = 'quick-book-slot';//
  static const quickBook = 'get-coach-time-slot';
  static const fetchReview = 'get-reviews';
  static const setReview = 'submit-review';
  static const sendPushNotification = 'push-notification';
  static const getAllSlots= 'get-slots';
  static const getVideo= 'video-by-id';
  static const setVideoPlay= 'recording-set-play';
  static const playerVideoStatus= 'recording-status';
  static const userWalkThrough='update-anytime-pro-user-custom-flag';

}

// const slotDataList = [
//   "00:00-01:00",
//   "01:00-02:00",
//   "02:00-03:00",
//   "03:00-04:00",
//   "04:00-05:00",
//   "05:00-06:00",
//   "06:00-07:00",
//   "07:00-08:00",
//   "08:00-09:00",
//   "09:00-10:00",
//   "10:00-11:00",
//   "11:00-12:00",
//   "12:00-13:00",
//   "13:00-14:00",
//   "14:00-15:00",
//   "15:00-16:00",
//   "16:00-17:00",
//   "17:00-18:00",
//   "18:00-19:00",
//   "19:00-20:00",
//   "20:00-21:00",
//   "21:00-22:00",
//   "22:00-23:00",
//   "23:00-24:00",
// ];

const slotDataList = [
  "00:00-00:45",
  "01:00-01:45",
  "02:00-02:45",
  "03:00-03:45",
  "04:00-04:45",
  "05:00-05:45",
  "06:00-06:45",
  "07:00-07:45",
  "08:00-08:45",
  "09:00-09:45",
  "10:00-10:45",
  "11:00-11:45",
  "12:00-12:45",
  "13:00-13:45",
  "14:00-14:45",
  "15:00-15:45",
  "16:00-16:45",
  "17:00-17:45",
  "18:00-18:45",
  "19:00-19:45",
  "20:00-20:45",
  "21:00-21:45",
  "22:00-22:45",
  "23:00-23:45",
];
