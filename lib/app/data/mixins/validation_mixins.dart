import 'package:get/get_utils/src/get_utils/get_utils.dart';

class ValidationMixin {
  String validateReqFields(value) {
    if (GetUtils.isNullOrBlank(value)) {
      return '*Required Field';
    } else {
      return null;
    }
  }

  String validateEmail(value) {
    if (GetUtils.isNullOrBlank(value)) {
      return '*Required Field';
    } else if (!GetUtils.isEmail(value)) {
      return 'Incorrect email';
    } else {
      return null;
    }
  }

  String validatePriceFields(value) {
    if (GetUtils.isNullOrBlank(value)) {
      return '*Price can not be null';
    } else if (value == '0') {
      return '*Price can not be zero';
    } else {
      return null;
    }
  }
}
