import 'package:anytime_pro/app/data/ModelClass/all_slot_model.dart';
import 'package:anytime_pro/app/data/ModelClass/book_lesson_model.dart';
import 'package:anytime_pro/app/data/ModelClass/coach_availabilit_model.dart';
import 'package:anytime_pro/app/data/ModelClass/coach_lesson_list_model.dart';
import 'package:anytime_pro/app/data/ModelClass/coaches_list.dart';
import 'package:anytime_pro/app/data/ModelClass/edit_profile_personal.dart';
import 'package:anytime_pro/app/data/ModelClass/forgot_Pass_Model.dart';
import 'package:anytime_pro/app/data/ModelClass/get_coach_availability_model.dart';
import 'package:anytime_pro/app/data/ModelClass/home_model.dart';
import 'package:anytime_pro/app/data/ModelClass/lesson_action_model.dart';
import 'package:anytime_pro/app/data/ModelClass/lesson_list_model.dart';
import 'package:anytime_pro/app/data/ModelClass/login_model.dart';
import 'package:anytime_pro/app/data/ModelClass/new_get_coach_availability_model.dart';
import 'package:anytime_pro/app/data/ModelClass/new_set_caoch_availability_model.dart';
import 'package:anytime_pro/app/data/ModelClass/payment_model.dart';
import 'package:anytime_pro/app/data/ModelClass/profile_model.dart';
import 'package:anytime_pro/app/data/ModelClass/review_model.dart';
import 'package:anytime_pro/app/data/ModelClass/signup_model.dart';
import 'package:anytime_pro/app/data/ModelClass/slot_model.dart';
import 'package:anytime_pro/app/data/ModelClass/video_folder_list_model.dart';
import 'package:anytime_pro/app/data/Provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:anytime_pro/app/data/ModelClass/login_model.dart' as meta;

import '../ModelClass/save_card.dart';


class Repository {
  static ApiProvider provider = ApiProvider();
  Future<String> loadVideoAPI(String filePath) => provider.loadRecordedVideo(filePath);

  Future<LoginModel> loginAPI({String loginEmail, String loginPass}) =>
      provider.loginPost(loginEmail, loginPass);

  Future<ForgotPassModel> forgotPassApi({String email}) =>
      provider.forgotPass(email);

  Future<ProfileModel> getUserProfile({int id}) => provider.getProfile(id);

  Future<dynamic> deleteAccount({int id}) => provider.deleteAccount(id);

  Future<HomeModel> fetchHome(String token) => provider.home(token);

  Future<dynamic> coachOnboardToStripe(int id) => provider.coachOnBoardToStripe(id);

  Future<dynamic> coachStripeStatus(int id) => provider.coachStripeStatus(id);

  Future<SignupModel> signUpAPI(
          {String email, String fullname, String role, String pass}) =>
      provider.signUpPost(email, fullname, role, pass);

  Future<EditProfilePersonal> editProfile(
          {int id,
          String name,
          String lastName,
          String gender,
          String nickname,
          String age,
          String country,
          String town,
          String coachType,
          String dp, about,price}) =>
      provider.updateProfile(id, name, lastName, gender, nickname, age, country,
          town, coachType, dp,about,price);

  Future<EditProfilePersonal> editGolfingDetails({
    int id,
    String handicap,
    String homeDrivingRange,
    String homeGolfHouse,
    String golfing,
    String favoriteCourse,
    String golfingSpecialisms,
    String testimonial,
  }) =>
      provider.updateGolfing(id, handicap, homeDrivingRange, homeGolfHouse,
          golfing, favoriteCourse, golfingSpecialisms, testimonial);

  Future<EditProfilePersonal> editPaymentInfo({
    @required int id,
    String use_name_account,
    @required String name_on_card,
    @required String card_number,
    @required String ex_month,
    @required String ex_year,
    bool rememberCardDetail,
  }) =>
      provider.updatePaymentInfo(
          id, use_name_account, name_on_card, card_number, ex_month, ex_year, rememberCardDetail);

  Future<CoachesListModel> fetchCoachesList(
          {String date, String slot, String day}) =>
      provider.fetchAllCoachList(date, slot, day);

  Future<BookLessonModel> bookLessonCoach(
          {int coachID,
          String date,
          String slot,
          int playerID,
          String amount}) =>
      provider.bookCoach(coachID, date, slot, playerID, amount);

  Future<List<ReviewModel>> fetchReview(int CoachId) =>
      provider.fetchReview(CoachId);

  Future<String> setReview(int PlayerId,int CoachId,String reviewData, String rating) =>
      provider.setReview(PlayerId,CoachId,reviewData,rating);

  Future<LessonListModel> playerLessonList({String date, int id}) =>
      provider.lessonList(date, id);

  Future<CoachLessonListModel> coachLessonList({String date, int id}) =>
      provider.coachLessonList(date, id);

  // Future<GetCoachAvailabilityModel> fetchCoachAvailability({int id}) =>
  //     provider.fetchCoachAvailability(id);

  Future<List<Slot>> fetchCoachAvailable({int id}) =>
      provider.fetchCoachAvailable(id);

  Future<LessonActionModel> lessonAction({int lessonId, String action,String playerId}) =>
      provider.lessonAction(lessonId, action,playerId);

  Future<CoachAvailabilityModel> coachPostAvailability(
          {var dateList, var slotList, int coachId}) =>
      provider.postCoachAvailability(coachId, dateList, slotList);

  Future<bool> setCoachAvailability(NewSetCoachAvailabilityModel setSlotDataModel) =>
      provider.postSetCoachAvailability(setSlotDataModel);

  Future<PaymentModel> addPayment(int id, String playerEmail, int amount, int uID,String coachId) =>
      provider.addPayment(id, playerEmail, amount, uID,coachId);

  Future<PaymentModel> addPaymentWithVoucher(String voucher, int amount, int uID, int id, String coachId) =>
      provider.addPaymentWithVoucher(voucher, amount, uID, id, coachId);

  Future<List<AllSlotModel>> quickBookSLOTS({String date, int coachId}) =>
      provider.quickBookSLOTList(date, coachId);

  Future<List<AllSlotModel>> allAvailableSLOTS({String date}) =>
      provider.allAvailableSLOT(date);

  Future<String> storeScreenRecordingCgp(String base64string, String playerId) =>
      provider.storeScreenOnGcp(base64string,playerId);

  Future<List<VideoFolderListModel>> fetchVideoList()=>
      provider.fetchVideoList();

  Future<List<VideoFolderListModel>> fetchVideosRelatedToCoachAndPlayerList(String agoraPlayerId)=>
      provider.fetchVideosRelatedToCoachAndPlayerList(agoraPlayerId);

  Future<String>fetchVideoUrl(String filePath)=>
      provider.getVideoUrl(filePath);

  Future<String>getLatestVideoFileName(int playerId)=>
      provider.getLatestVideoFileName(playerId);

  Future<String>getVideoByID(int videoID, int coachID)=>
      provider.getVideoByID(videoID, coachID);

  Future<void> setVideoPlay(bool videoPlayCheck, String videoId, bool isEnd )=>
      provider.setPlayerVideoPlay(videoPlayCheck,videoId,isEnd);

  Future<String> fetchVideoStatus(String videoId )=>
      provider.getPlayerVideoStatus(videoId);

  Future<bool> updateLessonTakenCount(Map<String, dynamic> queryP)=>
      provider.updateLessonTakenCount(queryP);

  Future<void> updateTourViewFlag()=>
      provider.updateWalkThroughFlag();

  Future<meta.MetaData> getLessonCount(Map<String, String> queryP) => provider.getLessonCount(queryP);

  Future<SaveCard> getSaveCard(int id) => provider.getSaveCard(id);

  Future<dynamic> cancelLesson(int id) => provider.cancelLesson(id);
}
