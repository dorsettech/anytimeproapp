import 'package:socket_io_client/socket_io_client.dart' as IO;


class SocketIOUser {
  static final SocketIOUser _socket = SocketIOUser._internal();
  // static IO.Socket socket = IO.io('wss://fluffynode.cloud:3000', <String, dynamic>{
  static IO.Socket socket = IO.io('wss://video.anytimepro.io:3000', <String, dynamic>{
    'pingInterval': 25000,
    'pingTimeout': 20000,
    'transports': ['websocket', 'polling'],
  });
  factory SocketIOUser() {
    return _socket;
  }

  SocketIOUser._internal();


}