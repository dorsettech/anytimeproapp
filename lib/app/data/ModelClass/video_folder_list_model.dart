// To parse this JSON data, do
//
//     final videoFolderListModel = videoFolderListModelFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

List<VideoFolderListModel> videoFolderListModelFromJson(String str) => List<VideoFolderListModel>.from(json.decode(str).map((x) => VideoFolderListModel.fromJson(x)));

String videoFolderListModelToJson(List<VideoFolderListModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VideoFolderListModel {
  VideoFolderListModel({
    this.coachName,
    this.playerName,
    this.fileName,
    this.date,
    this.file,
    this.fileId
  });

  String coachName;
  String playerName;
  String fileName;
  String file;
  String fileId;
  String date;

  factory VideoFolderListModel.fromJson(Map<String, dynamic> json) => VideoFolderListModel(
    coachName: json["coach_name"],
    playerName: json["player_name"],
    fileName: json["file_name"],
    file: json["filename"],
    fileId: json["id"],
    date: json["date"],
  );

  Map<String, dynamic> toJson() => {
    "coach_name": coachName,
    "player_name": playerName,
    "file_name": fileName,
    "date": date,
    "filename": file,
    "id": fileId,
  };
}

class RecordedVideos {
  File url;
  String id;
  bool isUploaded = false;
  RecordedVideos({this.url, this.id, this.isUploaded});
}
