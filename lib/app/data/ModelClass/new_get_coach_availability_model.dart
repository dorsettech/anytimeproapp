// To parse this JSON data, do
//
//     final newGetCoachAvailabilityModel = newGetCoachAvailabilityModelFromJson(jsonString);

import 'dart:convert';

List<NewGetCoachAvailabilityModel> newGetCoachAvailabilityModelFromJson(String str) => List<NewGetCoachAvailabilityModel>.from(json.decode(str).map((x) => NewGetCoachAvailabilityModel.fromJson(x)));

String newGetCoachAvailabilityModelToJson(List<NewGetCoachAvailabilityModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class NewGetCoachAvailabilityModel {
  NewGetCoachAvailabilityModel({
    this.date,
    this.timeSlots,
  });

  String date;
  List<String> timeSlots;

  factory NewGetCoachAvailabilityModel.fromJson(Map<String, dynamic> json) => NewGetCoachAvailabilityModel(
    date: json["date"],
    timeSlots: List<String>.from(json["time_slots"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "date": date,
    "time_slots": List<dynamic>.from(timeSlots.map((x) => x)),
  };
}
