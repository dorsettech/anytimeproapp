/// status : "success"
/// message : "Payment information updated."

class EditProfilePersonal {
  EditProfilePersonal({
    this.status,
    this.message,
  });

  EditProfilePersonal.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
  }
  String status;
  String message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    return map;
  }
}
