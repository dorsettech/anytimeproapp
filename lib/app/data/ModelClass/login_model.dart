class LoginModel {
  String status;
  String message;
  Data data;
  String cookie;

  LoginModel({this.status, this.message, this.data, this.cookie});

  LoginModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int iD;
  String token;
  String accountVerifyStatus;
  String role;
  String userLogin;
  String userPass;
  String userNicename;
  String userEmail;
  String userUrl;
  String userRegistered;
  String userActivationKey;
  String userStatus;
  String displayName;
  Caps caps;
  String capKey;
  List<String> roles;
  Allcaps allcaps;
  dynamic filter;
  MetaData metaData;

  Data(
      {this.iD,
      this.token,
      this.accountVerifyStatus,
      this.role,
      this.userLogin,
      this.userPass,
      this.userNicename,
      this.userEmail,
      this.userUrl,
      this.userRegistered,
      this.userActivationKey,
      this.userStatus,
      this.displayName,
      this.caps,
      this.capKey,
      this.roles,
      this.allcaps,
      this.filter,
      this.metaData});

  Data.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    token = json['token'];
    accountVerifyStatus = json['account_verify_status'];
    role = json['role'];
    userLogin = json['user_login'];
    userPass = json['user_pass'];
    userNicename = json['user_nicename'];
    userEmail = json['user_email'];
    userUrl = json['user_url'];
    userRegistered = json['user_registered'];
    userActivationKey = json['user_activation_key'];
    userStatus = json['user_status'];
    displayName = json['display_name'];
    caps = json['caps'] != null ? Caps.fromJson(json['caps']) : null;
    capKey = json['cap_key'];
    roles = json['roles'].cast<String>();
    allcaps =
        json['allcaps'] != null ? Allcaps.fromJson(json['allcaps']) : null;
    filter = json['filter'];
    metaData =
        json['meta_data'] != null ? MetaData.fromJson(json['meta_data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = this.iD;
    data['token'] = this.token;
    data['account_verify_status'] = this.accountVerifyStatus;
    data['role'] = this.role;
    data['user_login'] = this.userLogin;
    data['user_pass'] = this.userPass;
    data['user_nicename'] = this.userNicename;
    data['user_email'] = this.userEmail;
    data['user_url'] = this.userUrl;
    data['user_registered'] = this.userRegistered;
    data['user_activation_key'] = this.userActivationKey;
    data['user_status'] = this.userStatus;
    data['display_name'] = this.displayName;
    if (this.caps != null) {
      data['caps'] = this.caps.toJson();
    }
    data['cap_key'] = this.capKey;
    data['roles'] = this.roles;
    if (this.allcaps != null) {
      data['allcaps'] = this.allcaps.toJson();
    }
    data['filter'] = this.filter;
    if (this.metaData != null) {
      data['meta_data'] = this.metaData.toJson();
    }
    return data;
  }
}

class Caps {
  bool coach;

  Caps({this.coach});

  Caps.fromJson(Map<String, dynamic> json) {
    coach = json['coach'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['coach'] = this.coach;
    return data;
  }
}

class Allcaps {
  bool read;
  bool level0;
  bool coach;

  Allcaps({this.read, this.level0, this.coach});

  Allcaps.fromJson(Map<String, dynamic> json) {
    read = json['read'];
    level0 = json['level_0'];
    coach = json['coach'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['read'] = this.read;
    data['level_0'] = this.level0;
    data['coach'] = this.coach;
    return data;
  }
}

class MetaData {
  String nickname;
  String firstName;
  String lastName;
  String description;
  String richEditing;
  String syntaxHighlighting;
  String commentShortcuts;
  String adminColor;
  String useSsl;
  String showAdminBarFront;
  String locale;
  var t96jAqsCapabilities;
  String t96jAqsUserLevel;
  String sYoastWpseoProfileUpdated;
  String registerType;
  String userCustomFlag;
  String hasToBeActivated;
  String jwtAuthPass;
  dynamic lessonCount;
  dynamic fcmToken;
  dynamic deviceType;

  MetaData(
      {this.nickname,
      this.firstName,
      this.lastName,
      this.description,
      this.richEditing,
      this.syntaxHighlighting,
      this.commentShortcuts,
      this.adminColor,
      this.useSsl,
      this.showAdminBarFront,
      this.locale,
      this.t96jAqsCapabilities,
      this.t96jAqsUserLevel,
      this.sYoastWpseoProfileUpdated,
      this.registerType,
      this.userCustomFlag,
      this.hasToBeActivated,
      this.jwtAuthPass,
      this.fcmToken,
      this.lessonCount,
      this.deviceType,
      });

  MetaData.fromJson(Map<String, dynamic> json) {
    nickname = json['nickname'];
    firstName = json['first_name'];
    lessonCount = json['lessonCount'];
    lastName = json['last_name'];
    description = json['description'];
    richEditing = json['rich_editing'];
    syntaxHighlighting = json['syntax_highlighting'];
    commentShortcuts = json['comment_shortcuts'];
    adminColor = json['admin_color'];
    useSsl = json['use_ssl'];
    showAdminBarFront = json['show_admin_bar_front'];
    locale = json['locale'];
    t96jAqsCapabilities = json['t96jAqs_capabilities'] != null
        ? (json['t96jAqs_capabilities'] is String) == true ? json['t96jAqs_capabilities'] :Caps.fromJson(json['t96jAqs_capabilities'])
        : null;
    t96jAqsUserLevel = json['t96jAqs_user_level'];
    sYoastWpseoProfileUpdated = json['_yoast_wpseo_profile_updated'];
    registerType = json['register_type'];
    userCustomFlag = json['user_custom_flag'];
    hasToBeActivated = json['has_to_be_activated'];
    jwtAuthPass = json['jwt_auth_pass'];
    fcmToken = json['fcm_token'];
    deviceType = json['device_type'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['nickname'] = this.nickname;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['description'] = this.description;
    data['lessonCount'] = this.lessonCount;
    data['rich_editing'] = this.richEditing;
    data['syntax_highlighting'] = this.syntaxHighlighting;
    data['comment_shortcuts'] = this.commentShortcuts;
    data['admin_color'] = this.adminColor;
    data['use_ssl'] = this.useSsl;
    data['show_admin_bar_front'] = this.showAdminBarFront;
    data['locale'] = this.locale;
    if (this.t96jAqsCapabilities != null) {
      data['t96jAqs_capabilities'] = this.t96jAqsCapabilities.toJson();
    }
    data['t96jAqs_user_level'] = this.t96jAqsUserLevel;
    data['_yoast_wpseo_profile_updated'] = this.sYoastWpseoProfileUpdated;
    data['register_type'] = this.registerType;
    data['user_custom_flag'] =this.userCustomFlag;
    data['has_to_be_activated'] = this.hasToBeActivated;
    data['jwt_auth_pass'] = this.jwtAuthPass;
    data['fcm_token'] = this.fcmToken;
    data['device_type'] = this.deviceType;
    return data;
  }
}
