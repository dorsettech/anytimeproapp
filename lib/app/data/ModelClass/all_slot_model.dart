import 'dart:convert';

List<AllSlotModel> allSlotModelFromJson(String str) => List<AllSlotModel>.from(json.decode(str).map((x) => AllSlotModel.fromJson(x)));

String allSlotModelToJson(List<AllSlotModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AllSlotModel {
  AllSlotModel({
    this.slotTime,
    this.availability,
  });

  String slotTime;
  bool availability;

  factory AllSlotModel.fromJson(Map<String, dynamic> json) => AllSlotModel(
    slotTime: json["slot_time"],
    availability: json["availability"],
  );

  Map<String, dynamic> toJson() => {
    "slot_time": slotTime,
    "availability": availability,
  };
}
