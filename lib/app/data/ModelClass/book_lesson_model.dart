/// status : "success"
/// message : "Lesson booked successfully"

// class BookLessonModel {
//   BookLessonModel({
//       this.status,
//       this.message,});
//
//   BookLessonModel.fromJson(dynamic json) {
//     status = json['status'];
//     message = json['message'];
//   }
//   String status;
//   String message;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['status'] = status;
//     map['message'] = message;
//     return map;
//   }
//
// }

// To parse this JSON data, do
//
//     final bookLessonModel = bookLessonModelFromJson(jsonString);

import 'dart:convert';

BookLessonModel bookLessonModelFromJson(String str) => BookLessonModel.fromJson(json.decode(str));

String bookLessonModelToJson(BookLessonModel data) => json.encode(data.toJson());

class BookLessonModel {
  BookLessonModel({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory BookLessonModel.fromJson(Map<String, dynamic> json) => BookLessonModel(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.postTitle,
    this.amount,
    this.date,
  });

  int id;
  String postTitle;
  String amount;
  String date;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    postTitle: json["postTitle"],
    amount: json["amount"],
    date: json["date"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "postTitle": postTitle,
    "amount": amount,
    "date": date,
  };
}
