
import 'dart:convert';

List<NewSetCoachAvailabilityModel> newSetCoachAvailabilityModelFromJson(String str) => List<NewSetCoachAvailabilityModel>.from(json.decode(str).map((x) => NewSetCoachAvailabilityModel.fromJson(x)));

String newSetCoachAvailabilityModelToJson(List<NewSetCoachAvailabilityModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class NewSetCoachAvailabilityModel {
  NewSetCoachAvailabilityModel({
    this.coachId,
    this.slots,
  });

  String coachId;
  List<Slot> slots;

  factory NewSetCoachAvailabilityModel.fromJson(Map<String, dynamic> json) => NewSetCoachAvailabilityModel(
    coachId: json["coach_id"],
    slots: List<Slot>.from(json["slots"].map((x) => Slot.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "coach_id": coachId,
    "slots": List<dynamic>.from(slots.map((x) => x.toJson())),
  };
}

class Slot {
  Slot({
    this.date,
    this.timeSlots,
  });

  String date;
  List<String> timeSlots;

  factory Slot.fromJson(Map<String, dynamic> json) => Slot(
    date: json["date"],
    timeSlots: List<String>.from(json["time_slots"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "date": date,
    "time_slots": List<dynamic>.from(timeSlots.map((x) => x)),
  };
}
