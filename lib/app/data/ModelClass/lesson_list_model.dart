/// status : "success"
/// data : [{"id":1121,"post_title":"Lesson booked (10-28-2021 05:19:58)","slot":"09:00-10:00","amount":"20","date":"28, Thu 2021","coach_name":"Aaron","is_lesson":"pending"}]

class LessonListModel {
  LessonListModel({
    this.status,
    this.data,
  });

  LessonListModel.fromJson(dynamic json) {
    status = json['status'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data.add(LessonResData.fromJson(v));
      });
    }
  }
  String status;
  List<LessonResData> data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    if (data != null) {
      map['data'] = data.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1121
/// post_title : "Lesson booked (10-28-2021 05:19:58)"
/// slot : "09:00-10:00"
/// amount : "20"
/// date : "28, Thu 2021"
/// coach_name : "Aaron"
/// is_lesson : "pending"

class LessonResData {
  LessonResData({
    this.id,
    this.postTitle,
    this.slot,
    this.amount,
    this.date,
    this.coach_id,
    this.coachName,
    this.isLesson,
  });

  LessonResData.fromJson(dynamic json) {
    id = json['id'];
    postTitle = json['post_title'];
    slot = json['slot'];
    amount = json['amount'];
    date = json['date'];
    coach_id = json['coach_id'];
    coachName = json['coach_name'];
    isLesson = json['is_lesson'];
  }
  int id;
  String postTitle;
  String slot;
  String amount;
  String date;
  String coach_id;
  String coachName;
  String isLesson;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['post_title'] = postTitle;
    map['slot'] = slot;
    map['amount'] = amount;
    map['date'] = date;
    map['coach_id'] = coach_id;
    map['coach_name'] = coachName;
    map['is_lesson'] = isLesson;
    return map;
  }
}
