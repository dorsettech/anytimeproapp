class ProfileModel {
  String status;
  ProfileResponse data;

  ProfileModel({this.status, this.data});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? ProfileResponse.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ProfileResponse {
  int iD;
  String userLogin;
  String userPass;
  String userNicename;
  String userEmail;
  String userUrl;
  String userRegistered;
  String userActivationKey;
  String userStatus;
  String displayName;
  String proId;
  String avatar;
  String firstName;
  String coachSubType;
  String age;
  String gender;
  String lastLessonDate;
  String countryOfResidence;
  String currentTown;
  String lastName;
  String nickname;
  String lessonTaken;
  String handicap;
  String improvementFactor;
  String golfing;
  String cardType;
  String useNameAccount;
  String nameOnCard;
  String cardNumber;
  String exMonth;
  String exYear;
  String currentHandicap;
  String homeDrivingRange;
  String homeGolfHouse;
  String favoriteCourse;
  String golfingSpecialisms;
  String userTestimonialRatingSpecialism;
  Caps caps;
  String capKey;
  List<String> roles;
  Allcaps allcaps;
  Null filter;
  String about;
  String price;

  ProfileResponse(
      {this.iD,
      this.userLogin,
      this.userPass,
      this.userNicename,
      this.userEmail,
      this.userUrl,
      this.userRegistered,
      this.userActivationKey,
      this.userStatus,
      this.displayName,
      this.proId,
      this.avatar,
      this.lastLessonDate,
      this.firstName,
      this.coachSubType,
      this.age,
      this.gender,
      this.countryOfResidence,
      this.currentTown,
      this.lastName,
      this.nickname,
      this.lessonTaken,
      this.handicap,
      this.improvementFactor,
      this.golfing,
      this.cardType,
      this.useNameAccount,
      this.nameOnCard,
      this.cardNumber,
      this.exMonth,
      this.exYear,
      this.currentHandicap,
      this.homeDrivingRange,
      this.homeGolfHouse,
      this.favoriteCourse,
      this.golfingSpecialisms,
      this.userTestimonialRatingSpecialism,
      this.caps,
      this.capKey,
      this.roles,
      this.allcaps,
      this.filter,
      this.about,
      this.price});

  ProfileResponse.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    userLogin = json['user_login'];
    userPass = json['user_pass'];
    userNicename = json['user_nicename'];
    userEmail = json['user_email'];
    userUrl = json['user_url'];
    userRegistered = json['user_registered'];
    userActivationKey = json['user_activation_key'];
    userStatus = json['user_status'];
    displayName = json['display_name'];
    proId = json['pro_id'];
    lastLessonDate = json['last_lesson_date'];
    avatar = json['avatar'];
    firstName = json['first_name'];
    coachSubType = json['coach_sub_type'];
    age = json['age'];
    gender = json['gender'];
    countryOfResidence = json['country_of_residence'];
    currentTown = json['current_town'];
    lastName = json['last_name'];
    nickname = json['nickname'];
    lessonTaken = json['lesson_taken'];
    handicap = json['handicap'];
    improvementFactor = json['improvement_factor'];
    golfing = json['golfing'];
    cardType = json['card_type'];
    useNameAccount = json['use_name_account'];
    nameOnCard = json['name_on_card'];
    cardNumber = json['card_number'];
    exMonth = json['ex_month'];
    exYear = json['ex_year'];
    currentHandicap = json['current_handicap'];
    homeDrivingRange = json['home_driving_range'];
    homeGolfHouse = json['home_golf_house'];
    favoriteCourse = json['favorite_course'];
    golfingSpecialisms = json['golfing_specialisms'];
    userTestimonialRatingSpecialism =
        json['user_testimonial_rating_specialism'];
    caps = json['caps'] != null ? Caps.fromJson(json['caps']) : null;
    capKey = json['cap_key'];
    roles = json['roles'].cast<String>();
    allcaps =
        json['allcaps'] != null ? Allcaps.fromJson(json['allcaps']) : null;
    filter = json['filter'];
    about = json['about'];
    price= json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = this.iD;
    data['user_login'] = this.userLogin;
    data['user_pass'] = this.userPass;
    data['user_nicename'] = this.userNicename;
    data['user_email'] = this.userEmail;
    data['user_url'] = this.userUrl;
    data['user_registered'] = this.userRegistered;
    data['user_activation_key'] = this.userActivationKey;
    data['user_status'] = this.userStatus;
    data['display_name'] = this.displayName;
    data['last_lesson_date'] = this.lastLessonDate;
    data['pro_id'] = this.proId;
    data['avatar'] = this.avatar;
    data['first_name'] = this.firstName;
    data['coach_sub_type'] = this.coachSubType;
    data['age'] = this.age;
    data['gender'] = this.gender;
    data['country_of_residence'] = this.countryOfResidence;
    data['current_town'] = this.currentTown;
    data['last_name'] = this.lastName;
    data['nickname'] = this.nickname;
    data['lesson_taken'] = this.lessonTaken;
    data['handicap'] = this.handicap;
    data['improvement_factor'] = this.improvementFactor;
    data['golfing'] = this.golfing;
    data['card_type'] = this.cardType;
    data['use_name_account'] = this.useNameAccount;
    data['name_on_card'] = this.nameOnCard;
    data['card_number'] = this.cardNumber;
    data['ex_month'] = this.exMonth;
    data['ex_year'] = this.exYear;
    data['current_handicap'] = this.currentHandicap;
    data['home_driving_range'] = this.homeDrivingRange;
    data['home_golf_house'] = this.homeGolfHouse;
    data['favorite_course'] = this.favoriteCourse;
    data['golfing_specialisms'] = this.golfingSpecialisms;
    data['user_testimonial_rating_specialism'] =
        this.userTestimonialRatingSpecialism;
    if (this.caps != null) {
      data['caps'] = this.caps.toJson();
    }
    data['cap_key'] = this.capKey;
    data['roles'] = this.roles;
    if (this.allcaps != null) {
      data['allcaps'] = this.allcaps.toJson();
    }
    data['filter'] = this.filter;
    data['about'] = this.about;
    data['price'] = this.price;
    return data;
  }
}

class Caps {
  bool coach;

  Caps({this.coach});

  Caps.fromJson(Map<String, dynamic> json) {
    coach = json['coach'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['coach'] = this.coach;
    return data;
  }
}

class Allcaps {
  bool read;
  bool level0;
  bool coach;

  Allcaps({this.read, this.level0, this.coach});

  Allcaps.fromJson(Map<String, dynamic> json) {
    read = json['read'];
    level0 = json['level_0'];
    coach = json['coach'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['read'] = this.read;
    data['level_0'] = this.level0;
    data['coach'] = this.coach;
    return data;
  }
}
