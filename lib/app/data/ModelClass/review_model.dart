class ReviewModel {
  ReviewModel({
    this.reviewBy,
    this.review,
    this.reviewFor,
    this.rating,
  });

  String reviewBy;
  String review;
  String reviewFor;
  dynamic rating;

  factory ReviewModel.fromJson(Map<String, dynamic> json) => ReviewModel(
    reviewBy: json["review_by"],
    review: json["review"],
    reviewFor: json["review_for"],
    rating: json["rating"],
  );

  Map<String, dynamic> toJson() => {
    "review_by": reviewBy,
    "review": review,
    "review_for": reviewFor,
    "rating": rating,
  };
}