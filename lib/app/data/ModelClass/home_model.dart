// To parse this JSON data, do
//
//     final homeModel = homeModelFromJson(jsonString);

// ignore_for_file: prefer_null_aware_operators

import 'dart:convert';

HomeModel homeModelFromJson(String str) => HomeModel.fromJson(json.decode(str));

String homeModelToJson(HomeModel data) => json.encode(data.toJson());

class HomeModel {
  HomeModel({
    this.status,
    this.data,
  });

  final String status;
  final HomeData data;

  factory HomeModel.fromJson(Map<String, dynamic> json) => HomeModel(
        status: json["status"],
        data: json["data"] == null ? null : HomeData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "data": data == null ? null : data.toJson(),
      };
}

class HomeData {
  HomeData({
    this.lessonTaken,
    this.handicap,
    this.improvementFactor,
    this.coachs,
  });

  final String lessonTaken;
  final String handicap;
  final String improvementFactor;
  final List<Coach> coachs;

  factory HomeData.fromJson(Map<String, dynamic> json) => HomeData(
        lessonTaken: json["lesson_taken"],
        handicap: json["handicap"],
        improvementFactor: json["improvement_factor"],
        coachs: json["coachs"] == null
            ? null
            : List<Coach>.from(json["coachs"].map((x) => Coach.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "lesson_taken": lessonTaken,
        "handicap": handicap,
        "improvement_factor": improvementFactor,
        "coachs": coachs == null
            ? null
            : List<dynamic>.from(coachs.map((x) => x.toJson())),
      };
}

class Coach {
  Coach({
    this.id,
    this.avatar,
    this.firstName,
    this.lastName,
    this.amount,
  });

  final String id;
  final String avatar;
  final String firstName;
  final String lastName;
  final int amount;

  factory Coach.fromJson(Map<String, dynamic> json) => Coach(
        id: json["id"],
        avatar: json["avatar"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        amount: json["amount"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "avatar": avatar,
        "first_name": firstName,
        "last_name": lastName,
        "amount": amount,
      };
}
