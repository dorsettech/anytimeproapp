// /// status : "sucess"
// /// slot : [{"slot":"00:00-01:00","avability":"unavailable"},{"slot":"01:00-02:00","avability":"unavailable"},{"slot":"02:00-03:00","avability":"unavailable"},{"slot":"03:00-04:00","avability":"unavailable"},{"slot":"04:00-05:00","avability":"unavailable"},{"slot":"05:00-06:00","avability":"unavailable"},{"slot":"06:00-07:00","avability":"unavailable"},{"slot":"07:00-08:00","avability":"unavailable"},{"slot":"08:00-09:00","avability":"unavailable"},{"slot":"09:00-10:00","avability":"unavailable"},{"slot":"10:00-11:00","avability":"unavailable"},{"slot":"11:00-12:00","avability":"unavailable"},{"slot":"12:00-13:00","avability":"available"},{"slot":"13:00-14:00","avability":"available"},{"slot":"14:00-15:00","avability":"unavailable"},{"slot":"15:00-16:00","avability":"unavailable"},{"slot":"16:00-17:00","avability":"unavailable"},{"slot":"17:00-18:00","avability":"unavailable"},{"slot":"18:00-19:00","avability":"unavailable"},{"slot":"19:00-20:00","avability":"unavailable"},{"slot":"20:00-21:00","avability":"unavailable"},{"slot":"21:00-22:00","avability":"unavailable"},{"slot":"22:00-23:00","avability":"unavailable"},{"slot":"23:00-24:00","avability":"unavailable"}]
// /// date : ["11/6/2021","11/7/2021"]
//
// class GetCoachAvailabilityModel {
//   GetCoachAvailabilityModel({
//       this.status,
//       this.slot,
//       this.date,});
//
//   GetCoachAvailabilityModel.fromJson(dynamic json) {
//     status = json['status'];
//     if (json['slot'] != null) {
//       slot = [];
//       json['slot'].forEach((v) {
//         slot.add(Slot.fromJson(v));
//       });
//     }
//     date = json['date'] != null ? json['date'].cast<String>() : [];
//   }
//   String status;
//   List<Slot> slot;
//   List<String> date;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['status'] = status;
//     if (slot != null) {
//       map['slot'] = slot.map((v) => v.toJson()).toList();
//     }
//     map['date'] = date;
//     return map;
//   }
//
// }
//
// /// slot : "00:00-01:00"
// /// avability : "unavailable"
//
// class Slot {
//   Slot({
//       this.slot,
//       this.avability,});
//
//   Slot.fromJson(dynamic json) {
//     slot = json['slot'];
//     avability = json['avability'];
//   }
//   String slot;
//   String avability;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['slot'] = slot;
//     map['avability'] = avability;
//     return map;
//   }
//
// }