/// status : "success"
/// data : ["00:00-01:00","01:00-02:00","02:00-03:00","03:00-04:00"]

class SlotModel {
  SlotModel({
      this.status, 
      this.data,});

  SlotModel.fromJson(dynamic json) {
    status = json['status'];
    data = json['data'] != null ? json['data'].cast<String>() : [];
  }
  String status;
  List<String> data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['data'] = data;
    return map;
  }

}