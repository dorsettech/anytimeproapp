/// status : "success"
/// message : "User registered successfully"
/// data : {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYW55dGltZXByby5pbyIsImlhdCI6MTYzNDEyMTQ3NywibmJmIjoxNjM0MTIxNDc3LCJleHAiOjE2MzQ3MjYyNzcsImRhdGEiOnsidXNlciI6eyJpZCI6OTUsImRldmljZSI6IiIsInBhc3MiOm51bGx9fX0.jLWvYH2E0mabFFWV7PDe6WdTliElhhHpRsU8dO5fyto","id":95,"email":"devtest101@mail.comq","nicename":"devtest101mail-comq","firstName":"dev","lastName":"test","displayName":"dev test","success":true,"statusCode":200,"code":"jwt_auth_valid_credential","message":"Credential is valid"}

class SignupModel {
  SignupModel({
      this.status, 
      this.message, 
      this.data,});

  SignupModel.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  String status;
  String message;
  Data data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    if (data != null) {
      map['data'] = data.toJson();
    }
    return map;
  }

}

/// token : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYW55dGltZXByby5pbyIsImlhdCI6MTYzNDEyMTQ3NywibmJmIjoxNjM0MTIxNDc3LCJleHAiOjE2MzQ3MjYyNzcsImRhdGEiOnsidXNlciI6eyJpZCI6OTUsImRldmljZSI6IiIsInBhc3MiOm51bGx9fX0.jLWvYH2E0mabFFWV7PDe6WdTliElhhHpRsU8dO5fyto"
/// id : 95
/// email : "devtest101@mail.comq"
/// nicename : "devtest101mail-comq"
/// firstName : "dev"
/// lastName : "test"
/// displayName : "dev test"
/// success : true
/// statusCode : 200
/// code : "jwt_auth_valid_credential"
/// message : "Credential is valid"

class Data {
  Data({
      this.token, 
      this.id, 
      this.email, 
      this.nicename, 
      this.firstName, 
      this.lastName, 
      this.displayName, 
      this.success, 
      this.statusCode, 
      this.code, 
      this.message,});

  Data.fromJson(dynamic json) {
    token = json['token'];
    id = json['id'];
    email = json['email'];
    nicename = json['nicename'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    displayName = json['displayName'];
    success = json['success'];
    statusCode = json['statusCode'];
    code = json['code'];
    message = json['message'];
  }
  String token;
  int id;
  String email;
  String nicename;
  String firstName;
  String lastName;
  String displayName;
  bool success;
  int statusCode;
  String code;
  String message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = token;
    map['id'] = id;
    map['email'] = email;
    map['nicename'] = nicename;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['displayName'] = displayName;
    map['success'] = success;
    map['statusCode'] = statusCode;
    map['code'] = code;
    map['message'] = message;
    return map;
  }

}