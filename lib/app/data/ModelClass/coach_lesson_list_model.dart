/// status : "success"
/// data : {"lessons_upcomming":[{"id":"1119","slot":"01:00-02:00","date":"27-10-2021","post_title":"Lesson booked (10-27-2021 12:23:37)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},null],"lessons_past":[{"id":"920","slot":"07:00-08:00","date":"30-09-2021","post_title":"Lesson booked (08-31-2021 06:47:58)","is_lesson":"accept","player_id":"75","player_first_name":"testing2","player_last_name":"test","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"},{"id":"921","slot":"07:00-08:00","date":"17-09-2021","post_title":"Lesson booked (08-31-2021 07:10:53)","is_lesson":"accept","player_id":"75","player_first_name":"testing2","player_last_name":"test","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"},{"id":"922","slot":"07:00-08:00","date":"18-09-2021","post_title":"Lesson booked (08-31-2021 08:07:44)","is_lesson":"accept","player_id":"75","player_first_name":"testing2","player_last_name":"test","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"},{"id":"1009","slot":"11:00-12:00","date":"10-09-2021","post_title":"Lesson booked (09-10-2021 05:09:23)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1010","slot":"11:00-12:00","date":"10-09-2021","post_title":"Lesson booked (09-10-2021 05:09:26)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1040","slot":"15:00-16:00","date":"16-09-2021","post_title":"Lesson booked (09-16-2021 12:12:37)","is_lesson":"pending","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1041","slot":"07:00-08:00","date":"17-09-2021","post_title":"Lesson booked (09-17-2021 05:39:18)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1043","slot":"14:00-15:00","date":"17-09-2021","post_title":"Lesson booked (09-17-2021 10:16:27)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1049","slot":"17:00-18:00","date":"28-09-2021","post_title":"Lesson booked (09-28-2021 14:56:50)","is_lesson":"decline","player_id":"77","player_first_name":"mark","player_last_name":"devlin","avatar":"https://www.anytimepro.io/wp-content/uploads/download-1.png"},{"id":"1053","slot":"09:00-10:00","date":"14-10-2021","post_title":"Lesson booked (10-12-2021 06:42:00)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1115","slot":"02:00-03:00","date":"25-10-2021","post_title":"Lesson booked (10-25-2021 10:38:54)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1116","slot":"00:00-01:00","date":"25-10-2021","post_title":"Lesson booked (10-25-2021 10:41:22)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1117","slot":"00:00-01:00","date":"26-10-2021","post_title":"Lesson booked (10-25-2021 10:43:26)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1118","slot":"01:00-02:00","date":"27-10-2021","post_title":"Lesson booked (10-27-2021 04:29:01)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1119","slot":"01:00-02:00","date":"27-10-2021","post_title":"Lesson booked (10-27-2021 12:23:37)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1124","slot":"01:00-02:00","date":"20211029","post_title":"Lesson booked (10-29-2021 12:00:13)","is_lesson":"pending","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1130","slot":"12:00-13:00","date":"2021-11-6","post_title":"Lesson booked (11-06-2021 07:27:02)","is_lesson":"pending","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"}]}

class CoachLessonListModel {
  CoachLessonListModel({
    this.status,
    this.data,
  });

  CoachLessonListModel.fromJson(dynamic json) {
    status = json['status'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  String status;
  Data data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    if (data != null) {
      map['data'] = data.toJson();
    }
    return map;
  }
}

/// lessons_upcomming : [{"id":"1119","slot":"01:00-02:00","date":"27-10-2021","post_title":"Lesson booked (10-27-2021 12:23:37)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},null]
/// lessons_past : [{"id":"920","slot":"07:00-08:00","date":"30-09-2021","post_title":"Lesson booked (08-31-2021 06:47:58)","is_lesson":"accept","player_id":"75","player_first_name":"testing2","player_last_name":"test","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"},{"id":"921","slot":"07:00-08:00","date":"17-09-2021","post_title":"Lesson booked (08-31-2021 07:10:53)","is_lesson":"accept","player_id":"75","player_first_name":"testing2","player_last_name":"test","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"},{"id":"922","slot":"07:00-08:00","date":"18-09-2021","post_title":"Lesson booked (08-31-2021 08:07:44)","is_lesson":"accept","player_id":"75","player_first_name":"testing2","player_last_name":"test","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"},{"id":"1009","slot":"11:00-12:00","date":"10-09-2021","post_title":"Lesson booked (09-10-2021 05:09:23)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1010","slot":"11:00-12:00","date":"10-09-2021","post_title":"Lesson booked (09-10-2021 05:09:26)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1040","slot":"15:00-16:00","date":"16-09-2021","post_title":"Lesson booked (09-16-2021 12:12:37)","is_lesson":"pending","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1041","slot":"07:00-08:00","date":"17-09-2021","post_title":"Lesson booked (09-17-2021 05:39:18)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1043","slot":"14:00-15:00","date":"17-09-2021","post_title":"Lesson booked (09-17-2021 10:16:27)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1049","slot":"17:00-18:00","date":"28-09-2021","post_title":"Lesson booked (09-28-2021 14:56:50)","is_lesson":"decline","player_id":"77","player_first_name":"mark","player_last_name":"devlin","avatar":"https://www.anytimepro.io/wp-content/uploads/download-1.png"},{"id":"1053","slot":"09:00-10:00","date":"14-10-2021","post_title":"Lesson booked (10-12-2021 06:42:00)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1115","slot":"02:00-03:00","date":"25-10-2021","post_title":"Lesson booked (10-25-2021 10:38:54)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1116","slot":"00:00-01:00","date":"25-10-2021","post_title":"Lesson booked (10-25-2021 10:41:22)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1117","slot":"00:00-01:00","date":"26-10-2021","post_title":"Lesson booked (10-25-2021 10:43:26)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1118","slot":"01:00-02:00","date":"27-10-2021","post_title":"Lesson booked (10-27-2021 04:29:01)","is_lesson":"accept","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1119","slot":"01:00-02:00","date":"27-10-2021","post_title":"Lesson booked (10-27-2021 12:23:37)","is_lesson":"decline","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1124","slot":"01:00-02:00","date":"20211029","post_title":"Lesson booked (10-29-2021 12:00:13)","is_lesson":"pending","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"},{"id":"1130","slot":"12:00-13:00","date":"2021-11-6","post_title":"Lesson booked (11-06-2021 07:27:02)","is_lesson":"pending","player_id":"74","player_first_name":"chris ryu","player_last_name":"","avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"}]

class Data {
  Data({
    this.lessonsUpcomming,
    this.lessonsPast,
  });

  Data.fromJson(dynamic json) {
    if (json['lessons_upcomming'] != null) {
      lessonsUpcomming = [];
      json['lessons_upcomming'].forEach((v) {
        lessonsUpcomming.add(Lessons_upcomming.fromJson(v));
      });
    }
    if (json['lessons_past'] != null) {
      lessonsPast = [];
      json['lessons_past'].forEach((v) {
        lessonsPast.add(Lessons_past.fromJson(v));
      });
    }
  }
  List<Lessons_upcomming> lessonsUpcomming;
  List<Lessons_past> lessonsPast;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (lessonsUpcomming != null) {
      map['lessons_upcomming'] =
          lessonsUpcomming.map((v) => v.toJson()).toList();
    }
    if (lessonsPast != null) {
      map['lessons_past'] = lessonsPast.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : "920"
/// slot : "07:00-08:00"
/// date : "30-09-2021"
/// post_title : "Lesson booked (08-31-2021 06:47:58)"
/// is_lesson : "accept"
/// player_id : "75"
/// player_first_name : "testing2"
/// player_last_name : "test"
/// avatar : "https://www.anytimepro.io//wp-content/uploads/avatars/1634191495.jpeg"

class Lessons_past {
  Lessons_past({
    this.id,
    this.slot,
    this.date,
    this.postTitle,
    this.isLesson,
    this.playerId,
    this.playerFirstName,
    this.playerLastName,
    this.avatar,
  });

  Lessons_past.fromJson(dynamic json) {
    id = json['id'];
    slot = json['slot'];
    date = json['date'];
    postTitle = json['post_title'];
    isLesson = json['is_lesson'];
    playerId = json['player_id'];
    playerFirstName = json['player_first_name'];
    playerLastName = json['player_last_name'];
    avatar = json['avatar'];
  }
  int id;
  String slot;
  String date;
  String postTitle;
  String isLesson;
  String playerId;
  String playerFirstName;
  String playerLastName;
  String avatar;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['slot'] = slot;
    map['date'] = date;
    map['post_title'] = postTitle;
    map['is_lesson'] = isLesson;
    map['player_id'] = playerId;
    map['player_first_name'] = playerFirstName;
    map['player_last_name'] = playerLastName;
    map['avatar'] = avatar;
    return map;
  }
}

/// id : "1119"
/// slot : "01:00-02:00"
/// date : "27-10-2021"
/// post_title : "Lesson booked (10-27-2021 12:23:37)"
/// is_lesson : "decline"
/// player_id : "74"
/// player_first_name : "chris ryu"
/// player_last_name : ""
/// avatar : "https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1635146307.jpeg"

class Lessons_upcomming {
  Lessons_upcomming({
    this.id,
    this.slot,
    this.date,
    this.postTitle,
    this.isLesson,
    this.playerId,
    this.playerFirstName,
    this.playerLastName,
    this.avatar,
    this.amount,
  });

  Lessons_upcomming.fromJson(dynamic json) {
    id = json['id'];
    slot = json['slot'];
    date = json['date'];
    postTitle = json['post_title'];
    isLesson = json['is_lesson'];
    playerId = json['player_id'];
    playerFirstName = json['player_first_name'];
    playerLastName = json['player_last_name'];
    avatar = json['avatar'];
    amount = json['transaction_id'];
  }
  int id;
  String slot;
  String date;
  String postTitle;
  String isLesson;
  String playerId;
  String playerFirstName;
  String playerLastName;
  String avatar;
  String amount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['slot'] = slot;
    map['date'] = date;
    map['post_title'] = postTitle;
    map['is_lesson'] = isLesson;
    map['player_id'] = playerId;
    map['player_first_name'] = playerFirstName;
    map['player_last_name'] = playerLastName;
    map['avatar'] = avatar;
    map['transaction_id'] = amount;
    return map;
  }
}
