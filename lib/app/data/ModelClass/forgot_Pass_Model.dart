/// status : "success"
/// message : "Password reset link has been sent to your registered email"

class ForgotPassModel {
  ForgotPassModel({
      this.status, 
      this.message,});

  ForgotPassModel.fromJson(dynamic json) {
    status = json['status'];
    message = json['message'];
  }
  String status;
  String message;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    map['message'] = message;
    return map;
  }

}