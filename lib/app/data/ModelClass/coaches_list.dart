/// status : "success"
/// data : {"coaches":[{"id":76,"avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1638099674.","first_name":"matty","last_name":"ov pro","rating":5,"amount":20,"coach_sub_type":"basic","availabilty":false},{"id":82,"avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1629809843.jpeg","first_name":"Jonny","last_name":"Jones","rating":5,"amount":20,"coach_sub_type":"mid_tier","availabilty":false},{"id":73,"avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1638458445.jpeg","first_name":"Emma","last_name":"Ryu","rating":5,"amount":20,"coach_sub_type":"pros","availabilty":false}]}

class CoachesListModel {
  CoachesListModel({
    this.status,
    this.data,
  });

  CoachesListModel.fromJson(dynamic json) {
    status = json['status'];
    data = json['data'] != null ? CoachesResData.fromJson(json['data']) : null;
  }
  String status;
  CoachesResData data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    if (data != null) {
      map['data'] = data.toJson();
    }
    return map;
  }
}

/// coaches : [{"id":76,"avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1638099674.","first_name":"matty","last_name":"ov pro","rating":5,"amount":20,"coach_sub_type":"basic","availabilty":false},{"id":82,"avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/1629809843.jpeg","first_name":"Jonny","last_name":"Jones","rating":5,"amount":20,"coach_sub_type":"mid_tier","availabilty":false},{"id":73,"avatar":"https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1638458445.jpeg","first_name":"Emma","last_name":"Ryu","rating":5,"amount":20,"coach_sub_type":"pros","availabilty":false}]

class CoachesResData {
  CoachesResData({
    this.coaches,
  });

  CoachesResData.fromJson(dynamic json) {
    if (json['coaches'] != null) {
      coaches = [];
      json['coaches'].forEach((v) {
        coaches.add(Coaches.fromJson(v));
      });
    }
  }
  List<Coaches> coaches;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (coaches != null) {
      map['coaches'] = coaches.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 76
/// avatar : "https://www.anytimepro.io//wp-content/uploads/avatars/avatar-image-1638099674."
/// first_name : "matty"
/// last_name : "ov pro"
/// rating : 5
/// amount : 20
/// coach_sub_type : "basic"
/// availabilty : false

class Coaches {
  Coaches({
    this.id,
    this.avatar,
    this.firstName,
    this.lastName,
    this.rating,
    this.amount,
    this.coachSubType,
    this.availabilty,
    this.lessons,
    this.reviews
  });

  Coaches.fromJson(dynamic json) {
    id = json['id'];
    avatar = json['avatar'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    rating = json['rating'];
    amount = json['amount'];
    coachSubType = json['coach_sub_type'];
    availabilty = json['availabilty'];
    lessons = json['lessons'];
    reviews = json['reviews'] ?? 0;
  }
  int id;
  String avatar;
  String firstName;
  String lastName;
  int rating;
  String amount;
  String coachSubType;
  bool availabilty;
  int lessons;
  int reviews;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['avatar'] = avatar;
    map['first_name'] = firstName;
    map['last_name'] = lastName;
    map['rating'] = rating;
    map['amount'] = amount;
    map['coach_sub_type'] = coachSubType;
    map['availabilty'] = availabilty;
    map['lessons'] = lessons;
    map['reviews'] = reviews;
    return map;
  }
}
