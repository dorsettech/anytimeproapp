class SaveCard {
  String status;
  Message message;

  SaveCard({this.status, this.message});

  SaveCard.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message =
    json['message'] != null ? Message.fromJson(json['message']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    return data;
  }
}

class Message {
  String lastFour;
  String cardBrand;
  String cardSaved;
  String message;

  Message({this.lastFour, this.cardBrand, this.message});

  Message.fromJson(dynamic json) {
    message = (json is String) ? json : '';
    cardBrand = (json is String) ? '' : json['card_brand'];
    cardSaved = (json is String) ? '' : json['card_saved'];
    lastFour = (json is String) ? '' : json['last_four'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['last_four'] = this.lastFour;
    data['card_brand'] = this.cardBrand;
    data['card_saved'] = this.cardSaved;
    data['message'] = this.message;
    return data;
  }
}