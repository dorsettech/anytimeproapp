import 'dart:convert';
import 'dart:developer';
import 'dart:math';
import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/ModelClass/all_slot_model.dart';

import 'package:anytime_pro/app/data/ModelClass/book_lesson_model.dart';
import 'package:anytime_pro/app/data/ModelClass/coach_availabilit_model.dart';
import 'package:anytime_pro/app/data/ModelClass/coach_lesson_list_model.dart';
import 'package:anytime_pro/app/data/ModelClass/coaches_list.dart';
import 'package:anytime_pro/app/data/ModelClass/edit_profile_personal.dart';
import 'package:anytime_pro/app/data/ModelClass/forgot_Pass_Model.dart';
import 'package:anytime_pro/app/data/ModelClass/home_model.dart';
import 'package:anytime_pro/app/data/ModelClass/lesson_action_model.dart';
import 'package:anytime_pro/app/data/ModelClass/lesson_list_model.dart';
import 'package:anytime_pro/app/data/ModelClass/login_model.dart';
import 'package:anytime_pro/app/data/ModelClass/login_model.dart' as meta;
import 'package:anytime_pro/app/data/ModelClass/new_set_caoch_availability_model.dart';
import 'package:anytime_pro/app/data/ModelClass/payment_model.dart';
import 'package:anytime_pro/app/data/ModelClass/profile_model.dart';
import 'package:anytime_pro/app/data/ModelClass/review_model.dart';
import 'package:anytime_pro/app/data/ModelClass/signup_model.dart';
import 'package:anytime_pro/app/data/ModelClass/video_folder_list_model.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart' as Con;
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../ModelClass/save_card.dart';

class ApiProvider {
  static String token = GetStorage().read('token');
  static String id = GetStorage().read('id');
  static String fcmToken = GetStorage().read('fcm_token');
  var dio = Dio();
  final storage = GetStorage();

  /// error Handler
  dynamic errorHandler(Response response) {
    print(response.toString());
    switch (response.statusCode) {
      case 200:
      case 201:
      case 202:
        var responseJson = response.data;
        return responseJson;
      case 500:
        throw "Server Error please retry later";
      //return errorSnackbar("Server Error please retry later");
      case 403:
        throw 'Error occurred pls check internet and retry.';
      //return errorSnackbar('Error occurred pls check internet and retry.');
      default:
        throw 'Error occurred retry';
      // return errorSnackbar('Error occurred please retry');
    }
  }

  ///=========  Send Push Notification ============///
  Future<void> sendPushNotification(
      int senderId, String titleMessage, String sendMessage) async {
    try {
      Response response = await dio.post(
          "${KApiTexts.BASE_URL}${KApiTexts.sendPushNotification}",
          data: {"id": senderId, "title": titleMessage, "message": sendMessage},
          options: Options(headers: {
            'Content-type': 'application/json',
            'Authorization': 'Bearer $token',
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        final body = jsonDecode(_body)['success'];
        if (body == 1) {
          print("Push Notification Send Successfully");
        } else {
          print(
              "Facing issue with sending push notification or not registered device.1");
        }
      } else {
        print(
            "Facing issue with sending push notification or not registered device.2");
      }
    } catch (e) {
      print(e.toString());
    }
  }

  ///=========  AGORA CLOUD RECORDING ACQUIRE API============///
  Future<String> agoraAcquire(String uID) async {
    Response response = await dio.post(TPartyTokens.AgoraAcquireUrl,
        data: {
          "cname": TPartyTokens.AgoraCloudChannelName,
          "uid": uID,
          "clientRequest": {"resourceExpiredHour": 24, "scene": 0}
        },
        options: Options(headers: {
          'Content-type': 'application/json',
          "Authorization": TPartyTokens.AgoraAuthenticationKey
        }));
    if (response.statusCode == 200) {
      var _body = json.encode(response.data);
      String agoraRecordingResourceId = jsonDecode(_body)['resourceId'];
      if (agoraRecordingResourceId != null || agoraRecordingResourceId != "") {
        print("ACQUIRE API CALLING DONE NOW CALLING START RECORDING:");
        return agoraRecordingResourceId;
      } else {
        print("FACING ISSUE WITH ACQUIRE API CALLING FOR CLOUD RECORDING");
        return null;
      }
    } else {
      print(
          "ACQUIRE API CALLING FOR CLOUD RECORDING RESPONSE GETTING:  ${response.statusCode.toString()}");
      return null;
    }
  }

  ///=========  AGORA CLOUD RECORDING START API============///
  Future<String> agoraStartCloudRecording(String rID, String uID,
      String playerId, String ownAgoraUid, String playerName) async {
    //var userRole = storage.read('role');
    var now = DateTime.now();
    var formatter = DateFormat('dd-MM-yyyy');
    String formattedDate = formatter.format(now);
    var userId = storage.read('id');
    var userName = "${storage.read('name')} ${storage.read('lastname')}";
    Response response = await dio.post(
        "${TPartyTokens.AgoraStartStopBaseUrl}$rID${TPartyTokens.AgoraStartStopEndUrl}/start",
        data: {
          "uid": uID,
          "cname": TPartyTokens.AgoraCloudChannelName,
          "clientRequest": {
            "token": TPartyTokens.agoraToken,
            "recordingConfig": {
              "avFileType": ["hls", "mp4"]
            },
            "storageConfig": {
              "accessKey": TPartyTokens.GoogleCloudStorageAccessKey,
              "region": 2,
              // "bucket": "${TPartyTokens.GoogleCloudStorageBucketName}/${userRole.toString()}/${userId.toString()}",
              "bucket":
                  "${TPartyTokens.GoogleCloudStorageBucketName}/${userId.toString()}_${userName}_${playerId}_${playerName}_$formattedDate/$ownAgoraUid",
              "secretKey": TPartyTokens.GoogleCloudStorageSecretKey,
              "vendor": 6,
            }
          }
        },
        options: Options(headers: {
          'Content-type': 'application/json',
          "Authorization": TPartyTokens.AgoraAuthenticationKey
        }));

    if (response.statusCode == 200) {
      var _body = json.encode(response.data);
      String agoraRecordingStorageId = jsonDecode(_body)['sid'];
      if (agoraRecordingStorageId != null || agoraRecordingStorageId != "") {
        print("START API CALLING DONE");
        return agoraRecordingStorageId;
      } else {
        print("FACING ISSUE WITH START API CALLING FOR CLOUD RECORDING");
        return null;
      }
    } else {
      print(
          "START API CALLING FOR CLOUD RECORDING RESPONSE GETTING:  ${response.statusCode.toString()}");
      return null;
    }
  }

  ///=========  AGORA CLOUD RECORDING STOP API============///
  Future<String> agoraStopCloudRecording(
      String rID, String sID, String uID) async {
    Response response = await dio.post(
      "${TPartyTokens.AgoraStartStopBaseUrl}$rID/sid/$sID${TPartyTokens.AgoraStartStopEndUrl}/stop",
      data: {
        "cname": TPartyTokens.AgoraCloudChannelName,
        "uid": uID,
        "clientRequest": {}
      },
      options: Options(headers: {
        'Content-type': 'application/json',
        "Authorization": TPartyTokens.AgoraAuthenticationKey
      }),
    );
    if (response.statusCode == 200) {
      print("STOP API CALLING DONE");
      return "stop";
    } else {
      print(
          "STOP API CALLING FOR CLOUD RECORDING RESPONSE GETTING:  ${response.statusCode.toString()}");
      return null;
    }
  }

  ///=========  Load Video API============///
  Future<String> loadRecordedVideo(String filePath) async {
    Response response = await dio.post(
      "${KApiTexts.BASE_URL}file-download",
      data: {
        "bucket": TPartyTokens.GoogleCloudStorageBucketName,
        "file_path": filePath,
      },
      options: Options(headers: {
        'Content-type': 'application/json',
      }),
    );
    if (response.statusCode == 200) {
      print("SuccessFull");
      var _body = jsonDecode(json.encode(response.data["file_link"]));
      return _body;
    } else {
      print("Failed:  ${response.statusCode.toString()}");
      return null;
    }
  }

  ///=========  LOGIN API============///
  Future<LoginModel> loginPost(String loginEmail, String loginPass) async {
    try {
      Response response = await dio.post(
          "${KApiTexts.BASE_URL}${KApiTexts.LOGIN}",
          data: {
            'username': loginEmail,
            'password': loginPass,
            'fcm_token': fcmToken
          },
          options: Options(headers: {"Content-Type": "application/json"}));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        LoginModel userToken = LoginModel.fromJson(jsonDecode(_body));
        print("${response.statusCode}\n${response.data}");
        Map<String, String> queryParams = {
          'username':loginEmail,
          'password':loginPass,
          'json': 'user/generate_auth_cookie'
        };
        var cookieResponse = await dio.post("https://www.anytimepro.io/api", queryParameters: queryParams,  options: Options(
          headers: {"Content-Type": "application/json"},
        ));
        userToken.cookie = cookieResponse.data['cookie'];
        return userToken;
      } else {
        return errorHandler(response);
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  /// ===========SIGNUP API CALL===================//
  Future<SignupModel> signUpPost(
      String email, String fullname, String role, String pass) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.SIGNUP}",
            data: {
              "email": email,
              "full_name": fullname,
              "role": role,
              "password": pass,
              "register_type": 'normal',
              "user_custom_flag": "0",
              'fcm_token': fcmToken
            },
            options: Options(headers: {"Content-Type": "application/json"}));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      SignupModel signUpModel = SignupModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return signUpModel;
    } else {
      return errorHandler(response);
    }
  }

  Future<ForgotPassModel> forgotPass(String email) async {
    Response response = await dio.post(
        "${KApiTexts.BASE_URL}${KApiTexts.forgotPassword}",
        data: {"email": email},
        options: Options(headers: {"Content-Type": "application/json"}));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      ForgotPassModel forgotPassModel =
          ForgotPassModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return forgotPassModel;
    } else {
      return errorHandler(response);
    }
  }

  Future<ProfileModel> getProfile(int id) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.profile}",
            data: {"id": id.toString()},
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      ProfileModel profileModel = ProfileModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return profileModel;
    } else {
      return errorHandler(response);
    }
  }

  Future<dynamic> deleteAccount(int id) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.deleteAccount}",
            data: {"user_id": id.toString()},
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return errorHandler(response);
    }
  }

  Future<dynamic> coachOnBoardToStripe(int id) async {
    var map = <String, dynamic>{};
    map['coach_id'] = id;
    FormData data = FormData.fromMap(map);
    Response response =
        await dio.post("https://www.anytimepro.io/${KApiTexts.coachOnboard}/",
            data: data,
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return errorHandler(response);
    }
  }

  Future<dynamic> coachStripeStatus(int id) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.coachStripeStatus}",
            data: {"coach_id": id.toString()},
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return errorHandler(response);
    }
  }

  Future<HomeModel> home(String token) async {
    Response response = await dio.get("${KApiTexts.BASE_URL}${KApiTexts.home}",
        options: Options(headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      HomeModel homeModel = HomeModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return homeModel;
    } else {
      return errorHandler(response);
    }
  }

  Future<EditProfilePersonal> updateProfile(
      int id,
      String name,
      String lastName,
      String gender,
      String nickname,
      String age,
      String country,
      String town,
      String coachType,
      String dp,
      about,
      price) async {
    assert(id != null);

    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.updateProfile}",
            data: {
              'id': id,
              'type': 'personal',
              'first_name': name,
              'last_name': lastName,
              'nickname': nickname,
              'age': age,
              'gender': gender,
              'current_town': town,
              'coach_sub_type': coachType,
              'country_of_residence': country,
              'avatar': "data:image/jpg;base64,$dp",
              'about': about,
              'price': price
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      EditProfilePersonal editProfilePersonal =
          EditProfilePersonal.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return editProfilePersonal;
    } else {
      return errorHandler(response);
    }
  }

/*--------------------------------------------------------------------------*/

  Future<EditProfilePersonal> updateGolfing(
    int id,
    String handicap,
    String homeDrivingRange,
    String homeGolfHouse,
    String golfing,
    String favoriteCourse,
    String golfingSpecialisms,
    String testimonial,
  ) async {
    assert(id != null);

    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.updateProfile}",
            data: {
              'id': id,
              'type': 'golfing',
              'golfing': golfing,
              'current_handicap': handicap,
              'home_driving_range': homeDrivingRange,
              'home_golf_house': homeGolfHouse,
              'favorite_course': favoriteCourse,
              'golfing_specialisms': golfingSpecialisms,
              'user_testimonial_rating_specialism': testimonial,
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      EditProfilePersonal editProfilePersonal =
          EditProfilePersonal.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return editProfilePersonal;
    } else {
      return errorHandler(response);
    }
  }

/*--------------------------------------------------------------------------*/
  Future<dynamic> _handleSavePress(String clientSecret, int id) async {
    try {
      final billingDetails = BillingDetails(
        name: "Test User",
        email: 'email@stripe.com',
        phone: '+48888000888',
        address: Address(
          city: 'Houston',
          country: 'US',
          line1: '1459  Circle Drive',
          line2: '',
          state: 'Texas',
          postalCode: '77063',
        ),
      ); // mo/ mocked data for tests

      final setupIntentResult = await Stripe.instance.confirmSetupIntent(
        clientSecret,
        PaymentMethodParams.card(
          paymentMethodData: PaymentMethodData(
            billingDetails: billingDetails,
          ),
        ),
      );
      print('Setup Intent created $setupIntentResult');
      return setupIntentResult;
    } catch (error, s) {
      // errorSnackbar(error);
      print(error);
      print(s);
      return error;
    }
  }

  Future<EditProfilePersonal> updatePaymentInfo(
    int id,
    String use_name_account,
    String name_on_card,
    String card_number,
    String ex_month,
    String ex_year, bool rememberCardDetail,
  ) async {
    assert(id != null);

    var secretKey =  await saveCardFuture(id, rememberCardDetail);

      final setupIntentResult = await _handleSavePress(secretKey, id);

      if (setupIntentResult is SetupIntent && setupIntentResult != null) {
        Response res =
        await dio.post(
            "${KApiTexts.BASE_URL}${KApiTexts.savePaymentMethodFuture}",
            data: {
              'payment_method': setupIntentResult.paymentMethodId,
              'player': id,
              'remember_card_detail': rememberCardDetail,
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
        print(res.data);
      } else {
        return EditProfilePersonal(status: 'Fail', message: setupIntentResult.toString());
      }
      Response response =
          await dio.post("${KApiTexts.BASE_URL}${KApiTexts.updateProfile}",
              data: {
                'id': id,
                'use_name_account': use_name_account,
                'name_on_card': name_on_card,
                'card_number': card_number,
                'ex_month': ex_month,
                'ex_year': ex_year,
              },
              options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        EditProfilePersonal editProfilePersonal =
        EditProfilePersonal.fromJson(jsonDecode(_body));
        print("${response.statusCode}\n${response.data}");
        return editProfilePersonal;
      } else {
        return errorHandler(response);
      }
    }


  Future<dynamic> saveCardFuture(int id, bool rememberCardDetail,) async {
    assert(id != null);

    Response response =
    await dio.post("${KApiTexts.BASE_URL}${KApiTexts.saveCardFuture}",
        data: {
          'player': id,
          'remember_card_detail': rememberCardDetail,
        },
        options: Options(headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      print("${response.statusCode}\n${response.data}");
      return response.data.toString();
    } else {
      return errorHandler(response);
    }
  }

  Future<SaveCard> getSaveCard(int id,) async {
    assert(id != null);

    Response response =
    await dio.post("${KApiTexts.BASE_URL}${KApiTexts.getSavedCard}",
        data: {
          'player': id,
        },
        options: Options(headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        }));
    var _body = json.encode(response.data);
    print("response.data ${response.data}");
    if (response.statusCode == 200) {
      print("${response.statusCode}\n${response.data}");
      SaveCard editProfilePersonal =
      SaveCard.fromJson(jsonDecode(_body));
      return editProfilePersonal;
    } else {
      return errorHandler(response);
    }
  }

  /*--------------------------------------------------------------------------*/

  Future<CoachesListModel> fetchAllCoachList(
      String date, String slot, String day) async {
    try {
      Response response =
          await dio.post("${KApiTexts.BASE_URL}${KApiTexts.coachList}",
              data: {
                "date": date,
                "slot": slot,
                "day_name": day,
              },
              options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        CoachesListModel coachesListModel =
            CoachesListModel.fromJson(jsonDecode(_body));
        print("${response.statusCode}\n${response.data}");
        return coachesListModel;
      } else {
        return errorHandler(response);
      }
    } catch (ex) {
      print(ex);
      return null;
    }
  }

/*--------------------------------------------------------------------------*/

  Future<LessonListModel> lessonList(
    String date,
    int id,
  ) async {
    Response response = await dio.get(
        "${KApiTexts.BASE_URL}lesson-lists-new?date=$date&id=${id.toString()}",
        options: Options(headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      LessonListModel listModel = LessonListModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return listModel;
    } else {
      return errorHandler(response);
    }
  }

/*--------------------------------------------------------------------------*/
  // Future<GetCoachAvailabilityModel> fetchCoachAvailability(
  //   int id,
  // ) async {
  //   Response response = await dio.get(
  //       "https://www.anytimepro.io/wp-json/wp/v2/coach-availability-list?coach_id=${id.toString()}",
  //       options: Options(headers: {
  //         "Content-Type": "application/json",
  //         'Authorization': 'Bearer $token',
  //       }));
  //   var _body = json.encode(response.data);
  //
  //   if (response.statusCode == 200) {
  //     GetCoachAvailabilityModel coachAvailabilityModel = GetCoachAvailabilityModel.fromJson(jsonDecode(_body));
  //     print("${response.statusCode}");
  //
  //     return coachAvailabilityModel;
  //   } else {
  //     return errorHandler(response);
  //   }
  // }

  Future<List<Slot>> fetchCoachAvailable(
    int id,
  ) async {
    try {
      Response response = await dio.get(
          "${KApiTexts.BASE_URL}coach-availability-list-new?coach_id=${id.toString()}",
          options: Options(headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $token",
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        final body = jsonDecode(_body);
        final Iterable json = body;
        if (body is List) {
          return json.map((availability) => Slot.fromJson(availability)).toList();
        } else {
          if (body['data'] == 'No found') {
            return [];
          }
        }
      } else {
        return errorHandler(response);
      }
    } catch (ex) {
      print(ex);
      return null;
    }
  }

/*--------------------------------------------------------------------------*/
  Future<List<ReviewModel>> fetchReview(int coachId) async {
    try {
      Response response =
          await dio.post("${KApiTexts.BASE_URL}${KApiTexts.fetchReview}",
              data: {"coach_id": coachId},
              options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        final body = jsonDecode(_body);
        final Iterable json = body;
        return json.map((review) => ReviewModel.fromJson(review)).toList();
      } else {
        return null;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

/*--------------------------------------------------------------------------*/
  Future<String> setReview(
      int playerId, int coachId, String reviewData, String rating) async {
    try {
      Response response =
          await dio.post("${KApiTexts.BASE_URL}${KApiTexts.setReview}",
              data: {
                "coach_id": coachId,
                "player_id": playerId,
                "review": reviewData,
                "rating": rating,
              },
              options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        final body = jsonDecode(_body)['message'];
        if (body == "Review has been posted") {
          return "Success";
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

/*--------------------------------------------------------------------------*/

  Future<BookLessonModel> bookCoach(
      coachID, date, slot, playerID, amount) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.bookLesson}",
            data: {
              "coach": coachID.toString(),
              "date": date,
              "slot": slot,
              "player": playerID.toString(),
              "amount": amount
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      try {
      BookLessonModel bookLessonModel =
      BookLessonModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      sendPushNotification(int.parse(coachID.toString()), "New booking Request",
          "Player sent you new request.");
      return bookLessonModel;
    } catch(e) {
        errorSnackbar(jsonDecode(_body)["data"]);
        return BookLessonModel(status: 'fail');
      }
    } else {
      return errorHandler(response);
    }
  }

/*--------------------------------------------------------------------------*/

  Future<CoachLessonListModel> coachLessonList(date, id) async {
    String filterDate = "";

    DateTime inputDate = DateFormat("MM-dd-yyyy").parse(date);
    DateTime formattedCurrentDate = DateTime.parse(DateTime.now().toString().substring(0, 10));

    Map<String, dynamic> query = {
      "id": id
    };

    if (!inputDate.isAtSameMomentAs(formattedCurrentDate)) {
      filterDate = DateFormat("M-dd-yyyy").format(inputDate);
      query = {
        "date": "", "filter_date": filterDate, "id": id
      };
    }

    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.coachLessonList}",
            data: query,
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      CoachLessonListModel coachLessonListModel =
          CoachLessonListModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      return coachLessonListModel;
    } else {
      return errorHandler(response);
    }
  }

  /*--------------------------------------------------------------------------*/

  //
  // Future<CoachLessonListModel> coachLessonList(date, id) async {
  //   Response response =
  //   await dio.post("${KApiTexts.BASE_URL}${KApiTexts.voucherPayment}",
  //       data: {"date":"","filter_date": date, "id": id},
  //       options: Options(headers: {
  //         "Content-Type": "application/json",
  //         'Authorization': 'Bearer $token',
  //       }));
  //   var _body = json.encode(response.data);
  //   if (response.statusCode == 200) {
  //     CoachLessonListModel coachLessonListModel =
  //     CoachLessonListModel.fromJson(jsonDecode(_body));
  //     print("${response.statusCode}\n${response.data}");
  //     return coachLessonListModel;
  //   } else {
  //     return errorHandler(response);
  //   }
  // }
  /*--------------------------------------------------------------------------*/

  Future<LessonActionModel> lessonAction(lessonId, action, playerId) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.lessonAction}",
            data: {
              "post_id": lessonId,
              "action": action,
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      LessonActionModel actionModel =
          LessonActionModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      sendPushNotification(
          int.parse(playerId.toString()),
          "Booking Request Update",
          "Your Booking Request is ${action.toString()}.");
      return actionModel;
    } else {
      return errorHandler(response);
    }
  }

  /*--------------------------------------------------------------------------*/

  Future<CoachAvailabilityModel> postCoachAvailability(
      id, dateList, slotList) async {
    try {
      var dateListData = dateList.toString();
      String dateDataList =
          ((dateListData.split(" ").join("")).split("[").join(""))
              .split("]")
              .join("");
      var slotListData = slotList.toString();
      String slotDataList =
          ((slotListData.split(" ").join("")).split("[").join(""))
              .split("]")
              .join("");
      print(slotDataList);
      var formData = FormData.fromMap(
          {"date": dateDataList, "time_slots": slotDataList, "coach_id": 73});
      print("List Of Dates" + dateList.toString());
      Response response = await dio.post(
          "${KApiTexts.BASE_URL}${KApiTexts.postCoachAvailability}",
          data: formData,
          options: Options(headers: {
            'Authorization': 'Bearer $token',
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        CoachAvailabilityModel availabilityModel =
            CoachAvailabilityModel.fromJson(jsonDecode(_body));
        print("${response.statusCode}\n${response.data}");
        return availabilityModel;
      } else {
        return errorHandler(response);
      }
    } catch (ex) {
      print(ex);
    }
  }

  Future<bool> postSetCoachAvailability(
      NewSetCoachAvailabilityModel setSlotDataModel) async {
    var data2 = json.encode(setSlotDataModel);
    var data = setSlotDataModel.toJson();
    //var data2= jsonDecode(setSlotDataModel.toString());
    try {
      Response response = await dio.post(
          "${KApiTexts.BASE_URL}${KApiTexts.postCoachAvailability}",
          // data:{
          //       "coach_id": setSlotDataModel.coachId,
          //       "slots":setSlotDataModel.slots
          //     }
          data: data2, //setSlotDataModel,
          options: Options(headers: {
            'Authorization': 'Bearer $token',
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        var data = jsonDecode(_body);
        print(response.statusMessage);
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      print(ex);
      return null;
    }
  }

  /*--------------------------------------------------------------------------*/

  Future<PaymentModel> addPayment(int id, String playerEmail,
       int amount, int uID, String coachId) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.addPayments}",
            data: {
              "player": id,
              "player_id": playerEmail,
              // "stripe_card_token": stripeCardToken,
              "amount": amount,
              "post_id": uID,
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      PaymentModel paymentModel = PaymentModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}");
      sendPushNotification(int.parse(id.toString()), "Payment Is Sent.",
          "Your Booking Payment Is Paid Successfully");
      sendPushNotification(int.parse(coachId), "Payment is received.",
          "Your Booking Payment is received.");
      return paymentModel;
    } else {
      return errorHandler(response);
    }
  }

  /*--------------------------------------------------------------------------*/

  Future<PaymentModel> addPaymentWithVoucher(
      String voucher, int amount, int uID, int id, String coachId) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.voucherPayment}",
            data: {
              "voucher": voucher,
              "amount": amount,
              "post_id": uID,
            },
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      PaymentModel paymentModel = PaymentModel.fromJson(jsonDecode(_body));
      print("${response.statusCode}\n${response.data}\n${paymentModel.status}");
      sendPushNotification(int.parse(id.toString()), "Payment Is Sent.",
          "Your Booking Payment Is Paid Successfully");
      sendPushNotification(int.parse(coachId), "Payment is received.",
          "Your Booking Payment is received.");
      return paymentModel;
    } else {
      return errorHandler(response);
    }
  }

  /*--------------------------------------------------------------------------*/
  Future<List<AllSlotModel>> quickBookSLOTList(date, id) async {
    var formData = FormData.fromMap({"date": date, "coach_id": id});
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.quickBook}",
            // data: {'date': "28/4/2022", "coach": id},
            data: formData,
            options: Options(headers: {
              // "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      if (response.data != null) {
        final body = jsonDecode(_body);
        final Iterable json = body;
        return json.map((slot) => AllSlotModel.fromJson(slot)).toList();
      } else {
        return null;
      }
    } else {
      return errorHandler(response);
    }
  }

  /*--------------------------------------------------------------------------*/

  ///=========  All Slots with availability API============///
  Future<List<AllSlotModel>> allAvailableSLOT(date) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.getAllSlots}",
            data: {'date': date},
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    var _body = json.encode(response.data);
    if (response.statusCode == 200) {
      final body = jsonDecode(_body);
      final Iterable json = body;
      return json.map((slot) => AllSlotModel.fromJson(slot)).toList();
    } else {
      return errorHandler(response);
    }
  }

  /// Screen Recording on GCP
  Future<String> storeScreenOnGcp(String videoBase64, String playerId) async {
    try {
      var userId = storage.read('id');
      int tempPlayerId = int.parse(playerId);
      bool isShow = false;
      Con.RxInt count = 0.obs;

      Response response =
          await dio.post("https://www.anytimepro.io/google-cloud/index.php",
              // "https://anytime.honesttech.co.uk/google-cloud/index.php",
              data: {
            'video': videoBase64,
            'coach_id': userId,
            'player_id': tempPlayerId,
          }, options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      var _body = json.encode(response.data);
      print("upload file body ${_body.toString()}");
      if (response.statusCode == 200) {
        var data = (jsonDecode(response.data)).toString();
        print("Success");
        successSnackbar("Uploaded");
        return data;
      } else {
        print((errorHandler(response)).toString());
        return null;
      }
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<VideoFolderListModel>> fetchVideoList() async {
    Response response;
    try {
      var userRole = storage.read('role');
      var userId = storage.read('id');
      response = await dio.post(
          // "https://www.anytimepro.io/wp-json/wp/v2/recordings-list",
          "${KApiTexts.BASE_URL}recordings-list",
          data: {
            'user_id': userId,
            'type': userRole,
          },
          options: Options(headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        final body = jsonDecode(_body);
        final Iterable json = body;
        return json
            .map((videoList) => VideoFolderListModel.fromJson(videoList))
            .toList();
        // VideoFolderListModel videoFolderListModel= VideoFolderListModel.fromJson(jsonDecode(_body));
        // return videoFolderListModel;
      } else {
        return errorHandler(response);
      }
    } catch (ex) {
      return errorHandler(response);
    }
  }

  Future<dynamic> cancelLesson(lessonId) async {
    Response response;
    try {
      var userRole = storage.read('role');
      var userId = storage.read('id');
      response = await dio.post(
          "${KApiTexts.BASE_URL}lesson-cancel",
          data: {
            'user_id': userId,
            'type': userRole,
            'lesson_id': lessonId
          },
          options: Options(headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        print("cancel lesson ${response.data}");
        return response.data;
      } else {
        return errorHandler(response);
      }
    } catch (ex) {
      return errorHandler(response);
    }
  }

  Future<List<VideoFolderListModel>> fetchVideosRelatedToCoachAndPlayerList(
      String agoraPlayerId) async {
    Response response;
    try {
      var userId = storage.read('id');
      response = await dio.post(
          // "https://www.anytimepro.io/wp-json/wp/v2/recordings-list",
          "${KApiTexts.BASE_URL}recordings-by-coach",
          data: {
            'coach_id': userId,
            'player_id': agoraPlayerId,
          },
          options: Options(headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          }));
      var _body = json.encode(response.data);
      if (response.statusCode == 200) {
        print("videos received");
        print(_body);
        final body = jsonDecode(_body);
        final Iterable json = body;
        return json
            .map((videoList) => VideoFolderListModel.fromJson(videoList))
            .toList();
      } else {
        return errorHandler(response);
      }
    } catch (ex) {
      return errorHandler(response);
    }
  }

  Future<String> getVideoUrl(String filePath) async {
    Response response;
    try {
      response = await dio.post(
          // "https://www.anytimepro.io/wp-json/wp/v2/file-download",
          "${KApiTexts.BASE_URL}file-download",
          data: {
            "file_path": filePath,
            "bucket": TPartyTokens.GoogleCloudStorageBucketName,
          },
          options: Options(headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          }));
      if (response.statusCode == 200) {
        var _body = json.encode(response.data);
        return _body;
      } else {
        return null;
      }
    } catch (ex) {
      return errorHandler(response);
    }
  }

  Future<String> getVideoByID(videoID, coachID) async {
    Response response =
        await dio.post("${KApiTexts.BASE_URL}${KApiTexts.getVideo}",
            data: {'video_id': videoID, 'coach_id': coachID},
            options: Options(headers: {
              "Content-Type": "application/json",
              'Authorization': 'Bearer $token',
            }));
    if (response.statusCode == 200) {
      return response.data['data'];
    } else {
      return errorHandler(response);
    }
  }

  Future<String> getLatestVideoFileName(int playerId) async {
    Response response;
    try {
      response = await dio.post(
          // "https://www.anytimepro.io/wp-json/wp/v2/get-file-name",
          "${KApiTexts.BASE_URL}get-file-name",
          data: {
            "player_id": playerId,
            "bucket": TPartyTokens.GoogleCloudStorageBucketName,
          },
          options: Options(headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          }));

      if (response.statusCode == 200) {
        var _body = json.encode(response.data);
        var videoId = jsonDecode(_body)['id'];
        var fileName = jsonDecode(_body)['file_name'];
        if (videoId != null && fileName != null) {
          return _body;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (ex) {
      print(errorHandler(response));
      return null;
    }
  }

  Future<void> setPlayerVideoPlay(
      bool setPlayerVideoPlay, String videoId, bool isEnd) async {
    int videoFlag;
    if (setPlayerVideoPlay) {
      videoFlag = 1;
    } else {
      videoFlag = 0;
    }
    Response response;
    try {
      response =
          await dio.post("${KApiTexts.BASE_URL}${KApiTexts.setVideoPlay}",
              data: {
                "file_id": videoId,
                "video_play": videoFlag,
                "player_play": isEnd,
              },
              options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      if (response.statusCode == 200) {
        var _body = json.encode(response.data);
        print("video flag set successfully");
      } else {
        print("video flag not set ");
        print((errorHandler(response)).toString());
      }
    } catch (ex) {
      print((errorHandler(response)).toString());
    }
  }

  ///Video Status
  // Future<void>getPlayerVideoStatus(String videoId)async{
  //   Response response;
  //   try{
  //     response= await dio.post(
  //         "${KApiTexts.BASE_URL}${KApiTexts.playerVideoStatus}",
  //         data: {
  //           "file_id":videoId
  //         },
  //         options: Options(headers: {
  //           "Content-Type": "application/json",
  //           'Authorization': 'Bearer $token',
  //         })
  //     );
  //     if(response.statusCode==200)
  //     {
  //       var _body= json.encode(response.data);
  //       print("video flag get successfully");
  //     }
  //     else{
  //       print("video flag is not fetched");
  //       print((errorHandler(response)).toString());
  //     }
  //   }catch(ex){
  //     print((errorHandler(response)).toString());
  //   }
  // }
  Future<String> getPlayerVideoStatus(String videoId) async {
    bool tempVideoStatus = true;
    Response response;
    try {
      response =
          await dio.post("${KApiTexts.BASE_URL}${KApiTexts.playerVideoStatus}",
              data: {"file_id": videoId},
              options: Options(headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer $token',
              }));
      if (response.statusCode == 200) {
        var _body = json.encode(response.data);
        // print(json.decode(response.data));
        //var body= (json.decode(response.data)).toString();

        print("video flag get successfully");
        if (_body != null && _body.isNotEmpty) {
          return _body;
        } else {
          return null;
        }
        // if(_body=="0"){
        //   tempVideoStatus=false;
        //   return tempVideoStatus;
        // }else{
        //   tempVideoStatus=true;
        //   return tempVideoStatus;
        // }

      } else {
        print("video flag is not fetched");
        print((errorHandler(response)).toString());
        return null;
      }
    } catch (ex) {
      print((errorHandler(response)).toString());
      return null;
    }
  }

  Future<void> updateWalkThroughFlag() async {
    Response response;
    var userId = storage.read('id').toString();
    storage.write('customFlag', '0');
    try {
      response = await dio.post(
          // "${KApiTexts.BASE_URL}${KApiTexts.userWalkThrough}",
          "https://www.anytimepro.io/wp-json/wp/v2/update-anytime-pro-user-custom-flag",
          data: {
            "user_id": userId.toString(),
            "user_custom_flag": "1",
          },
          options: Options(headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer $token',
          }));
      if (response.statusCode == 200) {
        print("User Tour View flag set successfully");
      } else {
        print("Tour View flag flag not set ");
        print((errorHandler(response)).toString());
      }
    } catch (ex) {
      print(ex);
      print((errorHandler(response)).toString());
    }
  }

  Future<meta.MetaData> getLessonCount(Map<String, String> queryP) async {
    var response = await dio.get('https://www.anytimepro.io/api',
        queryParameters: queryP,
        options: Options(
          headers: {"Content-Type": "application/json"},
        ));
    if (response.statusCode == 200 && response.data['status'] != 'error') {
      var userMeta = meta.MetaData.fromJson(response.data);
      return userMeta;
    } else {
      return null;
    }
  }

  Future<bool> updateLessonTakenCount(Map<String, dynamic> queryP) async {
    print("object $queryP");
    var response = await dio.post('https://www.anytimepro.io/api/user/update_user_meta_vars',
        queryParameters: queryP,
        options: Options(
          headers: {"Content-Type": "application/json"},
        ));
    if (response.statusCode == 200 && response.data['status'] != 'error') {
      print(response.data);
      return true;
    } else {
      return null;
    }
  }
}
