import 'package:flutter/material.dart';

Future cancelAlert(BuildContext context,onTap) {
  return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.all(
                Radius.circular(
                    25.0))),
        titlePadding:
        const EdgeInsets.only(
            top: 0.0,
            bottom: 0.0),
        title: Container(
          child: Padding(
            padding:
            const EdgeInsets
                .all(25.0),
            child: Column(
              children: [
                Text(
                  'Are you sure you want to cancel this lesson?',
                  textAlign:
                  TextAlign
                      .center,
                  style: TextStyle(
                      fontWeight:
                      FontWeight
                          .bold),
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
              onPressed: onTap,
              child: Text('Yes, cancel'),
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(18),
                ),
              )),
          ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('No, I\'ll keep it'),
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(18),
                ),
              )),
        ],
      ));
}