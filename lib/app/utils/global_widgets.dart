import 'dart:developer';

import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/login/controllers/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

class MainButton extends StatelessWidget {
  final EdgeInsetsGeometry padding;

  MainButton({
    this.title,
    this.onPress,
    this.buttonColor,
    this.padding = EdgeInsets.zero,
  });

  final String title;
  final VoidCallback onPress;
  final Color buttonColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? EdgeInsets.all(20.w),
      child: ConstrainedBox(
        constraints:
            BoxConstraints.tightFor(width: Get.width, height: Get.width / 7),
        child: ElevatedButton(
          onPressed: onPress,
          style: ElevatedButton.styleFrom(
            elevation: 0,
            // shape: RoundedRectangleBorder(
            //   borderRadius: BorderRadius.circular(5.0),
            // ),
            backgroundColor: buttonColor ?? KColors.lightBlue,
          ),
          child: Center(
            child: Text(
              title.toUpperCase(),
              style: TextStyle(
                  color: Colors.white,
                  letterSpacing: 0.7,
                  fontWeight: FontWeight.w400,
                  fontSize: 15.sp),
            ),
          ),
        ),
      ),
    );
  }
}

Widget buildInputEmailField(
        {textController,
        title,
        isIcon,
        keybrdType,
        FormFieldValidator<String> validator,
        onChange}) =>
    Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 5),
      child: TextFormField(
          inputFormatters: [
            FilteringTextInputFormatter.deny(RegExp('[ ]')),
          ],
          textCapitalization: TextCapitalization.none,
          controller: textController,
          onChanged: onChange,
          obscureText:
              isIcon ? LoginController.to.passwordVisibility.value : false,
          decoration: InputDecoration(
              errorStyle: KTextStyle.f16w5.copyWith(
                  color: Colors.redAccent,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.8,
                  fontSize: 15.sp),
              hintText: title,
              hintStyle: KTextStyle.f16w5.copyWith(
                  color: Colors.grey,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1),
              suffixIcon: isIcon
                  ? InkWell(
                      onTap: () =>
                          LoginController.to.passwordVisibility.toggle(),
                      child: Icon(
                        LoginController.to.passwordVisibility.value
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined,
                        color: Color(0xFF757575),
                        size: 22,
                      ),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              contentPadding: EdgeInsets.only(top: 20)),
          style: KTextStyle.f16w5.copyWith(
              color: KColors.lightBlue,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.8,
              fontSize: 18.sp),
          textAlign: TextAlign.start,
          maxLines: 1,
          obscuringCharacter: "*",
          cursorColor: KColors.darkBlue,
          textInputAction: TextInputAction.next,
          keyboardType: keybrdType,
          validator: validator),
    );

Widget buildInputField(
        {textController,
        title,
        isIcon,
        keyboardType,
        FormFieldValidator<String> validator,
        onChange,
        onTap,
        bool readOnly = false}) =>
    Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 5),
      child: TextFormField(
          enabled: title == 'Age' ? false : true,
          onTap: onTap,
          readOnly: readOnly,
          textCapitalization: TextCapitalization.none,
          controller: textController,
          onChanged: onChange,
          obscureText:
              isIcon ? LoginController.to.passwordVisibility.value : false,
          decoration: InputDecoration(
              errorStyle: KTextStyle.f16w5.copyWith(
                  color: Colors.redAccent,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.8,
                  fontSize: 15.sp),
              hintText: title,
              hintStyle: KTextStyle.f16w5.copyWith(
                  color: Colors.grey,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1),
              suffixIcon: isIcon
                  ? InkWell(
                      onTap: () =>
                          LoginController.to.passwordVisibility.toggle(),
                      child: Icon(
                        LoginController.to.passwordVisibility.value
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined,
                        color: Color(0xFF757575),
                        size: 22,
                      ),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              contentPadding: EdgeInsets.only(top: 20)),
          style: KTextStyle.f16w5.copyWith(
              color: KColors.lightBlue,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.8,
              fontSize: 18.sp),
          textAlign: TextAlign.start,
          maxLines: 1,
          obscuringCharacter: "*",
          cursorColor: KColors.darkBlue,
          textInputAction: TextInputAction.next,
          keyboardType: keyboardType,
          validator: validator),
    );

Widget buildInputTextArea(
        {textController,
        title,
        isIcon,
        keybrdType,
        FormFieldValidator<String> validator,
        onChange}) =>
    Padding(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 5),
      child: TextFormField(
          textCapitalization: TextCapitalization.none,
          controller: textController,
          onChanged: onChange,
          obscureText:
              isIcon ? LoginController.to.passwordVisibility.value : false,
          decoration: InputDecoration(
              errorStyle: KTextStyle.f16w5.copyWith(
                  color: Colors.redAccent,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.8,
                  fontSize: 15.sp),
              hintText: title,
              hintStyle: KTextStyle.f16w5.copyWith(
                  color: Colors.grey,
                  fontWeight: FontWeight.w600,
                  letterSpacing: 1),
              suffixIcon: isIcon
                  ? InkWell(
                      onTap: () =>
                          LoginController.to.passwordVisibility.toggle(),
                      child: Icon(
                        LoginController.to.passwordVisibility.value
                            ? Icons.visibility_outlined
                            : Icons.visibility_off_outlined,
                        color: Color(0xFF757575),
                        size: 22,
                      ),
                    )
                  : SizedBox(
                      height: 0,
                    ),
              contentPadding: EdgeInsets.only(top: 20)),
          style: KTextStyle.f16w5.copyWith(
              color: KColors.lightBlue,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.8,
              fontSize: 18.sp),
          textAlign: TextAlign.start,
          minLines: 1,
          maxLines: 8,
          keyboardType: TextInputType.multiline,
          obscuringCharacter: "*",
          cursorColor: KColors.darkBlue,
          textInputAction: TextInputAction.next,
          validator: validator),
    );

hideKeyboard(BuildContext context) => FocusScope.of(context).unfocus();

SnackbarController errorSnackbar(String msg, {int sec = 3}) {
  return Get.rawSnackbar(
    message: msg.toUpperCase(),
    borderRadius: 1,
    margin: EdgeInsets.all(KDynamicWidth.width20),
    isDismissible: true,
    dismissDirection: DismissDirection.horizontal,
    duration: Duration(seconds: sec),
    icon: Icon(
      Icons.dangerous,
      color: Colors.white,
    ),
    shouldIconPulse: true,
    mainButton: TextButton(
        onPressed: () {
          Get.back();
        },
        child: Text(
          "Dismiss",
          style: TextStyle(color: Colors.white),
        )),
    snackPosition: SnackPosition.TOP,
    backgroundColor: Colors.redAccent,
  );
}

successSnackbar(String msg) {
  return Get.rawSnackbar(
    message: msg.toUpperCase(),
    borderRadius: 10,
    margin: EdgeInsets.all(KDynamicWidth.width20),
    isDismissible: true,
    dismissDirection: DismissDirection.horizontal,
    icon: Icon(
      Icons.check_circle,
      color: Colors.white,
    ),
    shouldIconPulse: true,
    mainButton: TextButton(
        onPressed: () {
          Get.back();
        },
        child: Text(
          "Dismiss",
          style: TextStyle(color: Colors.white),
        )),
    snackPosition: SnackPosition.BOTTOM,
    backgroundColor: Colors.green,
  );
}

Container strip(String s, {Function() onclick}) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
    width: Get.width,
    height: Get.height * 0.05,
    decoration: BoxDecoration(
      color: KColors.lightBlue,
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          s.toUpperCase(),
          style: KTextStyle.f16w5.copyWith(color: Colors.white),
        ),
        InkWell(
          onTap: onclick,
          child: Text(
            'Edit'.toUpperCase(),
            style: KTextStyle.f16w5.copyWith(color: Colors.white),
          ),
        )
      ],
    ),
  );
}

Future<void> launchInBrowser(Uri url) async {
  if (!await launchUrl(
    url,
    mode: LaunchMode.externalApplication,
  )) {
    throw 'Could not launch $url';
  }
}


var alertStyle = AlertStyle(
  animationType: AnimationType.fromTop,
  isCloseButton: false,
  isOverlayTapDismiss: false,
  descStyle: TextStyle(fontWeight: FontWeight.bold),
  descTextAlign: TextAlign.start,
  animationDuration: Duration(milliseconds: 400),
  alertAlignment: Alignment.center,
);

Divider buildDivider() => Divider(
      thickness: 1,
      color: KColors.darkBlue,
    );

class CustomColumn extends StatelessWidget {
  CustomColumn({
    Key key,
    this.buttonName,
    this.heading,
    this.onPress,
    this.subfield,
  }) : super(key: key);

  final String heading, subfield, buttonName;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20.w, 8.w, 20.w, 4.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            heading,
            style: KTextStyle.f16w5.copyWith(fontSize: 18.sp),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            subfield.toUpperCase(),
            overflow: TextOverflow.ellipsis,
            style: KTextStyle.f14w4.copyWith(
                fontWeight: FontWeight.w600,
                color: KColors.lightBlue,
                fontSize: 12.sp),
          ),
        ],
      ),
    );
  }
}

class Loader extends StatelessWidget {
  const Loader({Key key, this.color}) : super(key: key);

  final Color color;

  @override
  Widget build(BuildContext context) {
    return SpinKitCircle(
      color: color ?? Colors.white,
    );
  }
}
