import 'package:anytime_pro/app/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

Future<void> customAlert(String title, String message, {bool isWaitForVideo = false}) {
  return Get.dialog(
    Stack(
      children: [
        Align(
          alignment: AlignmentDirectional(-0.05, 0),
          child: Container(

            width: Get.width * 0.8,
           // height: Get.height * 0.2,
            child: Material(
              borderRadius: BorderRadius.circular(15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(
                        0, Get.width * 0.23, 0, 0),
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: KTextStyle.f18w6.copyWith(fontSize: 20.sp),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12.0),
                    child: Text(
                      message,
                      textAlign: TextAlign.start,
                      style: KTextStyle.f18w6.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Align(
          alignment: AlignmentDirectional(0, -0.2),
          child: Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
              color: Color(0xFFDADADA),
              shape: BoxShape.circle,
            ),
            child: isWaitForVideo == true ?
                ClipOval(
  child: Image.asset('assets/images/giphy.gif', fit: BoxFit.fill,),
  )
                 :Icon(
              Icons.info_outline,
              color: Colors.white,
              size: 40,
            ),
          ),
        )
      ],
    ),
    barrierDismissible: !isWaitForVideo
  );
}
