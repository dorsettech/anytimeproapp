import 'package:anytime_pro/app/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class VideoCallStats extends StatelessWidget {
  const VideoCallStats(
      this.distance, this.ballSpeed, this.angle, this.clubSpeed,
      {Key key})
      : super(key: key);

  final String distance, ballSpeed, angle, clubSpeed;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.1,
        decoration: BoxDecoration(
          color: KColors.darkBlue,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildColumn('distance', distance, 'yards'),
            _buildColumn('ball speed', ballSpeed, 'mph'),
            _buildColumn('launch angle', angle, 'degree'),
            _buildColumn('club speed', clubSpeed, 'mph'),
          ],
        ),
      ),
    );
  }

  Column _buildColumn(String title, String val, String unitVal) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Text(
              val,
              style: KTextStyle.f18w6.copyWith(
                color: Colors.white,
              ),
            ),
            const SizedBox(width: 3),
            RotatedBox(
              quarterTurns: 3,
              child: Text(
                unitVal.toUpperCase(),
                style: KTextStyle.f18w6
                    .copyWith(color: Colors.white, fontSize: 6.sp),
              ),
            )
          ],
        ),
        const SizedBox(height: 5),
        Text(
          title.toUpperCase(),
          style: KTextStyle.f18w6
              .copyWith(color: Colors.white, fontSize: 8.sp, letterSpacing: 1),
        )
      ],
    );
  }
}
