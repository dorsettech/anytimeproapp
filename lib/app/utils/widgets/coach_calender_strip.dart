import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/Coaching/controllers/coaching_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

import '../calender_utils.dart';

class CalenderStripForCoachDash extends StatefulWidget {
  @override
  State<CalenderStripForCoachDash> createState() =>
      _CalenderStripForCoachDashState();
}

class _CalenderStripForCoachDashState extends State<CalenderStripForCoachDash> {
  final _c = Get.find<CoachingController>();
  DateTime _focusedDay = DateTime.now();
  CalendarFormat _calendarFormat = CalendarFormat.week;

  @override
  void initState() {
    _c.getDateandDayForCoachLessonList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: TableCalendar(
        daysOfWeekHeight: 20.0,
        availableGestures: AvailableGestures.horizontalSwipe,
        daysOfWeekStyle: DaysOfWeekStyle(
            weekdayStyle: KTextStyle.f16w5.copyWith(
                color: KColors.lightBlue, fontWeight: FontWeight.w600),
            weekendStyle: KTextStyle.f16w5.copyWith(
                color: KColors.starColor, fontWeight: FontWeight.bold)),
        calendarStyle: CalendarStyle(
            weekendTextStyle:
                KTextStyle.f18w6.copyWith(color: KColors.starColor),
            selectedDecoration: BoxDecoration(
                color: KColors.lightBlue, shape: BoxShape.circle)),
        headerStyle: HeaderStyle(
          titleCentered: true,
          formatButtonVisible: false,
          titleTextStyle: KTextStyle.f18w6
              .copyWith(color: KColors.lightBlue, fontSize: 20.sp),
        ),
        startingDayOfWeek: StartingDayOfWeek.monday,
        firstDay: kFirstDay,
        lastDay: kLastDay,
        focusedDay: _focusedDay,
        weekendDays: [7],
        calendarFormat: _calendarFormat,
        selectedDayPredicate: (day) {
          return isSameDay(_c.selectedDateDay, day);
        },
        onDaySelected: (selectedDay, focusedDay) {
          if (!isSameDay(_c.selectedDateDay, selectedDay)) {
            // Call `setState()` when updating the selected day
            setState(() {
              _c.selectedDateDay = selectedDay;
              _focusedDay = focusedDay;
              _c.getDateandDayForCoachLessonList();

              /// FOR CHANGING THE FORMAT OF DATE AND TIME
              _c.coachLessonList();

              /// For CAlling Upcoming Lessons
            });
          }
        },
        onFormatChanged: (format) {
          if (_calendarFormat != format) {
            setState(() {
              _calendarFormat = format;
            });
          }
        },
        onPageChanged: (focusedDay) {
          _focusedDay = focusedDay;
        },
      ),
    );
  }
}
