import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

class AuthController extends GetxController {
  static final _localAuth = LocalAuthentication();
  var hasFingerPrintLock = false.obs;
  var hasFaceLock = false.obs;
  var isUserAuthenticated = false.obs;

  static Future<bool> hasBiometrics() async {
    try {
      bool hasLocalAuth = await _localAuth.canCheckBiometrics;
      print(hasLocalAuth);
      if (hasLocalAuth) {
        List<BiometricType> _availableBiometrics =
            await _localAuth.getAvailableBiometrics();
        print(_availableBiometrics);
      }
      return hasLocalAuth;
    } on PlatformException catch (e) {
      print(e.toString());
      return false;
    }
  }

  @override
  void onInit() {
    hasBiometrics();
    super.onInit();
  }

  static Future<bool> authenticateUser() async {
    try {
      bool isUserAuthenticated = await _localAuth.authenticate(
          localizedReason: "Authenticate Yourself",
          options: const AuthenticationOptions(biometricOnly: false,stickyAuth: true,useErrorDialogs: true)
          // biometricOnly: false,
          // stickyAuth: true,
          // useErrorDialogs: true
          );
      return isUserAuthenticated;
    } on PlatformException catch (e) {
      if (e.code == auth_error.lockedOut) {
        errorSnackbar('Too Many Attempts,Try again later');
      } else if (e.code == auth_error.notEnrolled) {
        errorSnackbar('Fingerprint not enrolled');
      }
    }
    return false;
  }
}
