import 'package:anytime_pro/app/modules/landingPage/bindings/landing_binding.dart';
import 'package:anytime_pro/app/modules/landingPage/views/landing_view.dart';
import 'package:get/get.dart';

import 'package:anytime_pro/app/modules/Coaching/bindings/coaching_binding.dart';
import 'package:anytime_pro/app/modules/Coaching/views/coaching_view.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editGolfDetail/bindings/edit_golf_detail_binding.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editGolfDetail/views/edit_golf_detail_view.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editPayment/bindings/edit_payment_binding.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editPayment/views/edit_payment_view.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editProfile/bindings/edit_profile_binding.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editProfile/views/edit_profile_view.dart';
import 'package:anytime_pro/app/modules/bookingLesson/bindings/booking_lesson_binding.dart';
import 'package:anytime_pro/app/modules/bookingLesson/views/booking_lesson_view.dart';
import 'package:anytime_pro/app/modules/coachAvailability/bindings/coach_availability_binding.dart';
import 'package:anytime_pro/app/modules/coachAvailability/views/coach_availability_view.dart';
import 'package:anytime_pro/app/modules/coachDetails/bindings/coach_details_binding.dart';
import 'package:anytime_pro/app/modules/coachDetails/views/coach_details_view.dart';
import 'package:anytime_pro/app/modules/forgotPass/bindings/forgot_pass_binding.dart';
import 'package:anytime_pro/app/modules/forgotPass/views/forgot_pass_view.dart';
import 'package:anytime_pro/app/modules/home/bindings/home_binding.dart';
import 'package:anytime_pro/app/modules/home/views/home_view.dart';
import 'package:anytime_pro/app/modules/lessonBooked/bindings/lesson_booked_binding.dart';
import 'package:anytime_pro/app/modules/lessonBooked/views/lesson_booked_view.dart';
import 'package:anytime_pro/app/modules/loggedIn/bindings/logged_in_binding.dart';
import 'package:anytime_pro/app/modules/loggedIn/views/logged_in_view.dart';
import 'package:anytime_pro/app/modules/login/bindings/login_binding.dart';
import 'package:anytime_pro/app/modules/login/views/login_view.dart';
import 'package:anytime_pro/app/modules/marketPlace/bindings/market_place_binding.dart';
import 'package:anytime_pro/app/modules/marketPlace/views/market_place_view.dart';
import 'package:anytime_pro/app/modules/paymentPage/bindings/payment_page_binding.dart';
import 'package:anytime_pro/app/modules/paymentPage/views/payment_page_view.dart';
import 'package:anytime_pro/app/modules/practice/bindings/practice_binding.dart';
import 'package:anytime_pro/app/modules/practice/views/practice_view.dart';
import 'package:anytime_pro/app/modules/profile/bindings/profile_binding.dart';
import 'package:anytime_pro/app/modules/profile/views/profile_view.dart';
import 'package:anytime_pro/app/modules/selectCoaches/bindings/select_coaches_binding.dart';
import 'package:anytime_pro/app/modules/selectCoaches/views/select_coaches_view.dart';
import 'package:anytime_pro/app/modules/settings/bindings/settings_binding.dart';
import 'package:anytime_pro/app/modules/settings/views/settings_view.dart';
import 'package:anytime_pro/app/modules/signup/bindings/signup_binding.dart';
import 'package:anytime_pro/app/modules/signup/views/signup_view.dart';
import 'package:anytime_pro/app/modules/tracking/bindings/tracking_binding.dart';
import 'package:anytime_pro/app/modules/tracking/views/tracking_view.dart';
import 'package:anytime_pro/app/modules/videoCall/bindings/video_call_binding.dart';
import 'package:anytime_pro/app/modules/videoCall/views/video_call_view.dart';
import 'package:anytime_pro/app/modules/walkthrough/bindings/walkthrough_binding.dart';
import 'package:anytime_pro/app/modules/walkthrough/views/walkthrough_view.dart';


part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;
  static const ISLOGGEDIN = Routes.LOGGED_IN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.WALKTHROUGH,
      page: () => WalkthroughView(),
      binding: WalkthroughBinding(),
    ),
    GetPage(
      name: _Paths.COACHING,
      page: () => CoachingView(),
      binding: CoachingBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.MARKET_PLACE,
      page: () => MarketPlaceView(),
      binding: MarketPlaceBinding(),
    ),
    GetPage(
      name: _Paths.SETTINGS,
      page: () => SettingsView(),
      binding: SettingsBinding(),
    ),
    GetPage(
      name: _Paths.PRACTICE,
      page: () => PracticeView(),
      binding: PracticeBinding(),
    ),
    GetPage(
      name: _Paths.FORGOT_PASS,
      page: () => ForgotPassView(),
      binding: ForgotPassBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_PROFILE,
      page: () => EditProfileView(),
      binding: EditProfileBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_GOLF_DETAIL,
      page: () => EditGolfDetailView(),
      binding: EditGolfDetailBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_PAYMENT,
      page: () => EditPaymentView(),
      binding: EditPaymentBinding(),
    ),
    GetPage(
      name: _Paths.LOGGED_IN,
      page: () => LoggedInView(),
      binding: LoggedInBinding(),
    ),
    GetPage(
        name: _Paths.SELECT_COACHES,
        page: () => SelectCoachesView(),
        binding: SelectCoachesBinding(),
        transition: Transition.rightToLeft,
        transitionDuration: const Duration(milliseconds: 500)),
    GetPage(
        name: _Paths.LESSON_BOOKED,
        page: () => LessonBookedView(),
        binding: LessonBookedBinding(),
        transition: Transition.downToUp,
        transitionDuration: const Duration(milliseconds: 500)),
    GetPage(
        name: _Paths.BOOKING_LESSON,
        page: () => BookingLessonView(),
        binding: BookingLessonBinding(),
        transition: Transition.topLevel,
        transitionDuration: const Duration(milliseconds: 500)),
    GetPage(
      name: _Paths.COACH_DETAILS,
      page: () => CoachDetailsView(),
      binding: CoachDetailsBinding(),
    ),
    GetPage(
      name: _Paths.COACH_AVAILABILITY,
      page: () => CoachAvailabilityView(),
      binding: CoachAvailabilityBinding(),
    ),
    GetPage(
      name: _Paths.PAYMENT_PAGE,
      page: () => PaymentPageView(),
      binding: PaymentPageBinding(),
    ),
    GetPage(
      name: _Paths.VIDEO_CALL,
      page: () => VideoCallView(),
      binding: VideoCallBinding(),
    ),
    GetPage(
      name: _Paths.TRACKING,
      page: () => TrackingView(),
      binding: TrackingBinding(),
    ),
    GetPage(
      name: _Paths.Landing,
      page: () => LandingView(),
      binding: LandingBinding(),
    ),
  ];
}
