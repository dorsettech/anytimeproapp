part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const WALKTHROUGH = _Paths.WALKTHROUGH;
  static const COACHING = _Paths.COACHING;
  static const PROFILE = _Paths.PROFILE;
  static const MARKET_PLACE = _Paths.MARKET_PLACE;
  static const SETTINGS = _Paths.SETTINGS;
  static const PRACTICE = _Paths.PRACTICE;
  static const FORGOT_PASS = _Paths.FORGOT_PASS;
  static const EDIT_PROFILE = _Paths.EDIT_PROFILE;

  static const EDIT_GOLF_DETAIL = _Paths.EDIT_GOLF_DETAIL;
  static const EDIT_PAYMENT = _Paths.EDIT_PAYMENT;
  static const LOGGED_IN = _Paths.LOGGED_IN;
  static const SELECT_COACHES = _Paths.SELECT_COACHES;
  static const LESSON_BOOKED = _Paths.LESSON_BOOKED;
  static const BOOKING_LESSON = _Paths.BOOKING_LESSON;
  static const COACH_DETAILS = _Paths.COACH_DETAILS;
  static const COACH_AVAILABILITY = _Paths.COACH_AVAILABILITY;
  static const PAYMENT_PAGE = _Paths.PAYMENT_PAGE;
  static const VIDEO_CALL = _Paths.VIDEO_CALL;
  static const TRACKING = _Paths.TRACKING;
  static const Landing =  _Paths.Landing;
}

abstract class _Paths {
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const WALKTHROUGH = '/walkthrough';
  static const COACHING = '/coaching';
  static const PROFILE = '/profile';
  static const MARKET_PLACE = '/market-place';
  static const SETTINGS = '/settings';
  static const PRACTICE = '/practice';
  static const FORGOT_PASS = '/forgot-pass';
  static const EDIT_PROFILE = '/edit-profile';
  static const EDIT_GOLF_DETAIL = '/edit-golf-detail';
  static const EDIT_PAYMENT = '/edit-payment';
  static const LOGGED_IN = '/logged-in';
  static const SELECT_COACHES = '/select-coaches';
  static const LESSON_BOOKED = '/lesson-booked';
  static const BOOKING_LESSON = '/booking-lesson';
  static const COACH_DETAILS = '/coach-details';
  static const COACH_AVAILABILITY = '/coach-availability';
  static const PAYMENT_PAGE = '/payment-page';
  static const VIDEO_CALL = '/video-call';
  static const TRACKING = '/tracking';
  static const Landing = '/landing';
}
