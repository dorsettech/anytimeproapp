import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/bookingLesson/controllers/booking_lesson_controller.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/calender_utils.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

class BookingLessonView extends StatefulWidget {
  @override
  State<BookingLessonView> createState() => _BookingLessonViewState();
}

class _BookingLessonViewState extends State<BookingLessonView> {
  final _c = Get.put(BookingLessonController());

  DateTime _focusedDay = DateTime.now().add(Duration(hours: 1)).toUtc();
  CalendarFormat _calendarFormat = CalendarFormat.week;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lesson Date & Time'),
      ),
      body: ListView(
        physics: ScrollPhysics(),
        children: [
          Card(
              clipBehavior: Clip.antiAlias,
              elevation: 3,
              child: calenderStrip()),
          SizedBox(height: 30.w),
          _c.arg == 'quick'
              ? Obx(() => _c.loader.value
              ? Loader(color: KColors.lightBlue)
              : _c.slotListFromRes.isEmpty
              ? Center(
              child: Text(
                'Sorry Coach is not Available on selected Date\n Please Select another date',
                style: KTextStyle.f18w6,
                textAlign: TextAlign.center,
              ))
              : _buildTimeSlotList(_c.slotListFromRes))
              :Obx(() => _c.loader.value
              ? Loader(color: KColors.lightBlue)
              : _c.allSlotListRes == null || _c.allSlotListRes.isEmpty
              ? Center(
              child: Text(
                'Sorry Coach is not Available on selected Date\n Please Select another date',
                style: KTextStyle.f18w6,
                textAlign: TextAlign.center,
              ))
              :_buildAllTimeSlotList(_c.allSlotListRes)
          ),
          Padding(
            padding:
            EdgeInsets.symmetric(horizontal: 20.w, vertical: Get.width / 5),
            child: MainButton(
              title: _c.arg == 'quick' ? 'book ${_c.coachName}' : 'select coach',
              buttonColor: KColors.lightBlue,
              onPress: () {
                if (_c.slotFromList.value != null &&
                    _c.slotFromList.value != '') {
                  _c.arg == 'quick'
                      ? _c.quickBookCoach()
                      : Get.toNamed(Routes.SELECT_COACHES, arguments: [
                    _c.slotFromList.value,
                    _c.pickedDate,
                    _c.pickedDay
                  ]);
                } else {
                  errorSnackbar('Please Select slot first');
                }
              },
            ),
          )
        ],
      ),
    );
  }

  Padding _buildTimeSlotList(List<Object> list,) {
    return Padding(
      padding: EdgeInsets.all(8.w),
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1.7,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 4),
          // itemCount: slotDataListTest.length,
          itemCount: slotDataList.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (ctx, index) {
            if(_c.slotListFromRes[index].availability==true)
            {
              return InkWell(
                onTap: () {
                  setState(() {
                    try{
                      _c.selectIndex = index;
                      _c.slotFromList.value = _c.slotListFromRes[_c.selectIndex].slotTime;
                      print(_c.slotFromList.value);
                    }catch(ex){
                      print(ex);
                    }

                  });
                },
                child: Container(
                  color: _c.selectIndex == index
                      ? Colors.lightBlueAccent
                      : KColors.lightBlue,
                  child: Center(
                      child: Text(
                        _c.slotListFromRes[index].slotTime,
                        style: TextStyle(color: Colors.white),
                      )),
                ),
              );
            }
            else{
              return Container(
                color:  KColors.darkishGrey,
                child: Center(
                    child: Text(
                      _c.slotListFromRes[index].slotTime,
                      style: TextStyle(color: Colors.white),
                    )),
              );
            }
          }
      ),
    );
  }
  Padding _buildAllTimeSlotList(List<Object> list,) {
    return Padding(
      padding: EdgeInsets.all(8.w),
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: 1.7,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 4),
          // itemCount: slotDataListTest.length,
          itemCount: slotDataList.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (ctx, index) {
            if(_c.allSlotListRes[index].availability==true)
            {
              return InkWell(
                onTap: () {
                  setState(() {
                    try{
                      _c.selectIndex = index;
                      _c.slotFromList.value = _c.allSlotListRes[_c.selectIndex].slotTime;
                      print(_c.slotFromList.value);
                    }catch(ex){
                      print(ex);
                    }

                  });
                },
                child: Container(
                  color: _c.selectIndex == index
                      ? Colors.lightBlueAccent
                      : KColors.lightBlue,
                  child: Center(
                      child: Text(
                        _c.allSlotListRes[index].slotTime,
                        style: TextStyle(color: Colors.white),
                      )),
                ),
              );
            }
            else{
              return Container(
                color:  KColors.darkishGrey,
                child: Center(
                    child: Text(
                      _c.allSlotListRes[index].slotTime,
                      style: TextStyle(color: Colors.white),
                    )),
              );
            }
          }
      ),
    );
  }
  Widget calenderStrip() {
    return TableCalendar(
      daysOfWeekHeight: 20.0,
      daysOfWeekStyle: DaysOfWeekStyle(
          weekdayStyle: KTextStyle.f16w5
              .copyWith(color: KColors.lightBlue, fontWeight: FontWeight.w600),
          weekendStyle: KTextStyle.f16w5
              .copyWith(color: KColors.starColor, fontWeight: FontWeight.bold)),
      calendarStyle: CalendarStyle(
          weekendTextStyle: KTextStyle.f18w6.copyWith(color: KColors.starColor),
          selectedDecoration:
          BoxDecoration(color: KColors.lightBlue, shape: BoxShape.circle)),
      headerStyle: HeaderStyle(
        titleTextStyle: KTextStyle.f18w6.copyWith(color: KColors.lightBlue),
      ),
      startingDayOfWeek: StartingDayOfWeek.monday,
      weekendDays: [7],
      firstDay: kFirstDay,
      lastDay: kLastDay,
      focusedDay: _focusedDay,
      calendarFormat: _calendarFormat,
      selectedDayPredicate: (day) {
        return isSameDay(_c.selectedDateDay, day);
      },
      onDaySelected: (selectedDay, focusedDay) {
        if (!isSameDay(_c.selectedDateDay, selectedDay)) {
          // Call `setState()` when updating the selected day
          setState(() {
            _c.selectedDateDay = selectedDay;
            _focusedDay = focusedDay;
            _c.getCurrentDate();
            _c.arg == 'quick' ? _c.quickBookSLOTS() :_c.allAvailableSLOTS();

            /// FOR CHANGING THE FORMAT OF DATE AND TIME
          });
        }
      },
      onFormatChanged: (format) {
        if (_calendarFormat != format) {
          setState(() {
            _calendarFormat = format;
          });
        }
      },
      onPageChanged: (focusedDay) {
        _focusedDay = focusedDay;
      },
    );
  }
}