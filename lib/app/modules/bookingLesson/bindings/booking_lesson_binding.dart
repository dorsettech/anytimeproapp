import 'package:get/get.dart';

import '../controllers/booking_lesson_controller.dart';

class BookingLessonBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BookingLessonController>(
      () => BookingLessonController(),
    );
  }
}
