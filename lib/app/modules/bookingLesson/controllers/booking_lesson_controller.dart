import 'package:anytime_pro/app/data/ModelClass/all_slot_model.dart';
import 'package:anytime_pro/app/data/ModelClass/book_lesson_model.dart';
import 'package:anytime_pro/app/data/ModelClass/slot_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

class BookingLessonController extends GetxController {
  final _storage = GetStorage();

  DateTime selectedDateDay = DateTime.now().add(Duration(hours: 1)).toUtc();
  String pickedDate;
  String pickedDay;
  //var slotListFromRes = List<String>.empty(growable: true);
  List<AllSlotModel> slotListFromRes;
  List<AllSlotModel> allSlotListRes;
  final repo = Repository();
  RxBool loader = false.obs;
  RxBool slotEmpty = false.obs;
  int selectIndex;
  RxString slotFromList = ''.obs;
  String arg;
  String _coachId;
  String coachName,coachAmount;
  BookLessonModel bookLessonModel;

  @override
  void onInit() {
    try
    {
      getCurrentDate();
      arg = Get.arguments[0] ?? '';
      _coachId = Get.arguments[1] ?? '';
      coachName= Get.arguments[2] ?? 'Coach';
      coachAmount= Get.arguments[3]??'0';
      arg == 'quick' ? quickBookSLOTS() :  allAvailableSLOTS();
      super.onInit();
    }catch(ex){
      print(ex);
    }

  }

  getCurrentDate() {
    var dateParse = DateTime.parse(selectedDateDay.toString());
    //var formattedDate = "${dateParse.year}/${dateParse.month}/${dateParse.day}";
    var formattedDate = "${dateParse.month}/${dateParse.day}/${dateParse.year}";
    pickedDate = formattedDate.toString();
    print("Print date1:"+pickedDate);
    pickedDay = DateFormat('EEEE').format(selectedDateDay);
    print("Print date2:"+pickedDay);
  }

  Future<void> quickBookCoach() async {
    int _id = _storage.read('id');
    String x=slotFromList.value ;
    Get.showOverlay(
        asyncFunction: () async {
          try {
            await repo
                .bookLessonCoach(
                    coachID: int.parse(_coachId),
                    amount: coachAmount.toString(),
                    playerID: _id,
                    date: pickedDate,
                    slot: slotFromList.value)
                .then((BookLessonModel value) async {
              if (value.status == 'success') {
                bookLessonModel =value;
                successSnackbar('Booking Completed');
                Future.delayed(const Duration(seconds: 1),
                () => Get.toNamed(Routes.PAYMENT_PAGE,arguments:[coachName.toString(), bookLessonModel.data.postTitle, slotFromList.value, bookLessonModel.data.date, bookLessonModel.data.id,_coachId,bookLessonModel.data.amount]));
                     //() => Get.offAllNamed(Routes.HOME));
              } else {
                errorSnackbar('Please try again Later');
              }
            });
          } catch (e) {
            print(e);
          }
        },
        loadingWidget: Loader());
  }
/*---------------------------------------------QUICK BOOK SLOTS---*/

  Future<void> quickBookSLOTS() async {
    loader.value = true;
    try{
      await repo
          .quickBookSLOTS(date: pickedDate,coachId: int.parse(_coachId))
          .then((List<AllSlotModel> value) async {
        loader.value = false;
        if (value != null) {
          ///For removing duplicate slot response///
          slotListFromRes = value;
          if (slotListFromRes.isEmpty) {
            slotEmpty.value = true;
          }
        } else {
          errorSnackbar('Please try again Later');
        }
      }
      );
    }catch(ex){
      loader.value = false;
      print(ex);
    }
  }

  /// Fetching all Slots
  Future<void> allAvailableSLOTS() async {
    loader.value = true;
    try {
      await repo
          .allAvailableSLOTS(date: pickedDate)
          .then((List<AllSlotModel> value) async {
        loader.value = false;
        if (value != null) {
          ///For removing duplicate slot response///
          // allSlotListRes = value;
          // List<AllSlotModel> v = [];
          // slotDataListTest.forEach((element) { v.add(AllSlotModel(slotTime: element));});
          allSlotListRes = value;
          if (allSlotListRes.isEmpty) {
            slotEmpty.value = true;
          }
        } else {
          errorSnackbar('Please try again Later');
        }
      }
      );
    } catch (e) {
      print(e);
    }
  }

}
