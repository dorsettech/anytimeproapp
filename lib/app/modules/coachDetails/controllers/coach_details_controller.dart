import 'dart:io';

import 'package:anytime_pro/app/data/ModelClass/book_lesson_model.dart';
import 'package:anytime_pro/app/data/ModelClass/profile_model.dart';
import 'package:anytime_pro/app/data/ModelClass/review_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CoachDetailsController extends GetxController {
  int _coachId;
  final _storage = GetStorage();
  final repo = Repository();
  RxBool loader = false.obs;
  // var refreshController = RefreshController(initialRefresh: false);
  RxString dp = ''.obs;
  RxString coachName = ''.obs;
  RxString about =''.obs;
  RxString userRole1 = ''.obs;
  int coachId,userid;
  final storage = GetStorage();
  List<ReviewModel> reviewList =<ReviewModel>[].obs ;
  TextEditingController reviewTextController = TextEditingController();
  RxBool LoaderValue = false.obs;
  RxInt activeTabIndex = 1.obs;
  RxBool reviewTab = false.obs;
  final ScrollController controller = ScrollController();

  String slot;
  String date;
  String amount;
  BookLessonModel bookLessonModel;
  RxDouble avgReview=0.0.obs;
  RxString lastBooked = "".obs;
  // onCoachDetailRefresh() async {
  //   readInit();
  //   await Future.delayed(const Duration(milliseconds: 600));
  //   refreshController.refreshCompleted();
  //   refreshController = RefreshController(initialRefresh: false);
  //   refreshController.refreshCompleted();
  //
  // }

  Future<void> getCoachDetails() async {
    try {
      loader.value = true;
      await repo.getUserProfile(id: _coachId).then((ProfileModel value) async {
        loader.value = false;
        if (value.status == 'success') {
          if (value.data != null) {
            dp.value = value.data.avatar;
            coachName.value = "${value.data.firstName} ${value.data.lastName}";
            about.value= value.data.about;
            if (value.data.lastLessonDate != null) userJoinedOn(value.data.lastLessonDate);
            Future.delayed(Duration(milliseconds: 500)).then((value) => _scrollDown());
          }
        } else {
          errorSnackbar('Please try again Later');
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _scrollDown() {
    controller.animateTo(
      controller.position.maxScrollExtent,
      duration: Duration(seconds: 2),
      curve: Curves.fastOutSlowIn,
    );
  }

   userJoinedOn(String lastBookedDate) {
     var dateTime = DateFormat("yyyy-MM-dd HH:mm:ss").parse(lastBookedDate, true);
     var date = dateTime.toLocal();
     Duration difference = DateTime.now().difference(date);

    if (difference.inDays != 0) {
      if (difference.inDays > 365) {
        int year = (difference.inDays / 365).ceil();
        if (year == 1) {
          lastBooked.value = "Last booked $year year ago";
        } else {
          lastBooked.value = "Last booked $year years ago";
        }
      } else {
        if (difference.inDays > 30) {
          int month = (difference.inDays / 30).ceil();
          if (month == 1) {
            lastBooked.value = "Last booked $month month ago";
          } else {
            lastBooked.value = "Last booked $month months ago";
          }
        } else {
          if (difference.inDays == 1) {
            lastBooked.value = "Last booked ${difference.inDays} day ago";
          } else {
            lastBooked.value = "Last booked ${difference.inDays} days ago";
          }
        }
      }
    } else if (difference.inHours != 0) {
      if (difference.inHours == 1) {
        lastBooked.value = "Last booked ${difference.inHours} hour ago";
      } else {
        lastBooked.value = "Last booked ${difference.inHours} hours ago";
      }
    } else if (difference.inMinutes != 0) {
      if (difference.inMinutes == 1) {
        lastBooked.value = "Last booked ${difference.inMinutes} minute ago";
      } else {
        lastBooked.value = "Last booked ${difference.inMinutes} minutes ago";
      }
    } else if (difference.inSeconds != 0) {
      if (difference.inSeconds == 1) {
        lastBooked.value = "Last booked ${difference.inSeconds} second ago";
      } else {
        lastBooked.value = "Last booked ${difference.inSeconds} seconds ago";
      }
    }
  }

  Future<void> _bookCoachLesson() async {
    var tempCoachId= coachId.toString();
    Future.delayed(
        const Duration(seconds: 1),
            () => Get.offNamed(Routes.PAYMENT_PAGE,arguments:[coachName.value.toString(), "", slot, date,tempCoachId,amount])
    );
  }

  @override
  void onInit() {
    super.onInit();
    _coachId = Get.arguments[0];
    coachId   = Get.arguments[0];
    userid = storage.read('id');
    date = Get.arguments[1];
    slot = Get.arguments[2];
    amount= Get.arguments[3];
    readInit();
  }

  @override
  void onReady() {
    super.onReady();
  }
  readInit() async {
    LoaderValue.value = true;
    userRole1.value = storage.read('role');
    getCoachDetails();
    fetchReview();
  }
  Future<dynamic> book() async {
    Get.showOverlay(
        asyncFunction: () => _bookCoachLesson(), loadingWidget: Loader());
  }

  Future<void> fetchReview() async {
    loader.value = true;
    try{
      if(coachId != null || coachId != ""){
        await repo.fetchReview(coachId).then((List<ReviewModel> value) async {
          print(value.length);
          if(value.length > 0){
            reviewList = value;
            loader.value = false;
            getAverageRating(reviewList);
          }else{
            reviewList = null;
            loader.value = false;
          }
          LoaderValue.value = false;
        });
      }else{
        reviewList = null;
        loader.value = false;
        LoaderValue.value = false;
      }
    }catch(e){
      print(e.toString());
    }
  }

  Future<bool> submitReview(String rating) async {
    try{
      if(coachId != null && coachId != "" && userid != null && userid != "" && reviewTextController.text != null && reviewTextController.text != "" ){
        LoaderValue.value = true;
        await repo.setReview(userid,coachId,reviewTextController.text,rating).then((value) async {
          if(value == "Success"){
            successSnackbar('Review Submitted');
            loader.value = false;
            readInit();
            reviewTab.value = false;
            reviewTextController.text = "";
            return true;
          }else{
            loader.value = false;
            return false;
          }
        });
      }else{
        loader.value = false;
        return false;
      }

    }catch(e){
      print(e.toString());
      return false;
    }
  }

  void getAverageRating(List<ReviewModel> reviewList) {
    RxDouble totalReview=0.0.obs;
    for(int i=0 ;i<reviewList.length;i++){
      totalReview+=double.parse(reviewList[i].rating);
    }
    avgReview.value=totalReview/reviewList.length;
    print(avgReview.toString());
  }
}
