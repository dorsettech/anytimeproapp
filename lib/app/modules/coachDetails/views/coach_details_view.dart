import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gcloud/storage.dart';

import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../constants.dart';
import '../controllers/coach_details_controller.dart';

class CoachDetailsView extends StatefulWidget {
    @override
    State<CoachDetailsView> createState() => _CoachDetailsViewState();
}

class _CoachDetailsViewState extends State<CoachDetailsView> with TickerProviderStateMixin{
  final _cDetail = Get.put(CoachDetailsController());
  TabController _tabController;
  double _rating=0.0;


  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      initialIndex: 1,
      length: 2,
      vsync: this,
    );
    _tabController.addListener(_setActiveTabIndex);
  }

  void _setActiveTabIndex() {
    setState(() {
      _cDetail.activeTabIndex.value = _tabController.index;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var refreshController = RefreshController(initialRefresh: false);

    onCoachDetailRefresh() async {
      _cDetail.readInit();
      await Future.delayed(const Duration(milliseconds: 600));
      refreshController.refreshCompleted();
      refreshController = RefreshController(initialRefresh: false);
      refreshController.refreshCompleted();

    }

    return Scaffold(
      persistentFooterButtons: [
        Obx(()
            {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: MainButton(
                  title: _cDetail.coachName.value.isNotEmpty ?"Book ${_cDetail.coachName.value}":"Book Coach",
                  buttonColor: (_cDetail.date=="" && _cDetail.slot=="" && _cDetail.amount=="")?KColors.DARK_GREY:KColors.lightBlue,
                  onPress: () {
                    (_cDetail.date==""&&_cDetail.slot==""&&_cDetail.amount=="")?{}:_cDetail.book();
                  },
                ),
              );
            }
        )
      ],
      appBar: AppBar(
        title: Text('Coach Details'),
      ),
      body: SmartRefresher(
        controller: refreshController,
        onRefresh: onCoachDetailRefresh,
        header: BezierCircleHeader(
          bezierColor: KColors.lightBlue,
        ),
        physics: BouncingScrollPhysics(),
        child: ListView(
          controller: _cDetail.controller,
          padding: EdgeInsets.only(bottom: Get.height * 0.02),
          shrinkWrap: true,
          children: [
            Obx(() {
              return Container(
                height: Get.height * 0.35,
                width: Get.width,
                child: _cDetail.loader.value
                    ? Loader(
                        color: KColors.lightBlue,
                      )
                    : CachedNetworkImage(
                        fit: BoxFit.fitHeight,
                        imageUrl: _cDetail.dp.value,
                        placeholder: (ctx, _) => Loader(
                          color: KColors.lightBlue,
                        ),
                        errorWidget: (ctx, _, i) => Icon(Icons.error),
                      ),
              );
            }),
            Container(
              color: KColors.lightishGrey,
              height: Get.height * 0.09,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  // buildPadding('list', Icons.list),
                  Spacer(),
                  // buildPadding('basic', Icons.list),
                  buildPadding('share', Icons.ios_share),
                  buildPadding('wishlist', Icons.favorite_border_outlined),
                ],
              ),
            ),
            SizedBox(height: 10.w),
            Container(
              padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Obx(() {
                    return Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: Get.width * 0.7,
                            child: Text(
                              _cDetail.coachName.value.toUpperCase() ?? '',
                              style: KTextStyle.f18w6.copyWith(fontSize: 20.sp),
                            ),
                          ),
                          Text(
                            _cDetail.lastBooked.value,
                            style: KTextStyle.f16w5
                                .copyWith(color: KColors.darkishGrey),
                          )
                        ]);
                  }),
                  Spacer(),
                  Obx(() {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _cDetail.avgReview.value==0.0?Container():Wrap(children: [
                          Icon(Icons.star, color: KColors.starColor),
                          Text(
                            _cDetail.avgReview.value==0.0?"":_cDetail.avgReview.value.toStringAsFixed(1),
                            style: KTextStyle.f18w6.copyWith(fontSize: 25.sp),
                          )
                        ],),
                      ],
                    );
                  }),
                ],
              ),
            ),
            SizedBox(height: Get.height * 0.05),
            Obx((){
              return Container(
                height: Get.height * 0.3,
                child: DefaultTabController(
                  length: 2,
                  initialIndex: 1,
                  child: Column(
                    children: [
                      TabBar(
                        controller: _tabController,
                        isScrollable: true,
                        labelColor: KColors.lightBlue,
                        labelStyle:
                        KTextStyle.f16w5.copyWith(fontFamily: 'futura'),
                        indicatorColor: KColors.lightBlue,
                        tabs: [
                          Tab(
                            text: 'ABOUT',
                          ),
                          Tab(
                            text: 'REVIEW',
                          )
                        ],
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 18.0),
                              child: Card(
                                color: KColors.lightishGrey,
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: SingleChildScrollView(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        // Row(
                                        //   mainAxisSize: MainAxisSize.max,
                                        //   mainAxisAlignment:
                                        //   MainAxisAlignment.center,
                                        //   crossAxisAlignment:
                                        //   CrossAxisAlignment.center,
                                        //   children: [
                                        //     Spacer(),
                                        //     Text(
                                        //       'VERIFIED REVIEW',
                                        //       style: KTextStyle.f14w4.copyWith(
                                        //         fontWeight: FontWeight.w600,
                                        //       ),
                                        //     ),
                                        //     Icon(
                                        //       Icons.check_circle,
                                        //       color: KColors.starColor,
                                        //       size: 18,
                                        //     )
                                        //   ],
                                        // ),
                                        Text(
                                          _cDetail.coachName.value,
                                          style: KTextStyle.f14w4.copyWith(
                                              fontSize: 20.sp,
                                              color: KColors.darkishGrey),
                                        ),
                                        SizedBox(height: 20.w),
                                        Text(
                                          _cDetail.about.value.toUpperCase() ?? '',
                                          style: KTextStyle.f14w4.copyWith(
                                              fontSize: 16.sp,
                                              color: KColors.darkishGrey),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 18.0),
                              child: Card(
                                color: KColors.lightishGrey,
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child:
                                  //_cDetail.reviewList != null ?
                                _cDetail.LoaderValue.value ?
                                  Loader(color: KColors.lightBlue)
                                    : ( _cDetail.reviewList== null || _cDetail.reviewList.isEmpty) == false ?
                                    _listView(_cDetail.reviewList)
                                    : Center(
                                    child: Text('No Video Available',
                                        style: KTextStyle.f18w6)
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
            ),
            ///Rating Bar
            Obx(
                    (){
                  return _cDetail.activeTabIndex.value == 1 ?
                  _cDetail.reviewTab.value == true ?
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RatingBar.builder(
                          initialRating: 0,
                          minRating: 0,
                          direction: Axis.horizontal,
                          allowHalfRating: false,
                          unratedColor:KColors.lightBlue,
                          itemCount: 5,
                          itemSize: 40.0,
                          itemPadding: EdgeInsets.symmetric(horizontal: 3.0),
                          itemBuilder: (context, int i){
                            if(i < _rating){
                              return Icon(Icons.star,color: KColors.lightBlue);
                            }else{
                              return Icon(Icons.star_border, color: KColors.lightBlue);
                            }
                          },
                          onRatingUpdate: (rating) {
                            setState(() {
                              _rating = rating;
                              print("Rating"+_rating.toString());
                            });
                          },
                          updateOnDrag: true,
                        ),
                      ],
                    ),
                  )
                      :Container()
                      :Container();
                }
            ),
            Obx(
                (){
                  return _cDetail.activeTabIndex.value == 1 ?
                  _cDetail.reviewTab.value == true ?
                  Padding(
                    padding : EdgeInsets.only(left:20.w,right:20.w),
                    child: buildInputTextArea(
                        textController: _cDetail.reviewTextController,
                        title: 'Enter Your Review Here',
                        isIcon: false),
                  )
                      :Container()
                      :Container();
                }
            ),
            Obx((){
              return _cDetail.activeTabIndex.value == 1 ?
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _cDetail.reviewTab.value == true ?
                  InkWell(
                    onTap: (){
                      setState(() {
                        _cDetail.reviewTab.value = false;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: KColors.lightBlue,
                          borderRadius: BorderRadius.circular(3)
                      ),
                      height: 40,
                      margin: EdgeInsets.only(top: 10,bottom: 10,right: 22.w),
                      child: Padding(
                        padding: EdgeInsets.only(left:10.w,right: 10.w),
                        child: Center(
                            child: Text(
                              "Cancel",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w500,
                                  color:Colors.white
                              ),
                            )
                        ),
                      ),
                    ),
                  ):Container(),
                  InkWell(
                    onTap: (){
                      setState(() {
                        if(_cDetail.reviewTab.value == true){
                          if(_cDetail.reviewTextController.text != null && _cDetail.reviewTextController.text != "") {
                            setReview(_rating);
                            _rating=0.0;
                            _cDetail.reviewTextController.text="";
                          }
                        }else{
                          _cDetail.reviewTab.value = true;
                        }
                      });
                    },
                    child: (_cDetail.loader.value!=true && (_cDetail.date==""&&_cDetail.slot==""&&_cDetail.amount==""))?Container(
                      decoration: BoxDecoration(
                          color: KColors.lightBlue,
                          borderRadius: BorderRadius.circular(3)
                      ),
                      height: 40,
                      margin: EdgeInsets.only(top: 10,bottom: 10,right: 22.w),
                      child: Padding(
                        padding: EdgeInsets.only(left:10.w,right: 10.w),
                        child: Center(
                            child: Text(
                              _cDetail.reviewTab.value == true ?
                              "Submit"
                                  : _cDetail.reviewList == null ?
                              "Be the first to review this coach"
                                  : "Review This Coach",
                              style: TextStyle(
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.w500,
                                  color:Colors.white
                              ),
                            )
                        ),
                      ),
                    ):SizedBox(),

                  )
                ],
              )
                  :Container();
            })
          ],
        ),
      ),
    );
  }
  _listView(List list){
    return  ListView.builder(
      itemCount: list.length,
      itemBuilder: (context,index){
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment:
              MainAxisAlignment.center,
              crossAxisAlignment:
              CrossAxisAlignment.center,
              children: [
                Spacer(),
                Text(
                  'VERIFIED REVIEW',
                  style: KTextStyle.f14w4.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Icon(
                  Icons.check_circle,
                  color: KColors.starColor,
                  size: 18,
                )
              ],
            ),
            Row(
              children: [
            Container(
            width: Get.width * 0.5,
              child: Text(
                  list[index].review ?? "",
                  style: KTextStyle.f14w4.copyWith(
                      fontSize: 20.sp,
                      color: KColors.darkishGrey),
                )),
                Spacer(),
                customRating(list[index].rating.toString()),
              ],
            ),
            SizedBox(height: 20.w),
          ],
        );
      },
    );
  }
  Widget customRating(String rating){
    var i = double.parse(rating);
    return  Row(
      children: [
        i>=1?Icon(Icons.star, color: KColors.starColor,size: 17,):Container(),
        i>=2?Icon(Icons.star, color: KColors.starColor,size: 17,):Container(),
        i>=3?Icon(Icons.star, color: KColors.starColor,size: 17,):Container(),
        i>=4?Icon(Icons.star, color: KColors.starColor,size: 17,):Container(),
        i>=5?Icon(Icons.star, color: KColors.starColor,size: 17,):Container()
      ],
    );
  }
  buildPadding(String title, icon) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 18.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: KColors.darkishGrey,
          ),
          SizedBox(height: 5.w),
          Text(title.toUpperCase())
        ],
      ),
    );
  }

  setReview(double rating) async {
    await _cDetail.submitReview(rating.toString());
  }

}
