import 'package:get/get.dart';

import '../controllers/coaching_controller.dart';

class CoachingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CoachingController>(
      () => CoachingController(),
    );
  }
}
