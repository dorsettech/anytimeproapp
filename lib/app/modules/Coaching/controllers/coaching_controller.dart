import 'dart:convert';
import 'dart:io';

import 'package:anytime_pro/app/data/ModelClass/coach_lesson_list_model.dart';
import 'package:anytime_pro/app/data/ModelClass/home_model.dart';
import 'package:anytime_pro/app/data/ModelClass/login_model.dart' as meta;
import 'package:anytime_pro/app/data/ModelClass/lesson_action_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:anytime_pro/app/utils/widgets/custom_alart.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CoachingController extends GetxController {
  static CoachingController get to => Get.find<CoachingController>();
  final storage = GetStorage();

  DeviceCalendarPlugin deviceCalendarPlugin = DeviceCalendarPlugin();
  List<Calendar> calendars;
  Calendar selectedCalendar;

  var coachesList = <Coach>[];
  final repo = Repository();
  RxBool coachListIsloading = false.obs;
  RxBool lessonActionLoader = false.obs;
  final homeRefresh = RefreshController(initialRefresh: false);

  RxString userRole = ''.obs;
  RxString lessonCount = '0'.obs;

  /*--------Coach Dashboard --------------*/
  RxBool coachLsnListLoader = false.obs;
  List upLessonsList = <Lessons_upcomming>[];
  List schLessonList = <Lessons_past>[];

  final coachLessonRefresh = RefreshController(initialRefresh: false);
  DateTime selectedDateDay = DateTime.now().add(Duration(hours: 1)).toUtc();


  getRole() async {
    userRole.value = await storage.read('role');
    print(userRole.value);
    if (userRole.value != 'player') {
      coachLessonList();
    } else {
      getLessonCount();
      _fetchPlayerHome();
    }
  }

  Future<void> getLessonCount() async {
    Map<String, String> queryP = {
      'cookie': storage.read('cookie'),
      'json': 'user/get_user_meta',
    };

    meta.MetaData metaData = await repo.getLessonCount(queryP);
    print(metaData.lessonCount);
    if (metaData.lessonCount.isNotEmpty) {
      lessonCount.value = (jsonDecode(metaData.lessonCount) as Map).entries.length.toString();
    }
  }

  Future<void> _fetchPlayerHome() async {
    try {
      coachListIsloading.value = true;
      await repo.fetchHome(storage.read('token')).then((HomeModel value) async {
        coachListIsloading.value = false;
        if (value.status == 'success') {
          if (value.data.coachs != null) {
            coachesList = value.data.coachs;
          }
        } else {
          errorSnackbar('Please try again Later');
        }
      });
    } catch (e) {
      print(e);
    }
  }

  /*---------------------------------COACH DASHBOARD-----------------------------------------*/
  Future<void> coachLessonList() async {
    int _id = storage.read('id');
    coachLsnListLoader.value = true;
    try {
      await repo
          .coachLessonList(id: _id, date: getDateandDayForCoachLessonList())
          .then((CoachLessonListModel value) async {
        coachLsnListLoader.value = false;
        if (value.status == 'success') {
          if (value.data != null) {
            upLessonsList = value.data.lessonsUpcomming;
            // schLessonList = value.data.lessonsPast;
          }
        } else {
          print('status is not success');
        }
      });
    } catch (e) {
      print(e);
    }
  }

  String getDateandDayForCoachLessonList() {
    var dateParse = DateTime.parse(selectedDateDay.toString());
    var formattedDate = "${dateParse.month}-${dateParse.day}-${dateParse.year}";
    // var formattedDate = "${dateParse.day}-${dateParse.month}-${dateParse.year}";
    String finalDate = formattedDate.toString();
    return finalDate;
  }

  Future<void> lessonActionApi(String action, int lessonId, String playerId) async {
    Get.showOverlay(
        asyncFunction: () async {
          try {
            await repo
                .lessonAction(action: action, lessonId: lessonId,playerId: playerId)
                .then((LessonActionModel value) async {
              if (value.status == 'Success') {
                await coachLessonList();
              }
            });
          } catch (e) {
            print(e);
          }
        },
        loadingWidget: Loader());
  }

  ///For video call start onjOin will be called
  Future<void> videoCallJoin(playerId, playerFirstName, slot, date) async {
/*

    await [Permission.microphone, Permission.camera,Permission.storage,Permission.bluetooth,Permission.bluetoothConnect].request();
    Map<Permission, PermissionStatus> statuses = await [
      Permission.microphone,
      Permission.camera,
      Permission.storage,
      Permission.bluetooth,
      Permission.bluetoothConnect,
      Permission.manageExternalStorage
    ].request();
    print(statuses[Permission.storage]);
    print("External Storage${statuses[Permission.manageExternalStorage]}");
    Get.toNamed(Routes.VIDEO_CALL,arguments: [playerId,playerFirstName,""]);*/
    var inputFormat= DateFormat("M-dd-yyyy");
    var myTime =inputFormat.parse(date);
    var dateTime= DateTime.now().add(Duration(hours: 1)).toUtc();
    var currentDateTime= inputFormat.parse(inputFormat.format(dateTime));
    if(currentDateTime.isBefore(myTime)){
      customAlert('Sorry', 'Your Joining Before Session');
    }else if(currentDateTime.isAfter(myTime)){
      customAlert('Sorry', 'Your Joining After Session');
    }else if(currentDateTime.isAtSameMomentAs(myTime)){
      print("Equal  Time");
      var currentHour=int.parse(dateTime.hour.toString());
      var splitSlot= slot.toString().split("-");
      print("Slot String time:${splitSlot[0]}\nSlot Ending time:${splitSlot[1]}");
      var stringTime= int.parse(splitSlot[0].split(":")[0]);
      var endingTime= int.parse(splitSlot[1].split(":")[0]);
      var endingMinute= int.parse(splitSlot[1].split(":")[1]);

      var currentTime=int.parse(dateTime.minute.toString());
      var isTenMinutesBefore = currentTime >= 50;
      var isPreviousHourCorrect = currentHour + 1 == stringTime;


      if((currentHour >= stringTime && currentHour == endingTime && currentTime < endingMinute) || (isTenMinutesBefore && isPreviousHourCorrect && currentHour <= endingTime)){
        print("Success");
        await [Permission.microphone, Permission.camera,Permission.storage,Permission.bluetooth,Permission.bluetoothConnect].request();
        Get.toNamed(Routes.VIDEO_CALL,arguments: [playerId,playerFirstName,""]);
      }else{
        customAlert('Sorry', 'Not Joining On Schedule Time');
      }
    }

  }
  Future<void> trackingCamera() async {
    await [Permission.microphone, Permission.camera].request();

    Get.toNamed(Routes.TRACKING);
  }

  void _retrieveCalendars() async {
    try {
      var permissionsGranted = await deviceCalendarPlugin.hasPermissions();
      if (permissionsGranted.isSuccess && !permissionsGranted.data) {
          permissionsGranted = await deviceCalendarPlugin.requestPermissions();
          if (!permissionsGranted.isSuccess || !permissionsGranted.data) {
            openAppSettings();
            return;
          }
        }


      final calendarsResult = await deviceCalendarPlugin.retrieveCalendars();
      calendars = calendarsResult?.data;
    } catch (e) {
      print(e);
    }
  }

  Future addEventsToCalendar(Event eventToCreate) async {
    var c = await deviceCalendarPlugin.retrieveCalendars();
      var id = '';
      deviceCalendarPlugin.hasPermissions().then((value) => {});
      for (var element in c.data) {
        if (element.isDefault == true) {
          id = element.id;
        }
      }
      final calEvents = await deviceCalendarPlugin.retrieveEvents(id, RetrieveEventsParams(startDate: eventToCreate.start, endDate: eventToCreate.end));
      if (calEvents.data.isNotEmpty &&
          calEvents.data.first.start.isAtSameMomentAs(DateTime.parse(eventToCreate.start.toString())) &&
          calEvents.data.first.end.isAtSameMomentAs(DateTime.parse(eventToCreate.end.toString()))) {
        return;
      }
      eventToCreate.calendarId = id;
      final createEventResult =
      await deviceCalendarPlugin.createOrUpdateEvent(eventToCreate);
      if (createEventResult.isSuccess &&
          (createEventResult.data?.isNotEmpty ?? false)) {
      }
  }



  @override
  void onInit() {
    getRole();
    _retrieveCalendars();
    super.onInit();
  }

  onHomeRefresh() async {
    _fetchPlayerHome();
    await Future.delayed(const Duration(milliseconds: 1000));
    homeRefresh.refreshCompleted();
  }

  onCoachLessonRefresh() async {
    coachLessonList();
    _retrieveCalendars();
    await Future.delayed(const Duration(milliseconds: 1000));
    coachLessonRefresh.refreshCompleted();
  }
}
