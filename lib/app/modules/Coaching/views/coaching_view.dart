import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'package:animate_do/animate_do.dart';
import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/cancel_alert.dart';

import 'package:anytime_pro/app/utils/global_widgets.dart';

import 'package:anytime_pro/app/utils/widgets/coach_calender_strip.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../data/socket.dart';
import '../controllers/coaching_controller.dart';
import 'package:timezone/timezone.dart';

class CoachingView extends StatefulWidget {
  @override
  State<CoachingView> createState() => _CoachingViewState();
}

class _CoachingViewState extends State<CoachingView> {
  final _c = Get.put(CoachingController());
  IO.Socket socket;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    socket = SocketIOUser.socket;
    socket.connect();

    socket.onConnect((_) {
      print('connect to socket');
    });

    socket.on('cancel_lesson', (data) async {
      if (data['type'] == 'player') {
        _c.coachListIsloading.value = true;
          _c.upLessonsList.removeWhere((element) => element.id == data['lesson_id']);

          var c = await _c.deviceCalendarPlugin.retrieveCalendars();
          var id = '';
          _c.deviceCalendarPlugin.hasPermissions().then((value) => {});
          for (var element in c.data) {
            if (element.isDefault == true) {
              id = element.id;
            }
          }

          final calEvents = await _c.deviceCalendarPlugin.retrieveEvents(id, RetrieveEventsParams(startDate: data['start_date'], endDate: data['end_date']));
          if (calEvents.data.isNotEmpty) {
            _c.deviceCalendarPlugin.deleteEvent(calEvents.data.first.calendarId, calEvents.data.first.eventId);
            }
          _c.coachListIsloading.value = false;
      }});
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion(
      value: SystemUiOverlayStyle.light
          .copyWith(statusBarColor: KColors.lightBlue),
      child: Scaffold(
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButton: Obx(() => _c.userRole.value == 'coach'
            ? FloatingActionButton.extended(
                onPressed: () => Get.toNamed(Routes.COACH_AVAILABILITY),
                icon: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 30, height: 30),
                    child: Lottie.asset('assets/jsonFiles/avail.json')),
                label: Text('My Availability'))
            : SizedBox()),
        body: Obx(
          () => Column(
            children: [
              _customAppBar(),
              _c.userRole.value == 'coach'
                  ? _coachDashBoard()
                  : _playerDashboard(),
            ],
          ),
        ),
      ),
    );
  }

  _customAppBar() {
    return Container(
      width: Get.width,
      height: Get.height / 6,
      color: KColors.lightBlue,
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/mini-logo.jpg',
              height: 50,
            ),
            SizedBox(height: 10.w),
            Text(
                _c.userRole.value == 'coach'
                    ? "Coaching"
                    : 'Welcome ${_c.storage.read('name')} !'.capitalize,
                style: KTextStyle.f18w6.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.6,
                  fontFamily: 'futura',
                )),
          ],
        ),
      ),
    );
  }

  Expanded _playerDashboard() {
    return Expanded(
      child: SmartRefresher(
        controller: _c.homeRefresh,
        onRefresh: _c.onHomeRefresh,
        header: BezierCircleHeader(
          bezierColor: KColors.lightBlue,
          rectHeight: 50,
        ),
        physics: BouncingScrollPhysics(),
        child: ListView(
          shrinkWrap: true,
          children: [
            playerHeader(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
              child: Text('QUICK BOOK', style: KTextStyle.f18w6),
            ),
            SizedBox(height: KDynamicWidth.width20),
            _c.coachListIsloading.value
                ? SpinKitCircle(
                    color: KColors.lightBlue,
                  )
                : _c.coachesList.isEmpty
                    ? SizedBox()
                    : _homeCoachLists(),
            SizedBox(height: Get.width / 8),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
              child: MainButton(
                buttonColor: KColors.lightBlue,
                onPress: () => Get.toNamed(Routes.LESSON_BOOKED),
                title: 'my lessons booked',
              ),
            ),
            SizedBox(height: KDynamicWidth.width20),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
              child: MainButton(
                buttonColor: KColors.darkBlue,
                onPress: () => Get.toNamed(Routes.BOOKING_LESSON,
                    arguments: ['', '', '', '']),
                title: 'book a lesson',
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _homeCoachLists() {
    return Container(
      height: Get.height / 4.9,
      width: Get.width,
      child: ListView.separated(
          padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
          shrinkWrap: true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: _c.coachesList.length,
          itemBuilder: (ctx, index) => Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () => Get.toNamed(
                      Routes.BOOKING_LESSON,
                      arguments: [
                        'quick',
                        _c.coachesList[index].id,
                        _c.coachesList[index].firstName +
                            " " +
                            _c.coachesList[index].lastName,
                        _c.coachesList[index].amount.toString()
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: CachedNetworkImage(
                        imageUrl: _c.coachesList[index].avatar,
                        width: Get.width / 3.6,
                        height: Get.width / 3.6,
                        placeholder: (ctx, _) =>
                            Loader(color: KColors.lightBlue),
                        errorWidget: (ctx, _, t) => Icon(Icons.error_outline),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(height: 10.w),
                  Text(
                    "${_c.coachesList[index].firstName} ${_c.coachesList[index].lastName}"
                        .capitalize,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    style: KTextStyle.f14w4
                        .copyWith(fontWeight: FontWeight.w600, fontSize: 16.sp),
                  )
                ],
              ),
          separatorBuilder: (BuildContext context, int index) =>
              SizedBox(width: KDynamicWidth.width20)),
    );
  }

  _coachDashBoard() => Expanded(
        child: SmartRefresher(
          controller: _c.coachLessonRefresh,
          onRefresh: _c.onCoachLessonRefresh,
          header: BezierCircleHeader(
            bezierColor: KColors.lightBlue,
          ),
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              CalenderStripForCoachDash(),
              // Expanded(
              //   child: DefaultTabController(
              //     length: 2,
              //     initialIndex: 0,
              //     child: Column(
              //       children: [
              //         TabBar(
              //           padding: EdgeInsets.only(left: KDynamicWidth.width20),
              //           labelPadding: EdgeInsets.only(
              //               left: KDynamicWidth.width20,
              //               right: KDynamicWidth.width20),
              //           isScrollable: true,
              //           labelStyle: KTextStyle.f18w6
              //               .copyWith(fontSize: 15.sp, fontFamily: 'futura'),
              //           labelColor: KColors.darkBlue,
              //           indicatorColor: KColors.lightBlue,
              //           indicatorWeight: 3,
              //           tabs: [
              //             Tab(
              //               text: 'Scheduled Lessons',
              //             ),
              //             Tab(
              //               text: 'Upcoming Lessons',
              //             )
              //           ],
              //         ),
              //         Expanded(
              //           child: Obx(() {
              //             return TabBarView(
              //               children: [
              //                 _c.coachLsnListLoader.value
              //                     ? Loader(color: KColors.lightBlue)
              //                     : _c.schLessonList.isNotEmpty
              //                         ? _coachLessonList(_c.schLessonList)
              //                         : Center(
              //                             child: Text('No Scheduled Lesson',
              //                                 style: KTextStyle.f18w6)),
              //                 _c.coachLsnListLoader.value
              //                     ? Loader(color: KColors.lightBlue)
              //                     : _c.upLessonsList.isNotEmpty
              //                         ? _coachLessonList(_c.upLessonsList)
              //                         : Center(
              //                             child: Text('No Upcoming Lesson',
              //                                 style: KTextStyle.f18w6)),
              //               ],
              //             );
              //           }),
              //         ),
              //
              //       ],
              //     ),
              //   ),
              // ),
              Expanded(
                child: Container(
                  width: Get.width,
                  child: _c.coachLsnListLoader.value
                      ? Loader(color: KColors.lightBlue)
                      : _c.upLessonsList.isNotEmpty
                          ? _c.coachListIsloading.value == true ? Loader(color: KColors.lightBlue) : _coachLessonList(_c.upLessonsList)
                          : Center(
                              child: Text('No Upcoming Lesson',
                                  style: KTextStyle.f18w6)),
                ),
              )
            ],
          ),
        ),
      );

  _coachLessonList(List list)  {

    return ListView.builder(
        itemCount: list.length,
        itemBuilder: (context, index) {

          var slotDate = list[index].date.toString().split("-");
          var slotTime = list[index].slot.toString().split("-");
          var year = slotDate.last;
          var month = slotDate.first;
          var day = slotDate[1];
          var startTime = slotTime.first;
          var endTime = slotTime.last.split(":");
          var endTimeHour = endTime.first;
          var endTimeMints = endTime.last;

          final eventToCreate = Event('1');
          eventToCreate.title = 'Anytime pro lesson';
          eventToCreate.start = TZDateTime.utc(int.parse(year), int.parse(month), int.parse(day), int.parse(startTime.split(":").first) - 1);
          eventToCreate.description = "Your Lesson with ${list[index].playerFirstName +" "+list[index].playerLastName} ";

          eventToCreate.end = TZDateTime.utc(int.parse(year), int.parse(month), int.parse(day), int.parse(endTimeHour) - 1, int.parse(endTimeMints));
          eventToCreate.reminders = [Reminder(minutes: 1440), Reminder(minutes: 10)];

          if (list[index].amount != null) {
            _c.addEventsToCalendar(eventToCreate);
          }

          return Card(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            color: Colors.white,
            shadowColor: KColors.lightBlue,
            margin: EdgeInsets.all(10.w),
            elevation: 3,
            child: Container(
              padding: EdgeInsets.all(12.w),
              // height: Get.height * 0.15,
              width: Get.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      CachedNetworkImage(
                        imageUrl: list[index].avatar,
                        width: Get.width * 0.14,
                        height: Get.height * 0.06,
                        fit: BoxFit.cover,
                        placeholder: (c, _) => Loader(color: KColors.lightBlue),
                        errorWidget: (ctx, _, t) => Icon(Icons.error_outline),
                      ),
                      SizedBox(height: 10.w),
                      Text(
                        '${list[index].playerFirstName} ${list[index].playerLastName}'
                            .capitalizeFirst,
                        style:
                            KTextStyle.f16w5.copyWith(color: KColors.darkBlue),
                      )
                    ],
                  ),
                  upComingListCol('Slot:', list[index].slot, list[index].date),
                  upComingListCol(
                      'Lesson:', list[index].postTitle, list[index].date),
                  Column(
                    children: [
                      _textButtons('Video', KColors.lightBlue,
                          onTap: () => _c.videoCallJoin(
                              list[index].playerId,
                              list[index].playerFirstName,
                              list[index].slot,
                              list[index].date)),
                      SizedBox(height: 20.w),
                      _textButtons('Cancel', Colors.red,
                          onTap: () => cancelAlert(context, () async {
                            Navigator.pop(context);
                            _c.coachListIsloading.value = true;
                            var res = await _c.repo.cancelLesson(list[index].id);
                            if (res != null) {

                              var c = await _c.deviceCalendarPlugin.retrieveCalendars();
                              var id = '';
                              _c.deviceCalendarPlugin.hasPermissions().then((value) => {});
                              for (var element in c.data) {
                                if (element.isDefault == true) {
                                  id = element.id;
                                }
                              }
                              final calEvents = await _c.deviceCalendarPlugin.retrieveEvents(id, RetrieveEventsParams(startDate: eventToCreate.start, endDate: eventToCreate.end));

                              if (calEvents.data.isNotEmpty) {
                                _c.deviceCalendarPlugin.deleteEvent(calEvents.data.first.calendarId, calEvents.data.first.eventId);
                              }

                              var userRole = _c.storage.read('role');
                              var userId = _c.storage.read('id');
                              var data = {
                                'user_id': userId,
                                'type': userRole,
                                'lesson_id': list[index].id,
                                'start_date': eventToCreate.start.toIso8601String(),
                                'end_date': eventToCreate.end.toIso8601String(),
                              };
                              socket.emit('cancel_lesson', data);
                              list.remove(list[index]);
                            }
                            _c.coachListIsloading.value = false;
                          })),

                      /// Track Button
                      // _textButtons('Track', KColors.lightBlue,
                      //     onTap: () => _c.trackingCamera()),
                    ],
                  ),

                  /// Accept Reject Button
                  // list[index].isLesson == 'pending'
                  //     ? acceptRejectButtons(list, index)
                  //     : Column(
                  //         children: [
                  //           _textButtons('Video', KColors.lightBlue,
                  //               onTap: () => _c.videoCallJoin(list[index].playerId,list[index].playerFirstName)),
                  //           /// Track Button
                  //           // _textButtons('Track', KColors.lightBlue,
                  //           //     onTap: () => _c.trackingCamera()),
                  //         ],
                  //       ),
                ],
              ),
            ),
          );
        });
  }
  Column acceptRejectButtons(List<dynamic> list, int index) {
    return Column(children: [
      _textButtons('Accept', Colors.green, onTap: () {
        _c.lessonActionApi('accept', list[index].id, list[index].playerId);
        // _c.coachLessonList();
      }),
      _textButtons('Reject', Colors.red, onTap: () {
        _c.lessonActionApi('reject', list[index].id, list[index].playerId);
        //_c.coachLessonList();
      }),
    ]);
  }

  TextButton _textButtons(String s, color, {onTap}) {
    return TextButton(
      onPressed: onTap,
      child: Text(
        s,
        style: TextStyle(color: Colors.white),
      ),
      style: ElevatedButton.styleFrom(primary: color),
    );
  }

  upComingListCol(String _t, String _s, String _d) {
    var lessonDate = "";
    var lessonTime = "";
    var slotDate = "";
    var lessonTitle = "";
    if(_t == "Slot:") {
      var date = _d.split("-");
      slotDate = '${date[1]}-${date.first}-${date.last}';
    } else {
      var date = _s.split("(");
      var newDate = date.last.split("/");
      lessonTitle = date.first;
      var d = newDate[2].split(" ");
      lessonDate = '${newDate.first}-${newDate[1]}-${d.first}';
      lessonTime = d.last.split(")").first;
    }

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            _t.toUpperCase(),
            style: KTextStyle.f18w6.copyWith(color: KColors.darkishGrey , ),
          ),
          SizedBox(
            height: KDynamicWidth.width10,
          ),
          Text(
            _t == 'Slot:' ? _s : lessonTitle,
            style: KTextStyle.f16w5
                .copyWith(color: KColors.darkBlue, fontWeight: FontWeight.w600 ),
          ),
          Text(
            _t == 'Slot:' ? slotDate ?? "" : lessonDate,
                  style: KTextStyle.f16w5.copyWith(
                      color: KColors.darkBlue, fontWeight: FontWeight.w600),
                ),
          _t != 'Slot:'
              ?
          Text(
            lessonTime ?? "",
            style: KTextStyle.f16w5.copyWith(
                color: KColors.darkBlue, fontWeight: FontWeight.w600),
          )
          : Container()
        ],
      ),
    );
  }

  Widget playerHeader() => ElasticInDown(
        child: Container(
          padding: EdgeInsets.symmetric(
              horizontal: KDynamicWidth.width20, vertical: 30),
          width: Get.width,
          decoration: BoxDecoration(),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Obx(() => _headerContent(_c.lessonCount.value ?? '0', 'Lessons')),
              //Lesson Taken
              // _headerContent(_c.storage.read('handicap') ?? '0', 'Handicap'),
              _headerContent(_c.storage.read('curHandicap') ?? '0', 'Handicap'),
              _headerContent(
                  _c.storage.read('impFactor') ?? '0', 'Improvement\nFactor'),
            ],
          ),
        ),
      );
}

Column _headerContent(String s, String h) {
  return Column(
    mainAxisSize: MainAxisSize.max,
    children: [
      Text(
        s,
        style: KTextStyle.f18w6.copyWith(color: Colors.green, fontSize: 25.sp),
      ),
      const SizedBox(height: 10),
      Text(h.toUpperCase(),
          textAlign: TextAlign.center,
          style: KTextStyle.f13w5
              .copyWith(color: KColors.darkBlue, fontSize: 14.sp))
    ],
  );
}
