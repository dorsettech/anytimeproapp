import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/ModelClass/lesson_list_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/widgets/custom_alart.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../../../data/socket.dart';

class LessonBookedController extends GetxController {
  static LessonBookedController get to => Get.find<LessonBookedController>();

  final repo = Repository();
  var lessonList = <LessonResData>[];
  final storage = GetStorage();
  RxBool isLoading = false.obs;
  IO.Socket socket;

  //final lessonRefreshCtrl = RefreshController(initialRefresh: false);

  DeviceCalendarPlugin deviceCalendarPlugin = DeviceCalendarPlugin();
  List<Calendar> calendars;
  Calendar selectedCalendar;

  Future<void> getLessonList() async {
    isLoading.value = true;
    try {
      await repo
          .playerLessonList(
              id: storage.read('id'), date: _getCurrentDateforLessonList())
          .then((LessonListModel value) async {
        isLoading.value = false;
        if (value.status == 'success') {
          if (value.data != null) {
            lessonList = value.data;
          }
        } else {
          print('status is not success');
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void _retrieveCalendars() async {
    try {
      var permissionsGranted = await deviceCalendarPlugin.hasPermissions();
      if (permissionsGranted.isSuccess && !permissionsGranted.data) {
        permissionsGranted = await deviceCalendarPlugin.requestPermissions();
        if (!permissionsGranted.isSuccess || !permissionsGranted.data) {
          openAppSettings();
          return;
        }
      }

      final calendarsResult = await deviceCalendarPlugin.retrieveCalendars();
      calendars = calendarsResult?.data;
    } catch (e) {
      print(e);
    }
  }

  Future addEventsToCalendar(Event eventToCreate) async {
    var c = await deviceCalendarPlugin.retrieveCalendars();
      var id = '';
      for (var element in c.data) {
        if (element.isDefault == true) {
          id = element.id;
        }
      }
      final calEvents = await deviceCalendarPlugin.retrieveEvents(id, RetrieveEventsParams(startDate: eventToCreate.start, endDate: eventToCreate.end));
      if (calEvents.data.isNotEmpty &&
          calEvents.data.first.start.isAtSameMomentAs(DateTime.parse(eventToCreate.start.toString())) &&
          calEvents.data.first.end.isAtSameMomentAs(DateTime.parse(eventToCreate.end.toString()))) {
        return;
      }
      eventToCreate.calendarId = id;
      final createEventResult =
      await deviceCalendarPlugin.createOrUpdateEvent(eventToCreate);
      if (createEventResult.isSuccess &&
          (createEventResult.data?.isNotEmpty ?? false)) {
      }
  }

  int selectedMonth(String s) {
    int month = 1;
    switch (s) {
      case 'Jan':
        month = 1;
        break;
      case 'Feb':
        month = 2;
        break;
      case 'Mar':
        month = 3;
        break;
      case 'Apr':
        month = 4;
        break;
      case 'May':
        month = 5;
        break;
      case 'Jun':
        month = 6;
        break;
      case 'Jul':
        month = 7;
        break;
      case 'Aug':
        month = 8;
        break;
      case 'Sept':
        month = 9;
        break;
      case 'Oct':
        month = 10;
        break;
      case 'Nov':
        month = 11;
        break;
      case 'Dec':
        month = 12;
        break;
    }
    return month;
  }

  String _getCurrentDateforLessonList() {
    var date = DateTime.now().add(Duration(hours: 1)).toUtc().toString();
    var dateParse = DateTime.parse(date);
    var formattedDate = "${dateParse.year}${dateParse.month}${dateParse.day}";
    String finalDate = formattedDate.toString();
    print(finalDate);
    return finalDate;
  }

  // onLessonRefresh() async {
  //   getLessonList();
  //   await Future.delayed(const Duration(milliseconds: 1000));
  //   lessonRefreshCtrl.refreshCompleted();
  // }

  @override
  void onInit() {
    _retrieveCalendars();
    getLessonList();
    super.onInit();
    socket = SocketIOUser.socket;
    socket.connect();

    socket.onConnect((_) {
      print('connect to socket');
    });

    socket.on('cancel_lesson', (data) async {
      if (data['type'] == 'coach') {
        isLoading.value = true;

          lessonList.removeWhere((element) => element.id == data['lesson_id']);
          var c = await deviceCalendarPlugin.retrieveCalendars();
          var id = '';
          deviceCalendarPlugin.hasPermissions().then((value) => {});
          for (var element in c.data) {
            if (element.isDefault == true) {
              id = element.id;
            }
          }
          final calEvents = await deviceCalendarPlugin.retrieveEvents(id, RetrieveEventsParams(startDate: DateTime.parse(data['start_date']), endDate: DateTime.parse(data['end_date'])));
          if (calEvents.data.isNotEmpty) {
            deviceCalendarPlugin.deleteEvent(calEvents.data.first.calendarId, calEvents.data.first.eventId);
          }
          isLoading.value = false;
    }});
  }

  String conditionTitle(String status) {
    switch (status) {
      case 'accept':
        return "PAY";
      case 'reject':
        return "DECLINED";
      case 'paid':
        return "GET SET UP";
      default:
        return "PENDING";
    }
  }

  Function conditionOnPress(String status, String coachName, String title, slot,
      date, int id, String coachId, String amount) {
    switch (status) {
      case 'accept':
        return () => Get.toNamed(Routes.PAYMENT_PAGE,
            arguments: [coachName, title, slot, date, id, coachId, amount]);
      case 'reject':
        return () {
          customAlert('Sorry', 'Your Booking was Declined by the coach');
        };
      case 'paid':
        return () {
          var inputFormat = DateFormat("dd, MMM yyyy");
          var myTime = inputFormat.parse(date);
          print(myTime.toString());
          var dateTime = DateTime.now().add(Duration(hours: 1)).toUtc();
          var currentDateTime = inputFormat.parse(inputFormat.format(dateTime));
          if (currentDateTime.isBefore(myTime)) {
            customAlert('Sorry', 'Your Joining Before Session');
          } else if (currentDateTime.isAfter(myTime)) {
            customAlert('Sorry', 'Your Joining After Session');
          } else if (currentDateTime.isAtSameMomentAs(myTime)) {
            print("Equal  Time");
            var currentHour=int.parse(dateTime.hour.toString());
            var splitSlot= slot.toString().split("-");
            print("Slot String time:${splitSlot[0]}\nSlot Ending time:${splitSlot[1]}");
            var stringTime= int.parse(splitSlot[0].split(":")[0]);
            var endingTime= int.parse(splitSlot[1].split(":")[0]);
            var endingMinute= int.parse(splitSlot[1].split(":")[1]);

            var currentTime=int.parse(dateTime.minute.toString());
            var isTenMinutesBefore = currentTime >= 50;
            var isPreviousHourCorrect = currentHour + 1 == stringTime;


            if((currentHour >= stringTime && currentHour == endingTime && currentTime < endingMinute) || (isTenMinutesBefore && isPreviousHourCorrect && currentHour <= endingTime)){
              print("Success");
              onJoin(coachId, id);
            }else{
          customAlert('Sorry', 'Not Joining On Schedule Time');
          }
          }
        };
      default:
        return () {
          customAlert('Sorry', 'Your Booking is currently pending ');
        };
    }
  }

  Color conditionColor(String status) {
    switch (status) {
      case 'accept':
        return KColors.ACCEPT;
      case 'reject':
        return KColors.DECLINED;
      case 'paid':
        return KColors.lightBlue;
      default:
        return KColors.PENDING;
    }
  }

  Future<void> onJoin(String coachId, int id) async {
    await [
      Permission.microphone,
      Permission.camera,
      Permission.bluetooth,
      Permission.bluetoothConnect
    ].request();
    Get.toNamed(Routes.VIDEO_CALL, arguments: ["", "", coachId, id]);
  }
}
