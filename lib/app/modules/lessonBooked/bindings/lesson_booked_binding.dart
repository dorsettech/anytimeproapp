import 'package:get/get.dart';

import '../controllers/lesson_booked_controller.dart';

class LessonBookedBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LessonBookedController>(
      () => LessonBookedController(),
    );
  }
}
