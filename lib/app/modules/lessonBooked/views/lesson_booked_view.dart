import 'package:animate_do/animate_do.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/cancel_alert.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../constants.dart';
import '../controllers/lesson_booked_controller.dart';
import 'package:timezone/timezone.dart';

class LessonBookedView extends GetView<LessonBookedController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Get.offAllNamed(Routes.HOME);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Lesson Booked'),
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_sharp),
            onPressed: () {
              Get.offAllNamed(Routes.HOME);
            },
          ),
        ),
        body: Obx(
          () {
            return Column(
              children: [
                controller.isLoading.value
                    ? Expanded(child: Loader(color: KColors.lightBlue))
                    : controller.lessonList.isNotEmpty
                        ? controller.isLoading.value == true ? Loader(color: KColors.lightBlue) :  Expanded(
                            child: BuildLessonListView(),
                          )
                        : Expanded(
                            child: Center(
                            child: Text(
                              'No Lesson Booked Today',
                              style: KTextStyle.f20w5,
                            ),
                          )),
                Padding(
                  padding: EdgeInsets.all(20.0.w),
                  child: MainButton(
                    title: 'book a lesson',
                    onPress: () => Get.toNamed(Routes.BOOKING_LESSON,arguments: ['', '', '', '']),
                    buttonColor: KColors.lightBlue,
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

class BuildLessonListView extends GetView<LessonBookedController> {
  const BuildLessonListView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final lessonRefreshCtrl = RefreshController(initialRefresh: false);
    onLessonRefresh() async {
      controller.getLessonList();
      await Future.delayed(const Duration(milliseconds: 1000));
      lessonRefreshCtrl.refreshCompleted();
    }

    return SmartRefresher(
      // controller: controller.lessonRefreshCtrl,
      // onRefresh: controller.onLessonRefresh,
      controller: lessonRefreshCtrl,
      onRefresh: onLessonRefresh,
      header:
          BezierCircleHeader(rectHeight: 40, bezierColor: KColors.lightBlue),
      child: ListView.separated(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: controller.lessonList.length,
        itemBuilder: (ctx, index) {
          String coachName = controller.lessonList[index].coachName;
          String title = controller.lessonList[index].postTitle;
          String slot = controller.lessonList[index].slot;
          String date = controller.lessonList[index].date;
          String status = controller.lessonList[index].isLesson;
          String amount = controller.lessonList[index].amount;
          // String status = "accept";
          int id = controller.lessonList[index].id;
          String coachId = controller.lessonList[index].coach_id;

          var slotDate = date.toString().split(",");
          var slotTime = slot.toString().split("-");
          var year = slotDate.last.split(" ").last;
          var month = controller.selectedMonth(slotDate.last.split(" ")[1]);
          var day = slotDate.first;
          var startTime = slotTime.first;
          var endTime = slotTime.last.split(":");
          var endTimeHour = endTime.first;
          var endTimeMints = endTime.last;

          final eventToCreate = Event('1');
          eventToCreate.title = 'Anytime pro lesson';
          eventToCreate.start = TZDateTime.utc(int.parse(year), month, int.parse(day), int.parse(startTime.split(":").first) - 1);
          eventToCreate.description = "Your Lesson with $coachName";

          eventToCreate.end = TZDateTime.utc(int.parse(year), month, int.parse(day), int.parse(endTimeHour)  - 1, int.parse(endTimeMints));
          eventToCreate.reminders = [Reminder(minutes: 1440), Reminder(minutes: 10)];

          if (controller.lessonList[index].isLesson == 'paid') {
            controller.addEventsToCalendar(eventToCreate);
          }

          return FadeInUp(
            delay: Duration(milliseconds: 600 + (index * 5)),
            from: (index * 50).toDouble(),
            child: Padding(
              padding: EdgeInsets.all(10.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text(
                          date.substring(0, 2),
                          style: KTextStyle.f18w6.copyWith(fontSize: 28.sp),
                        ),
                        Text(
                          date.substring(4, 8),
                          style: KTextStyle.f16w5,
                        ),
                        Text(date.substring(8, date.length))
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            slot,
                            style: KTextStyle.f18w6.copyWith(fontSize: 28.sp),
                          ),
                          RichText(
                            text: TextSpan(
                              text: 'COACH: ',
                              style: KTextStyle.f14w4.copyWith(
                                  color: KColors.darkBlue,
                                  fontFamily: 'futura',
                                  fontWeight: FontWeight.w700),
                              children: <TextSpan>[
                                TextSpan(
                                    text: coachName.capitalize,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w400)),
                              ],
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              text: 'LESSON: ',
                              style: KTextStyle.f14w4.copyWith(
                                  color: KColors.darkBlue,
                                  fontFamily: 'futura',
                                  fontWeight: FontWeight.w700),
                              children: <TextSpan>[
                                TextSpan(
                                    text: title.toUpperCase(),
                                    style:
                                        TextStyle(fontWeight: FontWeight.w400)),
                              ],
                            ),
                          )
                        ],
                      )),
                  Center(
                    child: Column(
                      children: [
                        InkWell(
                          onTap: controller.conditionOnPress(status, coachName,
                              title, slot, date, id, coachId, amount),
                          child: Container(
                            height: Get.width * 0.1,
                            width: Get.width * 0.25,
                            color: controller.conditionColor(status),
                            child: Center(
                              child: Text(
                                controller.conditionTitle(status),
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: KDynamicWidth.width10,
                        ),
                        InkWell(
                          onTap: () => cancelAlert(context, () async {
                            Navigator.pop(context);
                            controller.isLoading.value = true;
                            var res = await controller.repo.cancelLesson(controller.lessonList[index].id);
                            if (res != null) {
                              var c = await controller.deviceCalendarPlugin.retrieveCalendars();
                              var id = '';
                              controller.deviceCalendarPlugin.hasPermissions().then((value) => {});
                              for (var element in c.data) {
                                if (element.isDefault == true) {
                                  id = element.id;
                                }
                              }
                              final calEvents = await controller.deviceCalendarPlugin.retrieveEvents(id, RetrieveEventsParams(startDate: eventToCreate.start, endDate: eventToCreate.end));

                              if (calEvents.data.isNotEmpty) {
                                controller.deviceCalendarPlugin.deleteEvent(calEvents.data.first.calendarId, calEvents.data.first.eventId);
                              }
                              var userRole = controller.storage.read('role');
                              var userId = controller.storage.read('id');
                              var data = {
                                'user_id': userId,
                                'type': userRole,
                                'lesson_id': controller.lessonList[index].id,
                                'start_date': eventToCreate.start.toIso8601String(),
                                'end_date': eventToCreate.end.toIso8601String(),
                              };
                              controller.socket.emit('cancel_lesson', data);
                              controller.lessonList.remove(controller.lessonList[index]);
                            }
                            controller.isLoading.value = false;
                          }),
                          child: Container(
                            height: Get.width * 0.1,
                            width: Get.width * 0.25,
                            color: Colors.red,
                            child: Center(
                              child: Text(
                                'Cancel',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        ///Tracking Button
                       /*  InkWell(
                           onTap: trackingCamera,
                           child: Container(
                             height: Get.width * 0.1,
                             width: Get.width * 0.25,
                             color: KColors.darkBlue,
                             child: Center(
                               child: Text(
                                 'TRACKING',
                                 style: TextStyle(color: Colors.white),
                               ),
                             ),
                           ),
                         ),*/
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => FadeInUp(
          from: (index * 100).toDouble(),
          delay: Duration(milliseconds: 600 + (index * 5)),
          child: Divider(
            thickness: 2,
          ),
        ),
      ),
    );
  }
}

Future<void> trackingCamera() async {
  await [Permission.microphone, Permission.camera].request();

  Get.toNamed(Routes.TRACKING);
}
