import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/ModelClass/profile_model.dart';
import 'package:anytime_pro/app/data/ModelClass/save_card.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../views/stripe_onboard_screen.dart';

class ProfileController extends GetxController {
  static ProfileController get to => Get.find<ProfileController>();
  final storage = GetStorage();
  final repo = Repository();
  RxBool loader = false.obs;
  RxBool isCoachOnboardInStripe = false.obs;
  RxBool isCardSaved = false.obs;
  final refreshController = RefreshController(initialRefresh: false);
  //var profDataStg = ProfileResponse().obs;

  onRefresh() async {
    getProfile();
    await Future.delayed(const Duration(milliseconds: 1000));
    refreshController.refreshCompleted();
  }


  onLogout(String message) async {
    showDialog(
      context: Get.context,
      builder: (context) {
        return AlertDialog(
          insetPadding: EdgeInsets.all(24),
          title: Text(message , textAlign: TextAlign.center),
          actions: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: TextButton(
                      style: TextButton.styleFrom(
                          backgroundColor: KColors.lightishGrey),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text('No', style: TextStyle(color: Colors.white))),
                ),
                SizedBox(width: 12.w),
                Expanded(
                  child: TextButton(
                      style: TextButton.styleFrom(
                          backgroundColor: KColors.lightBlue),
                      onPressed: () async {
                        if (message == 'Are you sure you want to delete your account?') {
                          Get.showOverlay(asyncFunction: () => _deleteAccount(), loadingWidget: Loader());
                        } else if(message == 'You are going to be onboard for stripe setup!') {
                          Get.showOverlay(asyncFunction: () => setCoachOnboardToStripe(), loadingWidget: Loader());
                        } else {
                          await GetStorage().erase();
                          Get.offAllNamed(Routes.LOGIN);
                        }
                      },
                      child: Text('Yes', style: TextStyle(color: Colors.white))),
                ),
              ],
            )
          ],
        );
      },
    );
  }

    _onAlertWithCustomContentPressed(value) {
      Alert(
        context: Get.context,
        content: StripeOnboardScreen(data: value,)
      ).show();
    }

  Future<void> _deleteAccount() async {
    int id = storage.read('id');
    try {
      await repo
          .deleteAccount(id: id)
          .then((dynamic value) async {
        if(value != null){
          print(value['data']);
          if (value['data'] == 'User has been deleted successfully') {
            await GetStorage().erase();
            Get.offAllNamed(Routes.LOGIN);
          } else {
            errorSnackbar(value.message);
          }
        }else{
          errorSnackbar(value.message);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> getCoachStripeStatus() async {
    int id = storage.read('id');
    try {
      await repo
          .coachStripeStatus(id)
          .then((dynamic value) async {
        if(value != null){
          if(value == "complete") {
            isCoachOnboardInStripe.value = false;
          } else {
            isCoachOnboardInStripe.value = true;
          }
          print(value);
        }else{
          errorSnackbar(value.message);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> setCoachOnboardToStripe() async {
    int id = storage.read('id');
    try {
      await repo
          .coachOnboardToStripe(id)
          .then((dynamic value) async {
        if(value != null){
          Get.back();
          _onAlertWithCustomContentPressed(value);
        }else{
          Get.back();
          errorSnackbar(value.message);
        }
      });
    } catch (e) {
      print(e);
    }
  }


  // profileDataJsonDecoder() async {
  //   var data = storage.read('profileData');
  //   if (data != null) {
  //     Map businessJson = jsonDecode(data);
  //     ProfileResponse _info = ProfileResponse.fromJson(businessJson);
  //
  //     if (_info != null) {
  //       profDataStg.value = _info;
  //     }
  //   }
  // }

  Future<void> getProfile() async {
    int _id = storage.read('id');
    try {
      loader.value = true;
      await repo.getUserProfile(id: _id).then((ProfileModel value) async {
        loader.value = false;
        if (value.status == 'success') {
          // if (value.data != null) {
          //   await storage.write('profileData',
          //       jsonEncode(ProfileResponse.fromJson(value.data.toJson())));
          // }
          storage.write('avatar', value.data.avatar);
          storage.write('name', value.data.firstName);
          storage.write('lastname', value.data.lastName);
          storage.write('country', value.data.countryOfResidence);
          storage.write('town', value.data.currentTown);
          storage.write('coachType', value.data.coachSubType);
          storage.write('gender', value.data.gender);
          storage.write('age', value.data.age);

          storage.write('lessonTaken', value.data.lessonTaken);
          storage.write('handicap', value.data.handicap);
          storage.write('impFactor', value.data.improvementFactor);

          storage.write('crdHolder', value.data.nameOnCard);
          storage.write('crdNum', value.data.cardNumber);
          storage.write('crdExp', "${value.data.exMonth}/${value.data.exYear}");
          storage.write('curHandicap', value.data.currentHandicap);
          storage.write('glfHouse', value.data.homeGolfHouse);
          storage.write('favCourse', value.data.favoriteCourse);
          storage.write('specialism', value.data.golfingSpecialisms);
          storage.write('driRange', value.data.homeDrivingRange);
          storage.write('about', value.data.about ?? "");
          storage.write('price', value.data.price);
          print("value is ${storage.read('role')}");
          if(storage.read('role') == 'coach') getCoachStripeStatus();
        } else {
          errorSnackbar('Please try again Later');
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
