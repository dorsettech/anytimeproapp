import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../../utils/global_widgets.dart';

class StripeOnboardScreen extends StatelessWidget {
  final dynamic data;
  const StripeOnboardScreen({this.data});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 500,
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: Html(
            data: data,
            onLinkTap: (String url, RenderContext context, Map<String, String> attributes, dynamic element) => launchInBrowser(Uri.parse(url))
        ),
      ),
    );
  }
}


