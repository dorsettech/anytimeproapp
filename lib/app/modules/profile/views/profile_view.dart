import 'dart:convert';

import 'package:age_calculator/age_calculator.dart';
import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../EditProfileDetails/editPayment/views/future_payment.dart';
import '../controllers/profile_controller.dart';

class ProfileView extends StatefulWidget {
  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  final _c = Get.put(ProfileController());
  final storage = GetStorage();

  @override
  void initState() {
    print('onInit');
    _c.getProfile();
    // _c.profileDataJsonDecoder();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
              onPressed: () => _c.onLogout('Are you sure you want to logout?'),
              icon: Icon(Icons.logout)),
          IconButton(
              onPressed: () =>
                  _c.onLogout('Are you sure you want to delete your account?'),
              icon: Image.asset('assets/images/delete-account.png', color: Colors.white,)),
          storage.read('role') == 'coach'
              ? Visibility(
                  visible: _c.isCoachOnboardInStripe.value == false,
                  child: IconButton(
                      onPressed: () => _c.onLogout(
                          'You are going to be onboard for stripe setup!'),
                      icon: Image.asset('assets/images/stripe.png')))
              : Container()
        ],
      ),
      body: Obx(
        () {
          return Stack(
            children: [
              SmartRefresher(
                controller: _c.refreshController,
                onRefresh: _c.onRefresh,
                header: BezierCircleHeader(
                  bezierColor: KColors.lightBlue,
                ),
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                child: ListView(
                  children: [
                    // Padding(
                    //     padding: EdgeInsets.symmetric(
                    //         horizontal: KDynamicWidth.width20),
                    //     child: header()),
                    strip('Personal Info', onclick: () {
                      Get.toNamed(Routes.EDIT_PROFILE);
                    }),
                    SizedBox(height: 10.w),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomColumn(
                              subfield:
                                  "${storage.read('name')} ${storage.read('lastname')}",
                              heading: "Full Name",
                            ),
                            Container(
                              height: 1,
                              color: KColors.darkBlue,
                              width: Get.width / 2,
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            CustomColumn(
                              subfield: storage.read('email') ?? '',
                              heading: "Email Address",
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Container(
                              height: 1,
                              color: KColors.darkBlue,
                              width: Get.width / 2,
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 10),
                          child: avatar(context),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    CustomColumn(
                      subfield: storage.read('gender') ?? '',
                      heading: "Gender",
                    ),
                    buildDivider(),
                    CustomColumn(
                      subfield: AgeCalculator.age(DateTime.parse(storage.read('age') == '' || storage.read('age') == null ? DateTime.now().toString() : storage.read('age'))).years.toString(),
                      heading: "Age",
                    ),
                    buildDivider(),
                    CustomColumn(
                      subfield: storage.read('country') ?? '',
                      heading: "Residence",
                    ),
                    buildDivider(),
                    CustomColumn(
                      subfield: storage.read('town') ?? '',
                      heading: "Current Town",
                    ),
                    storage.read('role') == 'coach'
                        ? buildDivider()
                        : SizedBox(),
                    storage.read('role') == 'coach'
                        ? CustomColumn(
                            subfield: storage.read('about') ?? '',
                            heading: "About",
                          )
                        : SizedBox(),
                    storage.read('role') == 'coach'
                        ? buildDivider()
                        : SizedBox(),
                    storage.read('role') == 'coach'
                        ? CustomColumn(
                            subfield: storage.read('price') ?? '',
                            heading: "Price",
                          )
                        : SizedBox(),
                    SizedBox(height: KDynamicWidth.width20),

                    ///======================================================================GOLF DETAILS
                    strip('Golf details', onclick: () {
                      Get.toNamed(Routes.EDIT_GOLF_DETAIL);
                    }),
                    CustomColumn(
                      subfield: storage.read('curHandicap') ?? '',
                      heading: "Current Handicap",
                    ),

                    // buildDivider(),
                    // CustomColumn(
                    //   subfield: storage.read('driRange') ?? '',
                    //   heading: "Home Driving Range",
                    // ),
                    // Row(
                    //   children: [
                    //     Radio(
                    //       value: true,
                    //       groupValue: true,
                    //       toggleable: false,
                    //       onChanged: (bool value) {},
                    //     ),
                    //     Text('HOME'),
                    //     const SizedBox(width: 40),
                    //     Radio(
                    //       value: true,
                    //       groupValue: false,
                    //       toggleable: false,
                    //       onChanged: (bool value) {},
                    //     ),
                    //     Text('SECONDARY'),
                    //   ],
                    // ),
                    buildDivider(),
                    CustomColumn(
                      subfield: storage.read('glfHouse') ?? '',
                      heading: "Home Golf House",
                    ),
                    buildDivider(),
                    CustomColumn(
                      subfield: storage.read('favCourse') ?? '',
                      heading: "Favorite Course",
                    ),
                    // buildDivider(),
                    // CustomColumn(
                    //   subfield: storage.read('specialism') ?? '',
                    //   heading: "Golfing Specialism",
                    // ),
                    // buildDivider(),
                    // Row(
                    //   children: [
                    //     Expanded(
                    //         //TODO
                    //         child: Radio(
                    //             value: true,
                    //             groupValue: false,
                    //             toggleable: false,
                    //             onChanged: (bool value) {})),
                    //     Expanded(
                    //         flex: 4,
                    //         child: Padding(
                    //             padding: const EdgeInsets.only(right: 20),
                    //             child: Text(
                    //                 'this might be dictator, at least, regularly vetted against user testimonials and rating against each specialism'
                    //                     .toUpperCase(),
                    //                 style: KTextStyle.f16w5)))
                    //   ],
                    // ),
                    // buildDivider(),
                    // Obx(() =>
                    // Visibility(
                    //   visible: storage.read('role') == 'Coach' ? false : _c.isCardSaved.value ?? false == true ? false : true,
                    //   child: strip('Payment Information',
                    //       onclick: () => {
                    //         Get.toNamed(Routes.EDIT_PAYMENT)
                    //       }),
                    // )),
                    // Obx(() =>
                    // Visibility(
                    //   visible: storage.read('role') == 'Coach' ? false : _c.isCardSaved.value ?? false == true ? false : true,
                    //   child: Container(
                    //     width: Get.width,
                    //     child: CreditCardWidget(
                    //       cardNumber: storage.read('crdNum') ?? '',
                    //       expiryDate: storage.read('crdExp') ?? '',
                    //       cardHolderName: storage.read('crdHolder') ?? '',
                    //       cvvCode: '',
                    //       showBackView: false,
                    //       isHolderNameVisible: true,
                    //       cardBgColor: KColors.darkBlue,
                    //       obscureCardNumber: true,
                    //       obscureCardCvv: false,
                    //       animationDuration: const Duration(milliseconds: 800),
                    //       onCreditCardWidgetChange: (CreditCardBrand) {},
                    //       // cardType:,
                    //     ),
                    //   ),
                    // )),
                  ],
                ),
              ),
              _c.loader.value
                  ? Center(child: Loader(color: KColors.lightBlue))
                  : SizedBox(),
            ],
          );
        },
      ),
    );
  }
}

Container avatar(BuildContext context) {
  return Container(
    width: MediaQuery.of(context).size.width * 0.272,
    //110,
    height: MediaQuery.of(context).size.height * 0.155,
    //110,
    clipBehavior: Clip.antiAlias,
    decoration: BoxDecoration(shape: BoxShape.circle, color: KColors.DARK_GREY),
    child: CachedNetworkImage(
      imageUrl: ProfileController.to.storage.read('avatar'),
      placeholder: (ctx, _) =>
          Center(child: CircularProgressIndicator(color: KColors.darkBlue)),
      errorWidget: (ctx, _, t) => Icon(Icons.error_outline),
      fit: BoxFit.cover,
    ),
  );
}
