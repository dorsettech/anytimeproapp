import 'dart:convert';
import 'package:anytime_pro/app/data/ModelClass/save_card.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editPayment/views/card_utils.dart';
import 'package:anytime_pro/app/modules/lessonBooked/controllers/lesson_booked_controller.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:http/http.dart' as http;
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../data/ModelClass/book_lesson_model.dart';

class PaymentPageController extends GetxController {
  static PaymentPageController get to => Get.find<PaymentPageController>();

  String coachName, title, slot, date,coachId,amount, cDate;
  int uID;
  CardFieldInputDetails card;
  TokenData tokenData;
  BookLessonModel bookLessonModel;

  final stg = GetStorage();
  final _repo = Repository();
  TextEditingController otpController = TextEditingController();

  RxBool paymentLoader = false.obs;
  RxBool isValueNotEmpty = false.obs;
  RxBool useCurrentCard = true.obs;
  RxString savedCard = ''.obs;
  RxString savedCardBrand = ''.obs;

  @override
  void onInit() {
    coachName = Get.arguments[0];
    title = Get.arguments[1];
    slot = Get.arguments[2];
    date = Get.arguments[3];
    // uID = Get.arguments[4];
    coachId = Get.arguments[4];
    amount= Get.arguments[5];
    saveCard();
    super.onInit();
  }

  saveCard() async  {
    paymentLoader.value = true;
    SaveCard result = await _repo.getSaveCard(stg.read('id'));
    print(result.message.lastFour);
    savedCard.value = ' **** **** **** ${result.message.lastFour}';
    savedCardBrand.value = result.message.cardBrand;
    paymentLoader.value = false;
  }

  Future<void> paymentToServer() async {
    int _id = stg.read('id');
    String _playerEmail = stg.read('email');
    Get.showOverlay(
        asyncFunction: () async {
          try {
            await _repo
                .bookLessonCoach(
                    coachID: int.parse(coachId),
                    amount: amount,
                    playerID: _id,
                    date: date,
                    slot: slot)
                .then((BookLessonModel value) async {
              if (value.status == 'success') {
                bookLessonModel= value;
                uID = bookLessonModel.data.id;

                try {
                  await _repo
                      .addPayment(_id, _playerEmail, int.parse(amount), uID ,coachId)
                      .then((value) {
                    if (value.status == 'success') {
                      successSnackbar('Booking Completed');

                      Future.delayed(const Duration(seconds: 1), () {
                        try {
                          LessonBookedController.to.onInit();
                          Get.offNamed(Routes.LESSON_BOOKED);
                        } catch (e) {
                          Get.offNamed(Routes.LESSON_BOOKED);
                        }
                      });
                    } else {
                      errorSnackbar(value.message);
                    }
                  });
                } catch (e) {
                  print(e);
                }
              } else {
                errorSnackbar('Please try again Later');
              }
            });
          } catch (e) {
            print(e);
          }
        },
        loadingWidget: Loader());
  }

  Future<void> paymentToServerWithVoucher() async {
    int _id = stg.read('id');
    String _playerEmail = stg.read('email');
    Get.showOverlay(
        asyncFunction: () async {

          try {
            await _repo
                .bookLessonCoach(
                coachID: int.parse(coachId),
                amount: amount,
                playerID: _id,
                date: date,
                slot: slot)
                .then((BookLessonModel value) async {
              if (value.status == 'success') {
                bookLessonModel = value;
                uID = bookLessonModel.data.id;

                try {
                  await _repo
                      .addPaymentWithVoucher(otpController.text, int.parse(amount), uID, _id, coachId)
                      .then((value) {
                    if (value.status == 'success') {
                      successSnackbar('Booking Completed');
                      Future.delayed(const Duration(seconds: 1), () {
                        try {
                          LessonBookedController.to.onInit();
                          Get.offNamed(Routes.LESSON_BOOKED);
                        } catch (e) {
                          Get.offNamed(Routes.LESSON_BOOKED);
                        }
                      });
                    } else {
                      errorSnackbar(value.message);
                    }
                  });
                } catch (e) {
                  print(e);
                }
              }
                });
          } catch(e) {
            print(e);
          }

        },
        loadingWidget: Loader());
  }
}
