import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/EditProfileDetails/editPayment/views/card_utils.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import '../../../routes/app_pages.dart';
import '../controllers/payment_page_controller.dart';

class PaymentPageView extends StatefulWidget {
  @override
  State<PaymentPageView> createState() => _PaymentPageViewState();
}

class _PaymentPageViewState extends State<PaymentPageView> {
  final controller = Get.put(PaymentPageController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CONFIRM & PAY'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsetsDirectional.fromSTEB(20, 20, 20, 0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 20),
                child: _buildTitleHead('BOOKING DETAILS'),
              ),
              _buildRow('DATE:  ', controller.date),
              _buildRow('TIME:   ', controller.slot),
              _buildRow('COACH:  ', controller.coachName.toUpperCase()),
              _buildRow('LESSON: ', controller.title),
              Divider(),
              // Padding(
              //   padding: EdgeInsetsDirectional.fromSTEB(0, 15, 0, 15),
              //   child: _buildTitleHead('DISCOUNT CODE:'),
              // ),
              // Padding(
              //   padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 15),
              //   child: Row(
              //     mainAxisSize: MainAxisSize.max,
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       Text(
              //         '1',
              //         style: KTextStyle.f16w5.copyWith(color: Colors.black),
              //       ),
              //       TextButton(
              //           onPressed: () {},
              //           child: Text(
              //             'APPLY',
              //             style: KTextStyle.f16w5
              //                 .copyWith(color: KColors.starColor),
              //           ))
              //     ],
              //   ),
              // ),
              // Divider(),
              // Padding(
              //   padding: EdgeInsetsDirectional.fromSTEB(0, 15, 0, 15),
              //   child: _buildTitleHead(
              //     'AMOUNT',
              //   ),
              // ),
              // Padding(
              //   padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 15),
              //   child: Row(
              //     mainAxisSize: MainAxisSize.max,
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       Text(
              //         '1 X45 MIN LESSON WITH\n${controller.coachName.toUpperCase()}',
              //         textAlign: TextAlign.start,
              //         style: KTextStyle.f16w5.copyWith(color: Colors.black),
              //       ),
              //       _buildTitleHead(
              //         '£'+controller.amount,
              //       )
              //     ],
              //   ),
              // ),

              // Text(
              //   'App service charges amount is £5',
              //   textAlign: TextAlign.start,
              //   style: KTextStyle.f16w5.copyWith(color: Colors.black),
              // ),

              // Padding(
              //   padding: EdgeInsetsDirectional.fromSTEB(0, 15.w, 0, 15),
              //   child: Row(
              //     mainAxisSize: MainAxisSize.max,
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       _buildTitleHead(
              //         'TOTAL',
              //       ),
              //       _buildTitleHead(
              //         '£'+controller.amount,
              //       )
              //     ],
              //   ),
              // ),

              /// stripe native card field
              // CardField(
              //   cursorColor: KColors.lightBlue,
              //   numberHintText: 'Enter card number',
              //   autofocus: true,
              //   onCardChanged: (card) {
              //     setState(() {
              //       controller.card = card;
              //     });
              //   },
              // ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: TextField(
                  decoration: InputDecoration(hintText: 'Voucher Code'),
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.text,
                  controller: controller.otpController,
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      controller.isValueNotEmpty.value = true;
                    } else {
                      controller.isValueNotEmpty.value = false;
                    }
                  },
                ),
              ),

              Obx(() => controller.savedCardBrand.value.isEmpty ? Container() : Padding(
                padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 20),
                child: _buildTitleHead('Use this card'),
              )),

              Obx(() => controller.savedCardBrand.value.isEmpty ? Container() : Padding(
                padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                child: Row(
                  children: [
                    Obx(
                      () => Container(
                        height: 24,
                        width: 24,
                        child: Transform.scale(
                          scale: 1.5,
                          child: Checkbox(
                            value: controller.useCurrentCard.value,
                            onChanged: (bool value) {
                              controller.useCurrentCard.value = value;
                            },
                            checkColor: Colors.white,
                            fillColor: MaterialStateColor.resolveWith((states) {
                              if (states.contains(MaterialState.selected)) {
                                return KColors
                                    .lightBlue; // the color when checkbox is selected;
                              }
                              return Colors.grey;
                            }),
                            shape: CircleBorder(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                     Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: getCardTypeIconByBrand(controller.savedCardBrand.value),
                    ),
                    Expanded(
                        child: Text(controller.savedCard.value),
                    )
                  ],
                ),
              )),

              Obx(() => Center(
                child: TextButton(
                  onPressed: () => Get.toNamed(Routes.EDIT_PAYMENT),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      controller.savedCardBrand.value.isEmpty ? 'Add Card' : 'Use new Card',
                      style: KTextStyle.f16w5.copyWith(
                        color: KColors.lightBlue,
                      ),
                    ),
                  ),
                ),
              )),

              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(0, 15.w, 0, 15),
                child: Obx(() {
                  return controller.paymentLoader.value
                      ? Loader(color: KColors.lightBlue)
                      : MainButton(
                          title: 'book & pay',
                          onPress: controller.isValueNotEmpty.value == true
                              // || controller.savedCardBrand.value.isNotEmpty
                              ? _handleCreateTokenPress
                              : null);
                }),
              )
            ],
          ),
        ),
      ),
    );
  }

  String getCardLastFour() {
    String c = controller.stg.read('crdNum') ?? '';
    if (c.isBlank) return '';
    var n = c.length - 4;
    String result = c.replaceRange(0, n, '*' * n);
    return result;
  }

  Future<void> _handleCreateTokenPress() async {
    controller.paymentLoader.value = true;

    hideKeyboard(context);
    // if (controller.card == null) {
    //   return;
    // }

    try {
      // 1. Gather customer billing information (ex. email)
      final address = Address(
        city: 'Houston',
        country: 'US',
        line1: '1459  Circle Drive',
        line2: '',
        state: 'Texas',
        postalCode: '77063',
      ); // mocked data for tests

      // 2. Create payment method
      // final tokenData = await Stripe.instance.createToken(
      //   CreateTokenParams(type: TokenType.Card, address: address),
      // );
      // setState(() {
      //   controller.tokenData = tokenData;
      // });
      // print(controller.tokenData);
      controller.paymentLoader.value = false;
      // if(controller.otpController.text.isEmpty) {
      //   await controller.paymentToServer();
      // } else {
        await controller.paymentToServerWithVoucher();
      // }
      return;
    } catch (e) {
      errorSnackbar('Error: $e');

      rethrow;
    }
  }

  Row _buildRow(String t, String s) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          t,
          style: KTextStyle.f16w5
              .copyWith(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        Expanded(
          child: Text(
            s,
            style: KTextStyle.f16w5.copyWith(fontWeight: FontWeight.w400),
            overflow: TextOverflow.ellipsis,
          ),
        )
      ],
    );
  }

  Text _buildTitleHead(String s) {
    return Text(
      s,
      style: KTextStyle.f18w6.copyWith(
        color: KColors.lightBlue,
      ),
    );
  }
}
