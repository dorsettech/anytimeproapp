import 'package:anytime_pro/app/data/ModelClass/coaches_list.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:get/get.dart';

class SelectCoachesController extends GetxController {
  var allCoaches = List<Coaches>.empty(growable: true);
  var tempCoachList=List<Coaches>.empty(growable: true);


  final _repo = Repository();
  RxBool isLoad = false.obs;
  String timeSlot;
  String pickedDate;
  String pickedDay;

  Future<void> fetchCoachList() async {
    try {
      isLoad.value = true;
      await _repo
          .fetchCoachesList(date: pickedDate, slot: timeSlot, day: pickedDay)
          .then((CoachesListModel value) async {
        isLoad.value = false;
        if (value.status == 'success') {
          if (value.data != null) {
            for(int i=0;i<value.data.coaches.length;i++)
            {
              if(value.data.coaches[i].availabilty == true)
                {
                  allCoaches.add(value.data.coaches[i]);
                }
            }
            //allCoaches = value.data.coaches;
          }
        } else {
          errorSnackbar('Please try again Later');
        }
      });
    } catch (e) {
      print(e);
      isLoad.value = false;
    }
  }
  sortingCoach(int i){
    tempCoachList=allCoaches;
    isLoad.value = true;
    if(i==1) {
      tempCoachList.sort((a, b) => a.amount.compareTo(b.amount));
      isLoad.value = false;
    }else if(i==2){
      print("Rating");
      tempCoachList.sort((a,b)=>a.rating.compareTo(b.rating));
      isLoad.value = false;
    }else if(1==3){
      tempCoachList.sort((a,b)=>a.lessons.compareTo(b.lessons));
      isLoad.value = false;
    }else{
      tempCoachList.sort((a, b) => b.amount.compareTo(a.amount));
      isLoad.value = false;
    }

  }

  @override
  void onInit() {
    super.onInit();
    timeSlot = Get.arguments[0];
    pickedDate = Get.arguments[1];
    pickedDay = Get.arguments[2];
  }

  @override
  void onReady() {
    fetchCoachList();
    super.onReady();
  }
}
