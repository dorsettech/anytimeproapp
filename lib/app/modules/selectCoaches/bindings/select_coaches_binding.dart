import 'package:get/get.dart';

import '../controllers/select_coaches_controller.dart';

class SelectCoachesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectCoachesController>(
      () => SelectCoachesController(),
    );
  }
}
