import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import '../controllers/select_coaches_controller.dart';

class SelectCoachesView extends GetView<SelectCoachesController> {

  @override
  Widget build(BuildContext context) {
    double ratingBarValue;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Select Coach'),
        actions: [
          PopupMenuButton(
            icon: Icon(Icons.filter_list_alt),
              itemBuilder: (context) => [
                PopupMenuItem(
                  child: Text("Price"),
                  value: 1,
                  onTap: (){
                    controller.sortingCoach(1);
                  },
                ),
                PopupMenuItem(
                  child: Text("Rating"),
                  value: 2,
                  onTap: (){
                    controller.sortingCoach(2);
                  },
                ),
                PopupMenuItem(
                  child: Text("Number of lesson"),
                  value: 3,
                  onTap: (){
                    controller.sortingCoach(3);
                  },
                ),
                PopupMenuItem(
                  child: Text("None"),
                  value: 4,
                  onTap: (){
                    controller.sortingCoach(4);
                  },
                ),
              ]
          )
        ],
      ),
      body: SafeArea(child: Obx(() {
        return controller.isLoad.value
            ? SpinKitCircle(
                color: KColors.lightBlue,
              ):controller.tempCoachList.isEmpty?
            controller.allCoaches.isNotEmpty
                ? selectCoachList(ratingBarValue, controller.allCoaches)
                : Center(
                    child: Text(
                    'Sorry, No Coach Available',
                    style: KTextStyle.f16w5,
                  )):selectCoachList(ratingBarValue, controller.tempCoachList);
      })),
    );
  }

  selectCoachList(double ratingBarValue, List list) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(10.w, 20.w, 10.w, 0),
          child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.7,
                crossAxisSpacing: 20.w,
                mainAxisSpacing: 20.w,
                crossAxisCount: 2),
            itemCount: list.length,
            itemBuilder: (ctx, index) => InkWell(
              onTap: () {
                list[index].availabilty == true
                    ? Get.toNamed(Routes.COACH_DETAILS, arguments: [
                        list[index].id,
                        controller.pickedDate,
                        controller.timeSlot,
                        list[index].amount.toString(),
                      ])
                    : Get.defaultDialog(
                        title: 'Unavailable',
                        middleText:
                            'Coach ${list[index].firstName} is not available'
                                .capitalize,
                        textConfirm: 'OK',
                        middleTextStyle: KTextStyle.f18w6,
                        onConfirm: () => Get.back(),
                        confirmTextColor: Colors.white,
                        buttonColor: KColors.lightBlue);
              },
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: KColors.SHADOW_COLOR),
                      child: CachedNetworkImage(
                        imageUrl: list[index].avatar,
                        progressIndicatorBuilder: (ctx, _, i) => Center(
                          child: CircularProgressIndicator(),
                        ),
                        imageBuilder: (context, imageProvider) => Container(
                          width: Get.width * 0.5,
                          height: Get.width * 0.5,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                        errorWidget: (context, _, i) => Icon(Icons.error),
                      ),
                    ),
                  ),
                  SizedBox(height: 10.h),
                  Text(
                      "${list[index].firstName} ${list[index].lastName}"
                          .capitalizeFirst,
                      style: KTextStyle.f18w6),
                  _buildlistText('${list[index].reviews} reviews'),
                  _buildlistText('Amount: £${list[index].amount.toString()}'),
                  // _buildlistText(
                  //     list[index].availabilty == true
                  //         ? 'Available'
                  //         : 'Unavailable',
                  //     color: list[index].availabilty == true
                  //         ? KColors.lightBlue
                  //         : Colors.black,
                  //     size: list[index].availabilty == true ? 16.sp : 14.sp),
                  _buildlistText('Lessons: ${list[index].lessons.toString()}'),
                  RatingBar.builder(
                    onRatingUpdate: (newValue) => ratingBarValue = newValue,
                    itemBuilder: (context, _) => Icon(
                      Icons.star_rounded,
                      color: KColors.starColor,
                    ),
                    direction: Axis.horizontal,
                    initialRating: list[index].rating.toDouble(),
                    unratedColor: Color(0xFF9E9E9E),
                    itemCount: 5,
                    itemSize: 20,
                    glowColor: KColors.starColor,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Text _buildlistText(String s, {color, size}) {
    return Text(s,
        style: KTextStyle.f14w4
            .copyWith(color: color ?? Colors.black, fontSize: size ?? 14.sp));
  }
}
