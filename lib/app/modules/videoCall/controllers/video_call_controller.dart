import 'dart:convert';

import 'package:anytime_pro/app/data/ModelClass/login_model.dart';
import 'package:anytime_pro/app/data/Provider/provider.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/Coaching/controllers/coaching_controller.dart';
import 'package:anytime_pro/app/modules/home/controllers/home_controller.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'dart:async';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:get_storage/get_storage.dart';
import 'package:whiteboard/whiteboard.dart';
import '../../../constants.dart';
import '../../../data/ModelClass/video_folder_list_model.dart';

class VideoCallController extends GetxController {
  static final agoraUsers = <int>[];
  RxBool muted = false.obs;
  RxBool videoOnOff = true.obs;
  RxBool isVideoIsInSlowMotion = false.obs;
  RxBool isRecording = false.obs;
  RxBool isUIHide = false.obs;
  RxBool isUserJoined = false.obs;
  RxBool loudSpeaker = true.obs;
  RxBool isRecordingComplete= false.obs;
  RxBool onPlay = false.obs;
  RtcEngine engine;
  WhiteBoardController whiteBoardController;
  int remoteUid;
  String coachId;
  var faceDetails = List<FacePositionInfo>.empty(growable: true);
  List<RecordedVideos> recordedVideoList = [];
  String playerId,playerName;
  ///Used For Cloud Recording
  String agoraRecordingResourceId;
  String agoraStorageId, playerVideoUrl;

  int counter=0;
  RxBool isVideoAvailable = false.obs;
  RxBool isVideoUrl = false.obs;
  RxBool isVideoInit =false.obs;
  RxBool isInBuilt = false.obs;
  RxString userRole = ''.obs;
  RxBool isPlayerVideoStatus= false.obs;
  String videoId,agoraCoachID,agoraPlayerId;
  RxBool isVideoCompressed= false.obs,isCoachVideoEnd =false.obs;
  final repo = Repository();
  int userid;
  final storage = GetStorage();

  List<VideoFolderListModel> allVideoList = [];

  @override
  void onInit() {
    try{
      super.onInit();
      userRole.value = storage.read('role');
      playerId = Get.arguments[0];
      playerName = Get.arguments[1];
      coachId = Get.arguments[2]??"";
      if(userRole.value == "coach"){
        agoraCoachID = (coachId!=null && coachId!= "") ? coachId : storage.read('id').toString();
        agoraPlayerId = playerId;
        getVideoListFolder();
      }else{
        agoraPlayerId = storage.read('id').toString();
        agoraCoachID = coachId.toString();
        updateLessonTakenCount();
      }

      whiteBoardController = WhiteBoardController();
    }catch(ex)
    {
      print(ex);
    }
  }

  Future<void> getVideoListFolder() async {
    try {
      await repo
          .fetchVideosRelatedToCoachAndPlayerList(agoraPlayerId)
          .then((List<VideoFolderListModel> value) {
        if(value!=null){
          allVideoList= value;
        }else{
          print("No data");
        }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> fetchVideoUrlData() async{
    try{
      if(isVideoAvailable.value==false){
        userid = storage.read('id');
        await repo.getLatestVideoFileName(userid).then((value){
          if(value!= null && value!="")
          {
            videoId = jsonDecode(value)['id'];
            var tempFileNme= jsonDecode(value)['file_name'];
            isInBuilt.value = false;
            fetchVideoUrl(tempFileNme);
          }
          else{
            fetchVideoUrlData();
          }
        });
      }

    }catch(ex){
      print(ex);
    }
  }

  Future<void> getVideoByID(videoID) async{
    try{
      print('video data is received2');
      var userid = storage.read('id');
      await repo.getVideoByID(int.parse(videoID), userid).then((value){
          if(value!= null && value!="")
          {
            print('video data is received3');
            videoId = videoID;
            var tempFileNme= value;
            isInBuilt.value = false;
            print('video id is $videoId');
            print('video name is $tempFileNme');


            fetchVideoUrl(tempFileNme);
          }
        });

    }catch(ex){
      print(ex);
    }
  }

  Future<String> getVideoByRecordedID(videoID) async{
    try{
      var userid = storage.read('id');
      var video  = await repo.getVideoByID(int.parse(videoID), userid);
      if(video!= null && video!="")
      {
        videoId = videoID;
        var tempFileNme= video;
        isInBuilt.value = false;
        return tempFileNme;
      } else {
        return "";
      }
    }catch(ex){
      print(ex);
      return "";
    }
  }



  Future<void> fetchVideoUrl(String tempFileNme) async{
    try{
      // videoLsnListLoader.value= true;
      await repo.fetchVideoUrl(tempFileNme).then((value){
        print("value is $value");
        if(value!=null)
        {
          playerVideoUrl = value.replaceAll('"', "");
          print("value is $playerVideoUrl");
          isVideoUrl.value=true;
          //getPlayerVideoStatus();
        }
        else{
          isVideoUrl.value=false;
          print("value is $isVideoUrl");
        }
      });
    }catch(ex){
      print(ex);
    }
  }

  Future<String> fetchOlderVideoUrl(String tempFileNme) async {
    var url  = await repo.fetchVideoUrl(tempFileNme);
    if(url != null) {
      isVideoUrl.value = true;
      var newUrl = url.replaceAll('"', "");
      return newUrl;
    } else {
      isVideoUrl.value = false;
      return "";
    }
  }

  Future<void> setPlayerVideoPlay(bool videoPlayCheck, String videoId, bool isEnd) async{
    try{
      await repo.setVideoPlay(videoPlayCheck,videoId,isEnd);
    }catch(ex){
      print(ex);
    }
  }
  Future<void> getPlayerVideoStatus() async{
    if(isVideoUrl.value){
      await repo.fetchVideoStatus(videoId).then((value) {
        if(value!=null && value.isNotEmpty) {
          var coachVideoEnd = jsonDecode(value)['player_play'];
          var autoPlay = jsonDecode(value)['autoplay'];
          if(coachVideoEnd=="0" && coachVideoEnd!=null){
            isCoachVideoEnd.value=false;
          }else if(coachVideoEnd=="1" || coachVideoEnd== null){
            isCoachVideoEnd.value=true;
          }
          if(autoPlay=="1"){
            isPlayerVideoStatus.value=true;
          }else if(autoPlay=="0"){
            isPlayerVideoStatus.value=false;
          }
          // if(autoPlay=="1"){
          //   isPlayerVideoStatus.value=true;
          // }else if(autoPlay=="0"){
          //   isPlayerVideoStatus.value=false;
          // }
          ///
          //isPlayerVideoStatus.value=true;
          getPlayerVideoStatus();
        }
        else{
          getPlayerVideoStatus();
        }
        // if(value==true){
        //   isPlayerVideoStatus.value=true;
        //   getPlayerVideoStatus();
        // }else if(value==false){
        //   isPlayerVideoStatus.value=false;
        //   getPlayerVideoStatus();
        // }
      });
    }else{
      print ("Value not received");
    }
  }

  Future<void> updateLessonTakenCount() async{

    Map<String, String> queryP = {
      'cookie': storage.read('cookie'),
      'json': 'user/get_user_meta',
    };

    MetaData metaData = await repo.getLessonCount(queryP);
    var lessons = {};
    if (metaData.lessonCount != null) {
      lessons = jsonDecode(metaData.lessonCount) ?? {};
      lessons[Get.arguments[3].toString()] = true;
    } else {
      lessons[Get.arguments[3].toString()] = true;
    }

    Map<String, dynamic> queryParams = {
      'cookie': storage.read('cookie'),
      'lessonCount' : json.encode(lessons)
    };
    await repo.updateLessonTakenCount(queryParams).then((value) {
      if(value!=null && value) {

        print('update count');
        }
        else{
        }
      });
  }

  /// Create agora sdk instance and initialize
  Future<void> initAgoraRtcEngine() async {
    engine = await RtcEngine.create(TPartyTokens.AGORA_AppID);
    await engine.enableVideo();
    engine?.enableFaceDetection(true);
    //await engine?.setEnableSpeakerphone(false);
    await engine.setEnableSpeakerphone(loudSpeaker.value);
    await engine.setAudioProfile(AudioProfile.Default, AudioScenario.GameStreaming);
    await engine.registerMediaMetadataObserver();
  }

  void onCallEnd() {
    Get.offNamed(Routes.HOME);
    HomeController.to.currentIndex.value = 1;
  }

  void onToggleMute() {
    muted.toggle();
    engine.muteLocalAudioStream(muted.value);
  }

  void onToggleVideoOff() {
    videoOnOff.toggle();
    engine.enableLocalVideo(videoOnOff.value);
  }

  void onSwitchCamera() {
    engine.switchCamera();
  }
  void onLoudSpeaker(){
    loudSpeaker.toggle();
    if(loudSpeaker.value==false){
      engine.adjustAudioMixingVolume(0);
      engine.adjustPlaybackSignalVolume(0);
    }else{
      engine.setEnableSpeakerphone(loudSpeaker.value);
      engine.adjustAudioMixingVolume(100);
      engine.adjustPlaybackSignalVolume(100);
    }

  }

  @override
  void onClose() {
    super.onClose();
    // clear users
    agoraUsers.clear();
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   isVideoAvailable.value= false;
  // }


  ///Used for cloud recording
  ///AGORA ACQUIRE AND START RECORDING CALLING
  Future<bool> agoraStartCloudRecording(String uID, String ownAgoraUid) async {
    agoraRecordingResourceId = await ApiProvider().agoraAcquire(uID);

    if(agoraRecordingResourceId != null || agoraRecordingResourceId != ""){
      agoraStorageId = await ApiProvider().agoraStartCloudRecording(agoraRecordingResourceId,uID,playerId,ownAgoraUid,playerName);
      return true;
    }else{
      return false;
    }

  }

  ///AGORA STOP RECORDING CALLING
  Future<bool> agoraStopCloudRecording(String uID, String ownAgoraUid) async {
    if((agoraRecordingResourceId != null || agoraRecordingResourceId != "") && (agoraStorageId != null || agoraStorageId != ""))
    {
      String stop = await ApiProvider().agoraStopCloudRecording(agoraRecordingResourceId,agoraStorageId,uID);
      agoraRecordingResourceId = null;
      agoraStorageId = null;
      if(stop =="stop"){
        return false;
      }else{
        return true;
      }
    }else{
      return true;
    }

  }


}
