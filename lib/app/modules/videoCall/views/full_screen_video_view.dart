import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import '../../../constants.dart';
import 'package:auto_orientation/auto_orientation.dart';
import 'dart:io';

class FullScreenDisplay extends StatefulWidget {
  final File videoPath;
  FullScreenDisplay(this.videoPath,{Key key}) : super(key: key);

  @override
  State<FullScreenDisplay> createState() => _FullScreenDisplay();
}

class _FullScreenDisplay extends State<FullScreenDisplay> {
  VideoPlayerController _videoPlayerController;
  bool isLoaded = false , isFullScreen=false;
  RxBool isPlayingVideo = true.obs;
  @override
  void initState() {
    File tempUrl= widget.videoPath;
    super.initState();
    if(_videoPlayerController!=null){
      setState((){
        _videoPlayerController.dispose();
      });
    }
    _videoPlayerController = VideoPlayerController.file(tempUrl)
      ..initialize().then((_) {
        setState(() {
          _videoPlayerController.pause();
          _videoPlayerController.setLooping(false);
          _videoPlayerController.addListener(checkVideo);
          isLoaded = true;
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Video Player"),),
        body: SafeArea(
          child: isLoaded ?
          _videoPlayerController.value.isInitialized ?
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child:VideoPlayer(_videoPlayerController),
          )
              : Container()
              : Center(child:  Loader(color: KColors.lightBlue)),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              _videoPlayerController.value.isPlaying
                  ? _videoPlayerController.pause()
                  : _videoPlayerController.play();
            });
          },
          child: Icon(
            _videoPlayerController.value.isPlaying
                ? Icons.pause
                : Icons.play_arrow,
          ),
        )
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    super.dispose();
  }
  void checkVideo(){
    if(_videoPlayerController.value.position == _videoPlayerController.value.duration) {
      setState(() {
        isPlayingVideo.value=false;
      });
      print('video Ended');
    }

  }
}