import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/ModelClass/video_folder_list_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/data/socket.dart';
import 'package:anytime_pro/app/modules/Coaching/controllers/coaching_controller.dart';
import 'package:anytime_pro/app/modules/videoCall/TensorFlow/recognition.dart';
import 'package:anytime_pro/app/modules/videoCall/TensorFlow/stats.dart';
import 'package:anytime_pro/app/modules/videoCall/controllers/video_call_controller.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:anytime_pro/app/utils/widgets/box_widget.dart';
import 'package:anytime_pro/app/utils/widgets/stats_widget.dart';
import 'package:dio/dio.dart' as DioData;
import 'package:ed_screen_recorder/ed_screen_recorder.dart';
import 'package:ffmpeg_kit_flutter_full_gpl/ffmpeg_kit.dart';
import 'package:ffmpeg_kit_flutter_full_gpl/ffmpeg_kit_config.dart';
import 'package:ffmpeg_kit_flutter_full_gpl/return_code.dart';
import 'package:ffmpeg_kit_flutter_full_gpl/session.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_player/video_player.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'package:hand_signature/signature.dart';
import 'dart:io';

import '../../../utils/widgets/custom_alart.dart';
import '../../practice/views/drill_videos_view.dart';

HandSignatureControl control = HandSignatureControl(
  threshold: 0.01,
  smoothRatio: 0.99,
  velocityRange: 2.0,
);

class VideoCallView extends StatefulWidget {
  @override
  State<VideoCallView> createState() => _VideoCallViewState();
}

class _VideoCallViewState extends State<VideoCallView> {
  bool _isStats = false;
  final _c = Get.put(VideoCallController());
  String ownAgoraUid, joinedUserAgoraUid, base64string;
  File file, compressFile;

  // FlickManager flickManager;
  EdScreenRecorder screenRecorder;
  Map<String, dynamic> _response;
  bool inProgress = false;

  /// Results to draw bounding boxes
  List<Recognition> results;

  /// Realtime stats
  Stats stats;

  /// Video PLayer
  VideoPlayerController _videoPlayerController;
  RxBool isPlayingVideo = true.obs, isVideoCompressed = false.obs;
  bool isLoaded = false;
  String videoId;
  int recordedVideoId;
  bool pIsPlying, isPlayerVideoStatus, isInBuild = false;
  File fullVideoPath;
  bool isEnd = false;
  Directory directory;
  bool isVideoUploadingStart = false;
  double uploadVideoPercentage = 0.0;
  IO.Socket socket;
  final GlobalKey _cancelButtonKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    socket = SocketIOUser.socket;
    socket.connect();

    socket.onConnect((_) {
      print('connect to socket');
    });

    socket.on('play_pause', (data) {
      if (CoachingController.to.userRole.value == 'player') {
          if ((data as List)[1] == 'play') {
            setState(() {
              _c.onPlay.value = true;
              _videoPlayerController.play();
            });
          } else {
            setState(() {
              _videoPlayerController.pause();
            });
          }
      }
    });

    socket.on('close_video', (data) {
      if (CoachingController.to.userRole.value == 'player') {
        InkWell cancelButton = _cancelButtonKey.currentWidget;
        if (cancelButton != null && cancelButton.onTap != null) {
          cancelButton.onTap();
        }
      }
    });

    socket.on('new_video', (data) {

     if (CoachingController.to.userRole.value == 'player') {
       _c.isInBuilt.value = false;
       _c.isVideoUrl.value = true;
       _c.playerVideoUrl = data;
     }
    });

    socket.on('play_location', (data) {
      print('play_location --- ${(data as List).first['timestamp']}');
      print((data as List).first['timestamp'] is Duration);
     if (CoachingController.to.userRole.value == 'player') {
       setState(() {
         Duration currentPosition =
             _videoPlayerController.value.position;
         Duration targetPosition =
             currentPosition -  Duration(seconds: (data as List).first['timestamp']);
         _videoPlayerController.seekTo(targetPosition);
       });
     }
    });

    socket.onConnectError(
        (data) => {print("In onConnectError  : ${data.toString()}")});

    socket.onConnectTimeout(
        (data) => {print("In onConnectTimeout : ${data.toString()}")});

    socket.onError((data) => {print("In onError : ${data.toString()}")});

    socket.onDisconnect(
        (data) => {print("In onDisconnect  : ${data.toString()}")});

    _initialize();
    permissionHandler();
    screenRecorder = EdScreenRecorder();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    if (CoachingController.to.userRole.value == 'player') {
      _c.isVideoAvailable.value = false;
      _c.fetchVideoUrlData();
      Timer.periodic(Duration(seconds: 1), (timer) {
        if (mounted) {
          setState(() {
            playingVideo();

            // if(_c.isPlayerVideoStatus.value){
            //   reloadVideo();
            // }
          });
        }
      });
    }
    _c.isInBuilt.value = false;
  }

  permissionHandler() async {
    var status = await Permission.microphone.status;
    var status2 = await Permission.storage.status;
    if (!status.isGranted && !status2.isGranted) {
      await Permission.microphone.request();
      await Permission.storage.request();
    }
  }

  Future<void> startRecord({String fileName}) async {
    try {
      ///hide coding.
      setState(() {
        _c.isUIHide.value = true;
      });

      var startResponse;
      if (Platform.isAndroid) {
        directory = await getExternalStorageDirectory(); //FOR ANDROID
        startResponse = await screenRecorder?.startRecordScreen(
          fileName: "AnytimePro",
          dirPathToSave: directory.path.toString(),
          audioEnable: false,
        );
      } else {
        startResponse = await screenRecorder?.startRecordScreen(
          fileName: "AnytimePro",
          audioEnable: false,
        );
      }
      print("=-=-=-=-=-=-=-=-=-=-=" + startResponse["eventname"].toString());
      if (startResponse["eventname"] == "CancelButtonClicked") {
        print("CancelButtonClicked");
        setState(() {
          _c.isUIHide.value = false;
        });
      } else {
        setState(() {
          _response = startResponse;
          _c.isRecording.value = true;
          _c.isRecordingComplete.value = false;
          isVideoCompressed.value = false;
        });
        try {
          screenRecorder?.watcher?.events?.listen(
            (event) {
              //log(event.type.toString(), name: "Event: ");
            },
            onError: (e) =>
                kDebugMode ? debugPrint('ERROR ON STREAM: $e') : null,
            onDone: () => kDebugMode ? debugPrint('Watcher closed!') : null,
          );
        } catch (e) {
          kDebugMode ? debugPrint('ERROR WAITING FOR READY: $e') : null;
        }
      }
    } on PlatformException {
      kDebugMode
          ? debugPrint("Error: An error occurred while starting the recording!")
          : null;
    }
  }

  Future<void> stopRecord() async {
    try {
      setState(() {
        _c.isUIHide.value = false;
      });
      var stopResponse = await screenRecorder?.stopRecord();
      setState(() {
        try {
          _response = stopResponse;
          file = (_response['file'] as File);
          print("File path after record${file.path.toString()}");
          _c.isRecordingComplete.value = true;
          // _c.onPlay.value = true;
          _c.isVideoInit.value = true;
          customAlert('Please wait we are preparing video for upload...', '', isWaitForVideo: true);
          compressVideo(file.path);
        } catch (ex) {
          print("error found $ex");
          print(ex);
        }
      });
    } on PlatformException {
      kDebugMode
          ? debugPrint("Error: An error occurred while stopping recording.")
          : null;
    }
  }

  pathBase64() async {
    try {
      final _repository = Repository();
      File myFile = _response['file'] as File;
      File filePath;
      filePath = File(myFile.path);
      final videoBytes = await filePath.readAsBytes();
      var base64string = base64.encode(videoBytes);
      await _repository.storeScreenRecordingCgp(base64string, _c.playerId);
    } catch (ex) {
      print(ex);
    }
  }

  Future<void> takePermission() async {
    await [Permission.microphone, Permission.camera, Permission.storage]
        .request();
  }

  ///initialize agora
  Future<void> _initialize() async {
    if (TPartyTokens.AGORA_AppID.isEmpty) {
      errorSnackbar(
        'APP_ID missing, please provide your APP_ID in settings.dart',
      );
      errorSnackbar('Agora Engine is not starting');

      return;
    }
    await _c.initAgoraRtcEngine();
    _addAgoraEventHandlers();
    // await _c.getToken();
    var uId = await uniqueIDFunc(
        _c.agoraCoachID.toString(), _c.agoraPlayerId.toString());
    await _c.engine
        .joinChannel(TPartyTokens.agoraToken, uId.toString(), null, 0);
    //await _c.engine.joinChannel(TPartyTokens.agoraToken, 'test', null, 0);
    await _c.engine?.setMaxMetadataSize(1024);
  }

  Future<String> uniqueIDFunc(String user1ID, String user2ID) async {
    String tempID;
    if (int.parse(user1ID) > int.parse(user2ID)) {
      tempID = "${user1ID.toString()}_${user2ID.toString()}";
    } else {
      tempID = "${user2ID.toString()}_${user1ID.toString()}";
    }
    return tempID;
  }

  void _handleUserOffline(int uid) {
    print("User $uid has left the call.");
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _c.engine.setEventHandler(RtcEngineEventHandler(
      facePositionChanged:
          (int width, int height, List<FacePositionInfo> faces) {
        setState(() {
          _c.faceDetails = faces;
        });
      },
      joinChannelSuccess: (channel, uid, elapsed) {
        final info = 'onJoinChannel: $channel, uid: $uid';
        setState(() {
          successSnackbar('User joined');
          _c.isUserJoined.value = true;
          setState(() {
            ownAgoraUid = uid.toString();
            print("Own User Agora Id:++++:::::==== " + ownAgoraUid);
          });
        });
      },
      remoteVideoStateChanged: (uid, state, reason, elapsed) {
        print("object");
        print(reason);
        print(state);
        if (state == VideoRemoteState.Stopped) {
          successSnackbar('User has left the call');
          if (CoachingController.to.userRole.value != 'coach') {
            Get.offNamed(Routes.COACH_DETAILS, arguments: [
              int.parse(_c.coachId),
              "",
              "",
              "",
            ]);
          } else {
            Navigator.pop(context);
          }
        }
      },
      userOffline: (uid, reason) {
        print('reason is $reason');
        _handleUserOffline(uid);
      },
      leaveChannel: (stats) {
        print('sttaussssss---------------');
        // _c.infoStrings.add('onLeaveChannel');
        setState(() {
          VideoCallController.agoraUsers.clear();
        });
      },
      userJoined: (uid, elapsed) {
        final info = 'userJoined: $uid';
        setState(() {
          joinedUserAgoraUid = uid.toString();
          print("Joined User Agora Id:++++:::::==== " + joinedUserAgoraUid);
          VideoCallController.agoraUsers.add(uid);
          _c.remoteUid = uid;
        });
      },
      // userOffline: (uid, reason) {
      //   final info = 'userOffline: $uid , reason: $reason';
      //   // setState(() {
      //   //   _c.infoStrings.add(info);
      //   //   VideoCallController.agoraUsers.remove(uid);
      //   // });
      // },

      firstRemoteVideoFrame: (uid, width, height, elapsed) {
        final info = 'firstRemoteVideoFrame: $uid';
        // setState(() {
        //   _c.infoStrings.add(info);
        // });
      },
    ));
  }

  reloadVideo() {
    if (CoachingController.to.userRole.value == 'player') {
      if (_videoPlayerController != null) {
        if (_c.isCoachVideoEnd.value) {
          if (isEnd) {
            _videoPlayerController.pause();
          }
          // checkVideo();
        } else {
          if (_c.isPlayerVideoStatus.value == true &&
              _videoPlayerController.value.isInitialized == true) {
            setState(() {
              _videoPlayerController.play();
            });
          } else if (_c.isPlayerVideoStatus.value == false &&
              _videoPlayerController.value.isInitialized == true) {
            setState(() {
              _videoPlayerController.pause();
            });
          } else {
          }
          setState(() {
            isPlayerVideoStatus = _c.isPlayerVideoStatus.value;
          });
        }
      }
    }
  }

  playingVideo() {
    if (_c.isVideoUrl.value == true) {
      if (_c.isInBuilt.value == false) {
        setState(() {
          _c.onPlay.value = true;
          _c.isInBuilt.value = true;
        });
        if (CoachingController.to.userRole.value == 'player') {
          if (_videoPlayerController != null) {
            setState(() {
              _videoPlayerController.dispose();
            });
          }
          _videoPlayerController =
              VideoPlayerController.network(_c.playerVideoUrl)
                ..initialize().then((_) {
                  setState(() {
                    _videoPlayerController.pause();
                    _videoPlayerController.setLooping(false);
                    _videoPlayerController.addListener(checkVideo);
                    isLoaded = true;
                    // _c.getPlayerVideoStatus();
                  });
                });
          _c.isVideoInit.value = true;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // reloadVideo();
    return Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              // CameraView(resultsCallback, statsCallback),
              // boundingBoxes(results),
              _c.onPlay.value == true
                  ? isLoaded
                  ? _videoPlayerController.value.isInitialized ? SizedBox() : SizedBox() : SizedBox() : _remoteVideo(),

              _c.isUIHide.value ? SizedBox() : _buildLocalView(),

              Positioned(
                  left: 0, //MediaQuery.of(context).size.width*0.15,//60,
                  right: 0, //MediaQuery.of(context).size.width*0.15,//60,
                  top: 0, //MediaQuery.of(context).size.height*0.20,
                  //bottom: MediaQuery.of(context).size.height*0.20,//130,
                  child: _c.onPlay.value == true
                      ? isLoaded
                          ? _videoPlayerController.value.isInitialized
                              ? Stack(
                                  children: [
                                    Container(
                                      height:
                                          MediaQuery.of(context).size.height,
                                      child:
                                          VideoPlayer(_videoPlayerController),
                                    ),
                                    if (CoachingController.to.userRole.value ==
                                        'coach')
                                      minimizeRemoteVideo(),
                                     Positioned(
                                      top: 17,
                                      right: 5,
                                      child: InkWell(
                                        key: _cancelButtonKey,
                                        onTap: () {
                                          setState(() {
                                            if (CoachingController
                                                    .to.userRole.value ==
                                                'coach') {
                                              socket.emit(
                                                  'close_video_id', _c.videoId);
                                            }
                                            _videoPlayerController.pause();
                                            _videoPlayerController.dispose();
                                            _c.isVideoIsInSlowMotion.value =
                                                false;
                                            _c.isRecordingComplete.value =
                                                false;
                                            _c.onPlay.value = false;
                                            _c.isVideoUrl.value = false;
                                            videoId = null;
                                            isVideoCompressed.value = false;
                                            if (CoachingController
                                                    .to.userRole.value ==
                                                'player') {
                                              _videoPlayerController.dispose();
                                              _c.videoId = null;
                                              _c.isVideoAvailable.value = false;
                                              _c.fetchVideoUrlData();
                                            }
                                            _c.isInBuilt.value = false;
                                            isLoaded = false;
                                          });
                                        },
                                        child: CoachingController
                                            .to.userRole.value ==
                                            'coach' ? Container(
                                          padding: EdgeInsets.all(12.r),
                                          child: Icon(
                                            Icons.clear,
                                            color: Colors.white,
                                            size: 20.0,
                                          ),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: KColors.lightBlue,
                                          ),
                                        ) : Container(),
                                      ),
                                    ),
                                  ],
                                )
                              : Container()
                          : Container(
                              height: MediaQuery.of(context).size.height,
                              child: Center(
                                  child: Loader(color: KColors.lightBlue)))
                      : Container()),

              ///red marker enable disable
              Obx(() => _c.isRecording.value
                  //     ? WhiteBoard(
                  //   strokeColor: Colors.red,
                  //   controller: _c.whiteBoardController,
                  //   backgroundColor: Colors.transparent,
                  // )
                  ? HandSignature(
                      control: control,
                      type: SignatureDrawType.line,
                      color: Colors.red,
                      width: 5,
                    )
                  : SizedBox()),

              ///temp play bar
              // _c.isUIHide.value ?
              //     Container()
              // :Positioned(
              //     left: MediaQuery.of(context).size.width*0.15,//60,
              //     right: MediaQuery.of(context).size.width*0.15,//60,
              //     bottom: MediaQuery.of(context).size.height*0.172,//130,
              //     child: _c.onPlay.value==true ?isLoaded ? _videoPlayerController.value.isInitialized ? CoachingController.to.userRole.value == 'coach'?Container(
              //       color: KColors.darkBlue.withOpacity(0.8),
              //       height: 25,
              //       child:Row(
              //         mainAxisAlignment: MainAxisAlignment.end,
              //         children: [
              //           Spacer(flex: 2,),
              //           IconButton(
              //             padding: EdgeInsets.all(0),
              //             onPressed: () {
              //               setState(() {
              //                 if( _videoPlayerController.value.isPlaying){
              //                   pIsPlying=false;
              //                   _videoPlayerController.pause();
              //                 }
              //                 else{
              //                   pIsPlying=true;
              //                   _videoPlayerController.play();
              //                 }
              //                 /// Handel player side Play Pause
              //                 if( CoachingController.to.userRole.value == 'coach'){
              //                   print("Playing status");
              //                   setPlayingStatus();
              //                 }
              //               });
              //             },
              //             icon: Icon(_videoPlayerController.value.isPlaying ? Icons.pause : Icons.play_arrow,size: 30,color: Colors.white,),
              //           ),
              //           Spacer(),
              //           IconButton(
              //             alignment: Alignment.bottomRight,
              //             padding: EdgeInsets.all(0),
              //             onPressed: ()
              //             {
              //               if(fullVideoPath!=null){
              //                 Navigator.push(context, MaterialPageRoute(builder: (context) => FullScreenDisplay(fullVideoPath),));
              //               }
              //             },
              //             icon: Icon(Icons.fullscreen,size: 30,color: Colors.white,),
              //           )
              //         ],
              //       ),
              //     ):SizedBox():SizedBox():SizedBox():SizedBox()
              // ),
              ///end temp play bar
              _c.isUIHide.value
                  ? Positioned(
                      right: MediaQuery.of(context).size.width * 0.05,
                      left: MediaQuery.of(context).size.width * 0.05,
                      bottom: 0,
                      child: Container(
                        height: 100,
                        child: Column(
                          children: [
                            Expanded(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 40,
                                  ),
                                  _c.onPlay.value == true
                                      ? isLoaded
                                          ? _videoPlayerController
                                                  .value.isInitialized
                                              ? CoachingController
                                                          .to.userRole.value ==
                                                      'coach'
                                                  ? customButtons(
                                                      child: Icon(
                                                        _videoPlayerController !=
                                                                null
                                                            ? _videoPlayerController
                                                                    .value
                                                                    .isPlaying
                                                                ? Icons.pause
                                                                : Icons
                                                                    .play_arrow
                                                            : Icons.play_arrow,
                                                        size: 20,
                                                        color: Colors.white,
                                                      ),
                                                      bgColor:
                                                          KColors.lightBlue,
                                                      onTap: () {
                                                        setState(() {
                                                          if (_videoPlayerController
                                                              .value
                                                              .isPlaying) {
                                                            pIsPlying = false;
                                                            isEnd = false;
                                                            _videoPlayerController
                                                                .pause();
                                                          } else {
                                                            pIsPlying = true;
                                                            isEnd = false;
                                                            _videoPlayerController
                                                                .play();
                                                          }

                                                          /// Handel player side Play Pause
                                                          if (CoachingController
                                                                  .to
                                                                  .userRole
                                                                  .value ==
                                                              'coach') {
                                                            print(
                                                                "Playing status000");
                                                            setPlayingStatus();
                                                          }
                                                        });
                                                      })
                                                  : SizedBox()
                                              : SizedBox()
                                          : SizedBox()
                                      : SizedBox.shrink(),
                                  customButtons(
                                      child: Icon(
                                        _c.isRecording.value
                                            ? Icons.stop
                                            : Icons.fiber_manual_record,
                                        color: Colors.red,
                                        size: 20.0,
                                      ),
                                      bgColor: Colors.white,
                                      onTap: () {
                                        if (_c.isRecording.value = true) {
                                          setState(() {
                                            control.clear();
                                            _c.isRecording.value = false;
                                            pIsPlying = false;
                                            isEnd = true;
                                            setPlayingStatus();
                                          });
                                        }
                                        setState(() {
                                          _c.isRecording.value = true;
                                        });
                                        _recording();
                                      }),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : SizedBox(),

              AnimatedSwitcher(
                duration: const Duration(milliseconds: 0),
                child: _isStats
                    ? CoachingController.to.userRole.value == 'coach'
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: VideoCallStats(
                                _c.faceDetails.isEmpty
                                    ? 'Too far/near'
                                    : "${_c.faceDetails[0].distance * 10}"
                                        .toString(),
                                '${'23'}',
                                '${'233'}',
                                '${'444'}'))
                        : SizedBox()
                    : _c.isUIHide.value
                        ? SizedBox()
                        : bottomToolBar(context),
              ),
              CoachingController.to.userRole.value == 'coach'
                  ? _c.isUIHide.value
                      ? SizedBox()
                      : isVideoUploadingStart
                          ? CircularPercentIndicator(
                              radius: 120.0,
                              lineWidth: 13.0,
                              animation: true,
                              percent: uploadVideoPercentage / 100,
                              animationDuration: 1500,
                              center: Text(
                                "Uploading..${uploadVideoPercentage}%",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                    color: Colors.white),
                              ),
                              circularStrokeCap: CircularStrokeCap.round,
                              progressColor: Colors.purple,
                            )
                          : SizedBox()
                  : SizedBox(),

              ///toggle switch
              //CoachingController.to.userRole.value == 'Coach'?_switchForStats():SizedBox(),
              // _toolbar(context),
              //CoachingController.to.userRole.value == 'coach'?drawNotation():SizedBox(),
            ],
          ),
        )

        //
        );
  }

  ///SWITCH BUTTON FOR STATS
  SafeArea _switchForStats() {
    return SafeArea(
      child: Align(
        alignment: Alignment.topRight,
        child: Switch.adaptive(
            value: _isStats,
            onChanged: (v) {
              setState(() {
                _isStats = v;
              });
            }),
      ),
    );
  }

  SafeArea drawNotation() {
    return SafeArea(
      child: Align(
        alignment: Alignment.topRight,
        child: Icon(
          Icons.edit,
          color: Colors.red,
        ),
      ),
    );
  }

  /// Video View For Local USER
  Obx _buildLocalView() {
    return Obx(() => SafeArea(
          child: Align(
            alignment: Alignment.topLeft,
            child: Container(
              // width: 100,
              // height: 150,
              width: MediaQuery.of(context).size.width * 0.30,
              height: MediaQuery.of(context).size.height * 0.20,
              child: Center(
                child: _c.isUserJoined.value
                    ? RtcLocalView.SurfaceView()
                    : Loader(color: KColors.lightBlue),
              ),
            ),
          ),
        ));
  }

  /// Remote User View
  Widget _remoteVideo() {
    if (_c.remoteUid != null) {
      return RtcRemoteView.SurfaceView(uid: _c.remoteUid);
    } else {
      // return Image.asset(
      //   'assets/images/lesson.jpg',
      //   fit: BoxFit.fitHeight,
      // );
      return CoachingController.to.userRole.value == 'coach'
          ? Image.asset(
              'assets/images/waiting_player.png',
              fit: BoxFit.contain,
            )
          : Image.asset(
              'assets/images/waiting_coach.png',
              fit: BoxFit.contain,
            );
    }
  }

  /// Remote User View for coach above video playback
  Widget minimizeRemoteVideo() {
    if (_c.remoteUid != null) {
      return SafeArea(
        child: Align(
          alignment: Alignment.topLeft,
          child: Container(
            color: Colors.red,
            // width: 100,
            // height: 150,
            width: MediaQuery.of(context).size.width * 0.40,
            height: MediaQuery.of(context).size.height * 0.20,
            child: Center(
              child: RtcRemoteView.SurfaceView(uid: _c.remoteUid),
            ),
          ),
        ),
      );
    } else {
      return Container(
        width: MediaQuery.of(context).size.width * 0.40,
        height: MediaQuery.of(context).size.height * 0.20,
        child: Image.asset(
          'assets/images/waiting_player.png',
          fit: BoxFit.contain,
        ),
      );
    }
  }

  /// Cloud Recording
  _recording() async {
    if (_c.isRecording.value) {
      bool res;
      setState(() {
        _c.isRecording.value = false;
        stopRecord();
      });
      //res = await _c.agoraStopCloudRecording(ownAgoraUid.toString(),ownAgoraUid);

    } else {
      bool res;
      //res = await _c.agoraStartCloudRecording(ownAgoraUid.toString(),ownAgoraUid);

      setState(() {
        startRecord();
      });
    }
  }

  /// Toolbar layout
  Widget _toolbar(BuildContext context) {
    return Container(
      height: Get.height * 0.12,
      // decoration: BoxDecoration(
      //     color: KColors.darkBlue, borderRadius: BorderRadius.circular(12)),
      // padding: EdgeInsets.symmetric(vertical: 30.w),
      child: Obx(() {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            customButtons(
                child: Icon(
                  _c.muted.value ? Icons.mic_off : Icons.mic,
                  color: _c.muted.value ? Colors.white : KColors.lightBlue,
                  size: 20.0,
                ),
                bgColor: _c.muted.value ? KColors.lightBlue : Colors.white,
                onTap: () => _c.onToggleMute()),
            customButtons(
                child: Icon(
                  _c.videoOnOff.value ? Icons.videocam : Icons.videocam_off,
                  color: _c.videoOnOff.value ? KColors.lightBlue : Colors.white,
                  size: 20.0,
                ),
                bgColor: _c.videoOnOff.value ? Colors.white : KColors.lightBlue,
                onTap: () => _c.onToggleVideoOff()),
            customButtons(
                child: Icon(
                  _c.loudSpeaker.value ? Icons.volume_up : Icons.volume_off,
                  color:
                      _c.loudSpeaker.value ? KColors.lightBlue : Colors.white,
                  size: 20.0,
                ),
                bgColor:
                    _c.loudSpeaker.value ? Colors.white : KColors.lightBlue,
                onTap: _c.onLoudSpeaker),
            (_c.isRecordingComplete.value)
                ? customButtons(
                    child: Icon(
                      _videoPlayerController != null
                          ? _videoPlayerController.value.isPlaying
                              ? Icons.pause
                              : Icons.play_arrow
                          : Icons.play_arrow,
                      color: KColors.lightBlue,
                      size: 20.0,
                    ),
                    bgColor: Colors.white,
                    onTap: () {
                      if (_c.onPlay.value = true) {
                        setState(() {
                          if (_videoPlayerController.value.isPlaying) {
                            pIsPlying = false;
                            isEnd = false;
                            if (videoId != null &&
                                CoachingController.to.userRole.value == 'coach') {
                              socket.emit('update_play_pause', videoId);
                            }
                            _videoPlayerController.pause();
                          } else {
                            pIsPlying = true;
                            isEnd = false;
                            if (videoId != null &&
                                CoachingController.to.userRole.value == 'coach') {
                              socket.emit('update_play_play', videoId);
                            }
                            _videoPlayerController.play();
                          }

                          /// Handel player side Play Pause
                          // if (CoachingController.to.userRole.value == 'coach') {
                          //   print("Playing status");
                          //
                          //   setPlayingStatus();
                          // }
                        });
                      }
                    }
                    // )
                    // ],
                    )
                : Container(),
            CoachingController.to.userRole.value == 'player'
                ? customButtons(
                    child: Icon(
                      Icons.switch_camera,
                      color: KColors.lightBlue,
                      size: 20.0,
                    ),
                    bgColor: Colors.white,
                    onTap: _c.onSwitchCamera)
                : SizedBox(),
            (_c.onPlay.value &&
                    CoachingController.to.userRole.value == 'coach' &&
                    isVideoCompressed.value == true)
                ? customButtons(
                    child: Icon(
                      Icons.cloud_upload_rounded,
                      color: KColors.lightBlue,
                      size: 20.0,
                    ),
                    bgColor: Colors.white,
                    onTap: () {
                      print("Alert for Confirmation");
                      CoachingController.to.userRole.value == 'coach'
                          ? _c.onPlay.value == true
                              ? showDialog<String>(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      AlertDialog(
                                    title: const Text(
                                        'Are you want to Upload the Video'),
                                    // content: const Text('AlertDialog description'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'Cancel'),
                                        child: const Text(
                                          'Cancel',
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: KColors.lightBlue),
                                        ),
                                      ),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.pop(context, 'Upload');
                                          uploadVideo();
                                        },
                                        child: const Text('Upload',
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: KColors.lightBlue)),
                                      ),
                                    ],
                                  ),
                                )
                              : Container()
                          : Container();
                    })
                : SizedBox(),
            AnimatedSwitcher(
              duration: const Duration(milliseconds: 600),
              child: _c.isRecording.value
                  ? customButtons(
                      child: Icon(
                        Icons.clear,
                        color: KColors.lightBlue,
                        size: 20.0,
                      ),
                      bgColor: Colors.white,
                      // onTap: () => _c.whiteBoardController.clear())
                      onTap: () => control.clear())
                  : SizedBox(),
            ),
            CoachingController.to.userRole.value == 'coach'
                ? customButtons(
                    child: Icon(
                      _c.isRecording.value
                          ? Icons.stop
                          : Icons.fiber_manual_record,
                      color: Colors.red,
                      size: 20.0,
                    ),
                    bgColor: Colors.white,
                    onTap: () {
                      //_c.isRecording.toggle();
                      _recording();
                    })
                : SizedBox(),
            customButtons(
                child: Icon(
                  Icons.call_end,
                  color: Colors.white,
                  size: 35.0,
                ),
                bgColor: Colors.red,
                // onTap: () => Get.back()),
                onTap: () {
                  if (CoachingController.to.userRole.value != 'coach') {
                    Get.offNamed(Routes.COACH_DETAILS, arguments: [
                      int.parse(_c.coachId),
                      "",
                      "",
                      "",
                    ]);
                  } else {
                    Get.back();
                  }
                }),
          ],
        );
      }),
    );
  }

  Widget _toolbar2(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 8),
      child: Obx(() {
        return Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            (_c.isRecordingComplete.value) &&
                CoachingController.to.userRole.value == 'coach'
                ? customButtons(
                child: Icon(
                  Icons.replay_10_rounded,
                  color: KColors.lightBlue,
                  size: 20.0,
                ),
                bgColor: Colors.white,
                onTap: () {
                  if (_c.onPlay.value = true) {
                    setState(() {
                      Duration currentPosition =
                          _videoPlayerController.value.position;
                      Duration targetPosition =
                          currentPosition - const Duration(seconds: 3);
                      _videoPlayerController.seekTo(targetPosition);
                      Map<String, dynamic> data = {
                        "video_id": videoId,
                        "timestamp": 3
                      };
                      socket.emit('update_play_head', data);
                    });

                  }
                })
                : Container(),
            SizedBox(width: 10,),
            (_c.isRecordingComplete.value)
                ? customButtons(
                    child: Icon(
                      _c.isVideoIsInSlowMotion.value
                          ? Icons.fast_forward
                          : Icons.slow_motion_video_rounded,
                      color: KColors.lightBlue,
                      size: 20.0,
                    ),
                    bgColor: Colors.white,
                    onTap: () {
                      if (_c.onPlay.value = true) {
                        var speed = 1.0;
                        if (_c.isVideoIsInSlowMotion.value) {
                          speed = 1.0;
                          _c.isVideoIsInSlowMotion.value = false;
                        } else {
                          speed = 0.3;
                          _c.isVideoIsInSlowMotion.value = true;
                        }
                        _videoPlayerController.setPlaybackSpeed(speed);
                      }
                    })
                : Container()
          ],
        );
      }),
    );
  }

  ///Toolbar
  Widget bottomToolBar(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: DraggableScrollableSheet(
        initialChildSize: 0.14,
        minChildSize: 0.1,
        maxChildSize: 0.4,
        builder: (_, ScrollController scrollController) => Container(
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: KColors.darkBlue.withOpacity(0.8),
              borderRadius: BorderRadius.circular(10)),
          child: SingleChildScrollView(
            controller: scrollController,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _toolbar(context),
                  if (CoachingController.to.userRole.value == 'coach')
                    _toolbar2(context),
                  _c.recordedVideoList.isNotEmpty
                      ? Divider(
                          color: KColors.SHADOW_COLOR,
                        )
                      : Container(),
                  if (CoachingController.to.userRole.value == 'coach')
                    _c.recordedVideoList.isNotEmpty
                        ? _videoWall(_c.recordedVideoList)
                        : Container(),
                  Divider(
                    color: KColors.SHADOW_COLOR,
                  ),
                  if (CoachingController.to.userRole.value == 'coach')
                    Text(
                      'Older videos',
                      style: KTextStyle.f16w5.copyWith(
                          color: KColors.lightishGrey,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 0.8,
                          fontSize: 18.sp),
                    ),
                  if (CoachingController.to.userRole.value == 'coach')
                    _c.allVideoList.isNotEmpty
                        ? _videoWallOfOlderVideos(_c.allVideoList)
                        : Container(),
                ],
              ),

              // Bounding boxes
            ),
          ),
        ),
      ),
    );
  }

  _videoWallOfOlderVideos(List<VideoFolderListModel> drillVideoList) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //_videoHeading('Swing'),
            SizedBox(
              height: 10,
            ),
            Container(
              height: Get.height * 0.2,
              width: Get.width,
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                itemCount: drillVideoList.length,
                padding: EdgeInsets.only(left: KDynamicWidth.width20),
                itemBuilder: (ctx, index) => InkWell(
                  onTap: () async {
                    customAlert('Please wait', '', isWaitForVideo: true);
                    String url =
                        await _c.fetchOlderVideoUrl(drillVideoList[index].file);
                    if (url.isEmpty) {
                      Get.back();
                      return;
                    }

                    print("url is $url");

                    videoId = drillVideoList[index].fileId;
                    socket.emit('new_video_id', url);
                    Get.back();
                    _videoPlayerController = VideoPlayerController.network(url,
                        videoPlayerOptions:
                            VideoPlayerOptions(mixWithOthers: true))
                      ..initialize().then((_) {
                        setState(() {
                          _c.isVideoIsInSlowMotion.value = false;
                          _c.isRecordingComplete.value = true;
                          _c.onPlay.value = true;
                          _c.isVideoInit.value = true;
                          isVideoCompressed.value = true;
                          _videoPlayerController.pause();
                          _videoPlayerController.setLooping(false);
                          _videoPlayerController.addListener(checkVideo);
                          isLoaded = true;
                        });
                      });
                  },
                  child: Container(
                    width: Get.width * 0.34,
                    height: Get.height * 0.2,
                    child: Stack(
                      children: [
                        Align(
                          alignment: AlignmentDirectional(0, 0),
                          child: Image.asset(
                            'assets/images/appIcon.png',
                            width: Get.width,
                            height: Get.height * 1,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            drillVideoList[index].file,
                            style: KTextStyle.f16w5.copyWith(
                                color: KColors.lightishGrey,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.8,
                                fontSize: 13.sp),
                          ),
                        ),
                        Align(
                          alignment: AlignmentDirectional(0, 0),
                          child: Container(
                              padding: EdgeInsets.all(8.w),
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Icon(
                                Icons.videocam_outlined,
                                color: Colors.white,
                              )),
                        )
                      ],
                    ),
                  ),
                ),
                separatorBuilder: (BuildContext context, int index) =>
                    SizedBox(width: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _videoWall(List<RecordedVideos> drillVideoList) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //_videoHeading('Swing'),
            SizedBox(
              height: 10,
            ),
            Container(
              height: Get.height * 0.2,
              width: Get.width,
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                itemCount: drillVideoList.length,
                padding: EdgeInsets.only(left: KDynamicWidth.width20),
                itemBuilder: (ctx, index) => InkWell(
                  onTap: () async {
                    customAlert('Please wait', '', isWaitForVideo: true);
                    String name =
                        await _c.getVideoByRecordedID(drillVideoList[index].id);
                    videoId = drillVideoList[index].id;
                    if(name.isNotEmpty) {
                      String url =
                      await _c.fetchOlderVideoUrl(name);
                      if (url.isEmpty) {
                        Get.back();
                        return;
                      }
                      Get.back();
                      _videoPlayerController = VideoPlayerController.network(url,
                          videoPlayerOptions:
                          VideoPlayerOptions(mixWithOthers: true))
                        ..initialize().then((_) {
                          setState(() {
                            _c.isVideoIsInSlowMotion.value = false;
                            _c.isRecordingComplete.value = true;
                            _c.onPlay.value = true;
                            _c.isVideoInit.value = true;
                            isVideoCompressed.value = true;
                            _videoPlayerController.pause();
                            _videoPlayerController.setLooping(false);
                            _videoPlayerController.addListener(checkVideo);
                            isLoaded = true;
                          });
                        });

                      socket.emit('new_video_id', url);
                    } else {
                      Get.back();
                    }
                  },
                  child: Container(
                    width: Get.width * 0.34,
                    height: Get.height * 0.2,
                    child: Stack(
                      children: [
                        Align(
                          alignment: AlignmentDirectional(0, 0),
                          child: Image.asset(
                            'assets/images/appIcon.png',
                            width: Get.width,
                            height: Get.height * 1,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            'Recording ${index + 1}',
                            style: KTextStyle.f16w5.copyWith(
                                color: KColors.lightishGrey,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 0.8,
                                fontSize: 18.sp),
                          ),
                        ),
                        Align(
                          alignment: AlignmentDirectional(0, 0),
                          child: Container(
                              padding: EdgeInsets.all(8.w),
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.circular(10)),
                              child: Icon(
                                Icons.videocam_outlined,
                                color: Colors.white,
                              )),
                        )
                      ],
                    ),
                  ),
                ),
                separatorBuilder: (BuildContext context, int index) =>
                    SizedBox(width: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ///TOOLBAR CUSTOM BUTTONS
  Widget customButtons({child, bgColor, onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(12.r),
        child: child,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: bgColor,
        ),
      ),
    );
  }

  /// Returns Stack of bounding boxes
  Widget boundingBoxes(List<Recognition> results) {
    if (results == null) {
      return Container();
    }
    return Stack(
      children: results.map((e) => BoxWidget(result: e)).toList(),
    );
  }

  /// Callback to get inference results from [CameraView]
  void resultsCallback(List<Recognition> results) {
    setState(() {
      this.results = results;
    });
  }

  /// Callback to get inference stats from [CameraView]
  void statsCallback(Stats stats) {
    setState(() {
      this.stats = stats;
    });
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom, SystemUiOverlay.top]);
    super.dispose();
    socket.disconnect();
    socket.close();
    socket.destroy();
    socket.dispose();
    _c.engine.leaveChannel();
    _c.engine.destroy();
    _c.isVideoAvailable.value = true;
    _c.isVideoUrl.value = false;
    _c.onPlay.value = false;
    _c.isRecordingComplete.value = false;
    _c.muted.value = false;
    _c.videoOnOff.value = true;
    if (_c.isVideoInit.value == true) {
      _c.isVideoInit.value = false;
    }
    isVideoCompressed.value = false;
    if (_videoPlayerController != null &&
        _videoPlayerController.value.isInitialized == true) {
      _videoPlayerController.dispose();
    }
    _c.loudSpeaker.value = true;
    _c.isInBuilt.value = false;
  }

  compressVideo(String newFile) async {
    try {
      const _chars =
          'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
      Random _rnd = Random();
      String getRandomString(int length) =>
          String.fromCharCodes(Iterable.generate(
              length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
      String newName = getRandomString(15);
      String newInput = getRandomString(16);
      var outputPath = newFile.toString().split('/');

      outputPath.remove(outputPath[(outputPath.length) - 1]);

      var out = outputPath.join('/');
      String output = out + "/" + newName + ".mp4";
      String inputPath = out + "/" + newInput + ".mp4";

      File(newFile).rename(inputPath);
      print(newFile.toString());
      setState(() {
        fullVideoPath = File(inputPath);
        print("object00");
      });
      compressMain(inputPath, output);
      if (_videoPlayerController != null) {
        setState(() {
          _videoPlayerController.dispose();
        });
      }
      // _c.recordedVideoList.add(RecordedVideos(url: inputPath));
      _videoPlayerController = VideoPlayerController.file(File(inputPath),
          videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true))
        ..initialize().then((_) {
          setState(() {
            _c.onPlay.value = true;
            _videoPlayerController.pause();
            _videoPlayerController.setLooping(false);
            _videoPlayerController.addListener(checkVideo);
            isLoaded = true;
          });
        });
    } catch (ex) {
      print("object $ex");
      print(ex);
    }
  }

  void compressMain(String inputPath, String output) async {
    FFmpegKit.executeAsync("-i $inputPath -s 1024x576 $output",
        (Session session) async {
      final returnCode = await session.getReturnCode();
      final _repository = Repository();
      try {
        if (ReturnCode.isSuccess(returnCode)) {
          print("SUCCESS");
          File lastVid = File(output);
          print(output.toString());
          File filePath = File(lastVid.path);
          final videoBytes = await filePath.readAsBytes();
          base64string = base64.encode(videoBytes);
          if (base64string != null && base64string.isNotEmpty) {
            isVideoCompressed.value = true;
            print("base 64 enter");
            print(_c.onPlay.value);
            if (_c.onPlay.value == false) {
              _videoPlayerController = VideoPlayerController.file(File(inputPath),
                  videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true))
                ..initialize().then((_) {
                  setState(() {
                    _c.onPlay.value = true;
                    _videoPlayerController.pause();
                    _videoPlayerController.setLooping(false);
                    _videoPlayerController.addListener(checkVideo);
                    isLoaded = true;
                  });
                });
            }
          } else {
            errorSnackbar("video compressed failed");
          }
          print("base 64");
          Get.back();
          //await _repository.storeScreenRecordingCgp(base64string,_c.playerId);
        } else if (ReturnCode.isCancel(returnCode)) {
          print("Cancel");
          Get.back();
        } else {
          // Get.back();
          print("Error");
          FFmpegKitConfig.enableLogCallback((log) {
            final message = log.getMessage();
            print("message $message");
          });
          compressMain(inputPath, output);
        }
      } catch (ex) {
        print(ex);
      }
    });
  }

  void checkVideo() {
    if (_videoPlayerController.value.position ==
        _videoPlayerController.value.duration) {
      print('entery5');
      var x = _videoPlayerController.value.duration;
      setState(() {
        _videoPlayerController.seekTo(Duration(
            seconds: 0,
            microseconds: 0,
            milliseconds: 0,
            hours: 0,
            minutes: 0));
        isPlayingVideo.value = false;
        //_videoPlayerController.pause();
        pIsPlying = false;
        isEnd = true;
        print('enter0');
        setPlayingStatus();
      });
      print('video Ended');
    } else {
    }
  }

  uploadVideo() async {
    try {
      if (base64string.isNotEmpty &&
          base64string != null &&
          _c.playerId != null) {
        var userId = _c.storage.read('id');
        int tempPlayerId = int.parse(_c.playerId);

        DioData.Response response = await DioData.Dio()
            .post("https://www.anytimepro.io/google-cloud/index.php", data: {
          'video': base64string,
          'coach_id': userId,
          'player_id': tempPlayerId,
        }, onSendProgress: (int sent, int total) {
          double value = (sent / total) * 100;
          setState(() {
            isVideoUploadingStart = true;
            uploadVideoPercentage =
                num.parse(value.toStringAsFixed(2)).toDouble();
          });
        },
                options: DioData.Options(headers: {
                  "Content-Type": "application/json",
                  'Authorization': 'Bearer ${_c.storage.read('token')}',
                }));
        if (response.statusCode == 200) {
          var data = (jsonDecode(response.data)).toString();
          Future.delayed(Duration(milliseconds: 500)).then((value) => {
                setState(() {
                  isVideoUploadingStart = false;
                }),
                successSnackbar("Uploaded")
              });
          videoId = data;
          print('videoId --- $videoId');
          setState(() {
            _c.recordedVideoList
                .add(RecordedVideos(url: fullVideoPath, id: videoId));
          });
        } else {
          print((errorHandler(response)).toString());
          errorSnackbar((errorHandler(response)).toString());
        }
      }
    } catch (ex) {
      print(ex);
    }
  }

  dynamic errorHandler(DioData.Response response) {
    print(response.toString());
    switch (response.statusCode) {
      case 200:
      case 201:
      case 202:
        var responseJson = response.data;
        return responseJson;
      case 500:
        throw "Server Error please retry later";
      //return errorSnackbar("Server Error please retry later");
      case 403:
        throw 'Error occurred pls check internet and retry.';
      //return errorSnackbar('Error occurred pls check internet and retry.');
      default:
        throw 'Error occurred retry';
      // return errorSnackbar('Error occurred please retry');
    }
  }

  setPlayingStatus() {
    if (videoId != null) {
      _c.setPlayerVideoPlay(pIsPlying, videoId, isEnd);
    }
  }
}
