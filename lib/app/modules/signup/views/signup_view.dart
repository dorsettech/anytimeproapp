import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/mixins/validation_mixins.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/signup_controller.dart';

class SignupView extends StatefulWidget {
  @override
  State<SignupView> createState() => _SignupViewState();
}

class _SignupViewState extends State<SignupView> with ValidationMixin {
  final controller = Get.put(SignupController());
  @override
  Widget build(BuildContext context) {
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: controller.signUpFormKey,
      child: Scaffold(
          key: controller.signUpScaffoldKey,
          body: Obx(() {
            return SingleChildScrollView(
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              physics: ClampingScrollPhysics(),
              padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
              child: SafeArea(
                child: Column(
                  children: [
                    SizedBox(
                      height: Get.height / 40,
                    ),
                    Hero(
                      tag: 'logo',
                      child: Image.asset(
                        'assets/images/small-logo.jpg',
                        height: controller.registerAs != null
                            ? Get.height / 5
                            : Get.height / 3,
                        width: Get.width,
                        fit: BoxFit.contain,
                      ),
                    ),
                    AnimatedOpacity(
                      duration: const Duration(seconds: 1),
                      opacity: controller.registerAs == 'COACH' ? 1 : 0,
                      curve: Curves.fastOutSlowIn,
                      child: Visibility(
                        visible:
                            controller.registerAs == 'COACH' ? true : false,
                        child: Image.asset(
                          'assets/images/register_coach.jpg',
                          height: Get.height / 3,
                          width: Get.width,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    AnimatedOpacity(
                      duration: const Duration(seconds: 1),
                      opacity: controller.registerAs == 'PLAYER' ? 1 : 0,
                      curve: Curves.fastOutSlowIn,
                      child: Visibility(
                        visible:
                            controller.registerAs == 'PLAYER' ? true : false,
                        child: Image.asset(
                          'assets/images/register_player.jpg',
                          height: Get.height / 3,
                          width: Get.width,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    buildInputField(
                        validator: validateReqFields,
                        textController: controller.signUpNameCont,
                        title: 'FULL NAME',
                        isIcon: false),
                    buildInputEmailField(
                        validator: validateEmail,
                        textController: controller.signUpEmailCont,
                        title: 'EMAIL ADDRESS',
                        isIcon: false),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'REGISTER AS..',
                          style: KTextStyle.f16w5.copyWith(
                              color: KColors.DARK_GREY,
                              fontWeight: FontWeight.w600),
                        ),
                        DropdownButton<String>(
                          isExpanded: false,
                          hint: Text(
                            'CHOOSE OPTION',
                            style: KTextStyle.f16w5.copyWith(
                                color: KColors.DARK_GREY,
                                fontWeight: FontWeight.w600),
                          ),
                          value: controller.registerAs,
                          items:
                              <String>['COACH', 'PLAYER'].map((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(
                                value,
                                style: KTextStyle.f16w5.copyWith(
                                    color: KColors.darkBlue,
                                    fontWeight: FontWeight.w600),
                              ),
                            );
                          }).toList(),
                          onChanged: (v) {
                            setState(() {
                              controller.registerAs = v;
                            });
                          },
                        ),
                      ],
                    ),
                    buildInputField(
                        validator: validateReqFields,
                        textController: controller.signUpPassCont,
                        title: 'PASSWORD',
                        isIcon: true),
                    SizedBox(
                      height: KDynamicWidth.width20,
                    ),
                    MainButton(
                        title: 'Create account',
                        onPress: () {
                          controller.signUpSubmit(context);
                        },
                        buttonColor: KColors.darkBlue),
                    SizedBox(height: 25.h),
                    InkWell(
                      child: Text('BACK TO LOGIN'),
                      onTap: () => Get.back(),
                    ),
                    SizedBox(
                      height: Get.height / 15,
                    ),
                  ],
                ),
              ),
            );
          })),
    );
  }
}
