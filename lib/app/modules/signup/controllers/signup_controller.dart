import 'package:anytime_pro/app/data/ModelClass/signup_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SignupController extends GetxController {
  TextEditingController signUpEmailCont = TextEditingController();
  TextEditingController signUpPassCont = TextEditingController();
  TextEditingController signUpNameCont = TextEditingController();
  GlobalKey<FormState> signUpFormKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> signUpScaffoldKey = GlobalKey<ScaffoldState>();
  final storage = GetStorage();
  final _repository = Repository();
  var registerAs;

  Future<dynamic> signUpSubmit(BuildContext context) async {
    if (signUpFormKey.currentState.validate()) {
      if (registerAs == null) {
        errorSnackbar('Please Select One Option');
      }
      await hideKeyboard(context);
      Get.showOverlay(
          asyncFunction: () => _doSignUp(context), loadingWidget: Loader());
    }
  }

  Future<void> _doSignUp(context) async {
    try {
      await _repository
          .signUpAPI(
              email: signUpEmailCont.text.trim(),
              pass: signUpPassCont.text,
              fullname: signUpNameCont.text.capitalizeFirst,
              role: registerAs)
          .then((SignupModel value) async {
        if (value.status == 'success') {
          Get.back();
          successSnackbar('Registration Successful');
        } else {
          errorSnackbar(value.message);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void onClose() {
    signUpEmailCont.dispose();
    signUpPassCont.dispose();
    signUpNameCont.dispose();
    super.onClose();
  }
}
