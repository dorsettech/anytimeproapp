import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/mixins/validation_mixins.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/forgot_pass_controller.dart';

class ForgotPassView extends GetView<ForgotPassController>
    with ValidationMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: Get.height / 30,
                ),
                Image.asset(
                  'assets/images/small-logo.jpg',
                  height: Get.height / 4,
                  width: Get.width,
                  fit: BoxFit.contain,
                ),
                SizedBox(
                  height: Get.height / 30,
                ),
                buildInputEmailField(
                    textController: controller.forgotPassEmail,
                    title: 'EMAIL',
                    isIcon: false,
                    validator: validateReqFields),
                SizedBox(height: 25.h),
                MainButton(
                    title: 'Submit',
                    onPress: () => controller.onSubmitForgotPass(context),
                    buttonColor: KColors.lightBlue),
                SizedBox(height: 25.h),
                InkWell(
                  child: Text('BACK TO LOGIN'),
                  onTap: () => Get.back(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
