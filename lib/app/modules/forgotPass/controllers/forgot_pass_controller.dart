import 'package:anytime_pro/app/data/ModelClass/forgot_Pass_Model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotPassController extends GetxController {
  TextEditingController forgotPassEmail = TextEditingController();
  final _repository = Repository();

  onSubmitForgotPass(BuildContext context) async {
    await hideKeyboard(context);
    Get.showOverlay(
        asyncFunction: () => _doForgot(context), loadingWidget: Loader());
  }

  _doForgot(BuildContext context) async {
    try {
      if (forgotPassEmail.text.isNotEmpty) {
        await _repository
            .forgotPassApi(email: forgotPassEmail.text.trim())
            .then((ForgotPassModel value) async {
          if (value.status == 'success') {
            Get.back();
            successSnackbar(value.message);
          } else {
            errorSnackbar(value.message);
          }
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
