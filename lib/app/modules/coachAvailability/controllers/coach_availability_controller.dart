import 'dart:convert';

import 'package:anytime_pro/app/data/ModelClass/coach_availabilit_model.dart';
import 'package:anytime_pro/app/data/ModelClass/get_coach_availability_model.dart';
import 'package:anytime_pro/app/data/ModelClass/new_get_coach_availability_model.dart';
import 'package:anytime_pro/app/data/ModelClass/new_set_caoch_availability_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class CoachAvailabilityController extends GetxController {
  final _storage = GetStorage();
  final repo = Repository();

  RxBool slotLoader = false.obs;

  var resDates = List<DateTime>.empty(growable: true);
  var resSlots = List<Slot>.empty(growable: true);

  var selectedSlots = List<String>.empty(growable: true);
  var listOfDates = List<String>.empty(growable: true);
  RxString selectedDateForSlot;
  RxString visibleDateString = DateFormat('yMMMM').format(DateTime.now().add(Duration(hours: 1)).toUtc()).obs;
  List<Slot> dateAndSlotList = [];
  List<NewSetCoachAvailabilityModel> setSlotData = [];
  Slot setSlotAvailabilityModel = Slot();
  NewSetCoachAvailabilityModel setSlotDataModel= NewSetCoachAvailabilityModel();
  List<String> slots = [];
  @override
  void onInit() {
    getCoachAvailability();
    super.onInit();
  }

  void updateMonthLabelFromVisibleDate(DateTime visibleDate) {
    DateFormat dateFormat = DateFormat.yMMMM();
    visibleDateString.value = dateFormat.format(visibleDate);
  }

/*-----------------------------------------------------------------------------*/
  Future<void> getCoachAvailability()async{
    try{
      slotLoader.value = true;
      int _id = _storage.read('id');
      try {
        dateAndSlotList = await repo.fetchCoachAvailable(id: _id);
        setAva( (DateFormat('M/d/yyyy').format(DateTime.now().add(Duration(hours: 1)).toUtc()).toString()));
        slotLoader.value = false;
        // if(dateAndSlotList != null && dateAndSlotList.isNotEmpty) {
        //   setAva( (DateFormat('M/d/yyyy').format(DateTime.now().add(Duration(hours: 1)).toUtc()).toString()));
        //   slotLoader.value = false;
        // } else {
        //   setAva( (DateFormat('M/d/yyyy').format(DateTime.now().add(Duration(hours: 1)).toUtc()).toString()));
        //   slotLoader.value = false;
        // }
      } catch (e) {
        print(e);
      }
    }catch(ex){
      print(ex);
    }
  }
  Future<void> setAva(String selectedDate) async {
    setSlotAvailabilityModel = Slot();
    int tempIndex = 0;
    bool isOnZero = false;

    if (dateAndSlotList != null) {
      for (int i = 0; i < dateAndSlotList.length; i++) {
        print(i.toString());
        if (dateAndSlotList[i].date == selectedDate) {
          if (i == 0) {
            isOnZero = true;
          } else {
            isOnZero = false;
          }
          tempIndex = i;
        }
      }
    }
    if(tempIndex == 0 && isOnZero == true ){
      setSlotAvailabilityModel.date = selectedDate;
      setSlotAvailabilityModel.timeSlots = dateAndSlotList[tempIndex].timeSlots;
    }else if(tempIndex == 0 && isOnZero == false ){
      setSlotAvailabilityModel.date = selectedDate;
      setSlotAvailabilityModel.timeSlots = [];
      dateAndSlotList = [];
      dateAndSlotList.add(setSlotAvailabilityModel);
    } else if(tempIndex>0){
      setSlotAvailabilityModel.date = selectedDate;
      setSlotAvailabilityModel.timeSlots = dateAndSlotList[tempIndex].timeSlots;
    }
    slotLoader.value = false;
  }

  /*-----------------------------------------------------------------------------*/
  getLoaderForAvailablity() {
    Get.showOverlay(
        asyncFunction: () => postAvailability(), loadingWidget: Loader());
  }

  Future<void> postAvailability() async {
    int _id = _storage.read('id');
    setSlotDataModel.coachId= _id.toString();
    setSlotDataModel.slots = dateAndSlotList;
    try {
      await repo.setCoachAvailability(setSlotDataModel).then((value) async {
        if (value== true) {
          successSnackbar("Slots are updated");
        } else {
          errorSnackbar('Please try again Later');
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }
}
