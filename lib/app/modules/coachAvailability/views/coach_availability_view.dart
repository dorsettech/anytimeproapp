import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/coachAvailability/controllers/coach_availability_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:calendarro/calendarro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:calendarro/date_utils.dart' as calUtils;
import 'package:intl/intl.dart';

class CoachAvailabilityView extends StatefulWidget {
  @override
  State<CoachAvailabilityView> createState() => _CoachAvailabilityViewState();
}

class _CoachAvailabilityViewState extends State<CoachAvailabilityView> {
  final _c = Get.put(CoachAvailabilityController());

  @override
  void initState() {
    update();
    super.initState();
  }

  update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            print(_c.listOfDates);
            print(_c.selectedSlots);
            _c.getLoaderForAvailablity();
            //_c.postAvailability();
          },
          label: Text('SUBMIT')),
      appBar: AppBar(
        title: Text('My Availability'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8.w),
              child: Obx(() {
                return Text(
                  _c.visibleDateString.value,
                  style: KTextStyle.f20w5.copyWith(fontWeight: FontWeight.bold),
                );
              }),
            ),
            AvailabilityCalender(),
            Padding(
                padding: EdgeInsets.all(Get.width * 0.03),
                child: Text('Select Time slots', style: KTextStyle.f18w6)),
            Obx(
                  () => Container(
                height: Get.height / 1.85,
                width: Get.width,
                child: _c.slotLoader.value
                    ? Center(child: Loader(color: KColors.lightBlue))
                    : Padding(
                  padding: EdgeInsets.all(8.w),
                  child: GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate:
                    SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 1.7,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 4),
                    // itemCount: slotDataListTest.length,
                    itemCount: slotDataList.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (ctx, index) => InkWell(
                      onTap: () {
                        setState(() {
                          /// latest updated
                          int index1 = 0;
                          bool isOnZero = false;
                          if(_c.setSlotAvailabilityModel.timeSlots == null || _c.setSlotAvailabilityModel.timeSlots.isEmpty){
                            // _c.setSlotAvailabilityModel.timeSlots.add(slotDataListTest[index]);
                            _c.setSlotAvailabilityModel.timeSlots = [];
                            _c.setSlotAvailabilityModel.timeSlots.add(slotDataList[index]);
                          }else{
                            for(int i=0;i<_c.setSlotAvailabilityModel.timeSlots.length;i++){
                              // if(_c.setSlotAvailabilityModel.timeSlots[i] == slotDataListTest[index]){
                              if(_c.setSlotAvailabilityModel.timeSlots[i] == slotDataList[index]){
                                if(i==0){
                                  isOnZero = true;
                                }else{
                                  isOnZero = false;
                                }
                                index1 = i;
                              }
                            }
                            if(index1 == 0 && isOnZero == true ){
                              _c.setSlotAvailabilityModel.timeSlots.removeAt(index1);
                            }else if(index1 == 0 && isOnZero == false ){
                              // _c.setSlotAvailabilityModel.timeSlots.add(slotDataListTest[index]);
                              _c.setSlotAvailabilityModel.timeSlots.add(slotDataList[index]);
                            } else if(index1>0){
                              _c.setSlotAvailabilityModel.timeSlots.removeAt(index1);
                            }
                          }
                          //_c.setDataInList(_c.setSlotAvailabilityModel);
                        });
                      },
                      child:Container(
                        decoration: BoxDecoration(
                            color:  (_c.setSlotAvailabilityModel.timeSlots !=null)?
                            // _c.setSlotAvailabilityModel.timeSlots.contains(slotDataListTest[index])
                            _c.setSlotAvailabilityModel.timeSlots.contains(slotDataList[index])
                                ? Colors.lightBlueAccent
                                : KColors.lightBlue : KColors.lightBlue),
                            //color: KColors.lightBlue),
                        child: Center(
                            child: Text(
                              // slotDataListTest[index],
                              slotDataList[index],
                              style: TextStyle(color: Colors.white),
                            )),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AvailabilityCalender extends StatefulWidget {
  @override
  _AvailabilityCalenderState createState() => _AvailabilityCalenderState();
}

class _AvailabilityCalenderState extends State<AvailabilityCalender> {
  var startDate = calUtils.DateUtils.getFirstDayOfCurrentMonth();
  var endDate = calUtils.DateUtils.getLastDayOfNextMonth();

  final _c = Get.put(CoachAvailabilityController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 20.w, 0, 0),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Calendarro(
              startDate: startDate,
              endDate: endDate,
              //selectedDate: DateTime(2022, 8, 1),//_c.resDates ?? [],
              selectedSingleDate:DateTime.now().add(Duration(hours: 1)).toUtc(),
              displayMode: DisplayMode.MONTHS,
              selectionMode: SelectionMode.SINGLE,
              onPageSelected: (pageStartDate, pageEndDate) {
                setState(() {
                  ///change month name text
                  _c.updateMonthLabelFromVisibleDate(pageStartDate);
                });
              },
              onTap: (date) {
                setState(() {
                  /// adding selected dates to list in string datatype
                  _c.slotLoader.value = true;
                  String myDate= (DateFormat('M/d/yyyy').format(date).toString());
                  _c.setAva(myDate);
                });
              }),
        ),
      ),
    );
  }
}
