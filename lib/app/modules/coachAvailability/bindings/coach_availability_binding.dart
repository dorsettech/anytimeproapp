import 'package:get/get.dart';

import '../controllers/coach_availability_controller.dart';

class CoachAvailabilityBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CoachAvailabilityController>(
      () => CoachAvailabilityController(),
    );
  }
}
