import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../controllers/walkthrough_controller.dart';

class WalkthroughView extends GetView<WalkthroughController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: KDynamicWidth.width20,
                vertical: 2 * KDynamicWidth.width20),
            child: PageView(
              controller: controller.pageViewController ??=
                  PageController(initialPage: 0),
              scrollDirection: Axis.horizontal,
              children: [
                introPagesScreen(
                    nextbutton: 'NEXT',
                    onTap: () {
                      controller.pageViewController.nextPage(
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.easeInToLinear);
                    },
                    heading: 'How to use Anytime Pro',
                    screenImage: 'assets/images/small-logo.jpg',
                    subheading:
                        """ Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. """),
                introPagesScreen(
                    nextbutton: 'CONTINUE TO APP',
                    onTap: () {
                      Get.offAllNamed(Routes.HOME);
                    },
                    heading: 'How to use Anytime Pro',
                    screenImage: 'assets/images/small-logo.jpg',
                    subheading:
                        """ Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. """)
              ],
            ),
          ),
          Positioned(
            bottom: Get.height / 8,
            child: SmoothPageIndicator(
              controller: controller.pageViewController ??=
                  PageController(initialPage: 0),
              count: 2,
              axisDirection: Axis.horizontal,
              onDotClicked: (i) {
                controller.pageViewController.animateToPage(
                  i,
                  duration: const Duration(milliseconds: 500),
                  curve: Curves.ease,
                );
              },
              effect: const ExpandingDotsEffect(
                expansionFactor: 2,
                spacing: 5,
                radius: 16,
                dotWidth: 10,
                dotHeight: 10,
                dotColor: Color(0xFF9E9E9E),
                activeDotColor: KColors.darkBlue,
                paintStyle: PaintingStyle.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }

  introPagesScreen({heading, onTap, screenImage, subheading, nextbutton}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          screenImage,
          height: Get.height / 6,
          width: Get.width,
          fit: BoxFit.contain,
        ),
        const SizedBox(height: 30),
        Text(
          heading,
          style: KTextStyle.f20w5.copyWith(color: KColors.lightBlue),
        ),
        SizedBox(height: KDynamicWidth.width20),
        Text(
          subheading,
          style: KTextStyle.f16w5.copyWith(color: Colors.grey),
        ),
        SizedBox(height: Get.height / 6),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: MainButton(
            buttonColor: KColors.darkBlue,
            onPress: onTap,
            title: nextbutton,
          ),
        )
      ],
    );
  }
}
