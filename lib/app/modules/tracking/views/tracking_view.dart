import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/Coaching/controllers/coaching_controller.dart';
import 'package:anytime_pro/app/modules/videoCall/TensorFlow/recognition.dart';
import 'package:anytime_pro/app/modules/videoCall/TensorFlow/stats.dart';
import 'package:anytime_pro/app/modules/videoCall/controllers/video_call_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:anytime_pro/app/utils/widgets/box_widget.dart';
import 'package:anytime_pro/app/utils/widgets/camera_view.dart';
import 'package:anytime_pro/app/utils/widgets/camera_view_singleton.dart';
import 'package:anytime_pro/app/utils/widgets/stats_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:agora_rtc_engine/rtc_engine.dart';
// import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
// import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;
import 'package:get/get.dart';
import 'package:whiteboard/whiteboard.dart';

class TrackingView extends StatefulWidget {
  @override
  State<TrackingView> createState() => _TrackingViewState();
}

class _TrackingViewState extends State<TrackingView> {
  bool _isStats = false;
  final _c = Get.put(VideoCallController());

  /// Results to draw bounding boxes
  List<Recognition> results;

  /// Realtime stats
  Stats stats;

  @override
  void initState() {
    super.initState();
    //  _initialize();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
  }

  ///initialize agora
  Future<void> _initialize() async {
    if (TPartyTokens.AGORA_AppID.isEmpty) {
      errorSnackbar(
        'APP_ID missing, please provide your APP_ID in settings.dart',
      );
      errorSnackbar('Agora Engine is not starting');

      return;
    }
    // await _c.initAgoraRtcEngine();
    // // _addAgoraEventHandlers();
    // // await _c.getToken();
    // await _c.engine.joinChannel(TPartyTokens.agoraToken, 'test', null, 0);
    // await _c.engine?.setMaxMetadataSize(1024);
  }

  /// Add agora event handlers
  // void _addAgoraEventHandlers() {
  //   _c.engine.setEventHandler(RtcEngineEventHandler(
  //     facePositionChanged:
  //         (int width, int height, List<FacePositionInfo> faces) {
  //       setState(() {
  //         _c.faceDetails = faces;
  //       });
  //     },
  //     joinChannelSuccess: (channel, uid, elapsed) {
  //       final info = 'onJoinChannel: $channel, uid: $uid';
  //       setState(() {
  //         print(
  //             "elapsed Time ---------------------------------------------------------------> $elapsed");
  //         successSnackbar('User joined');
  //         _c.isUserJoined.value = true;
  //       });
  //     },
  //     leaveChannel: (stats) {
  //       // _c.infoStrings.add('onLeaveChannel');
  //       setState(() {
  //         VideoCallController.agoraUsers.clear();
  //       });
  //     },
  //     userJoined: (uid, elapsed) {
  //       final info = 'userJoined: $uid';
  //       setState(() {
  //         VideoCallController.agoraUsers.add(uid);
  //         _c.remoteUid = uid;
  //       });
  //     },
  //     // userOffline: (uid, reason) {
  //     //   final info = 'userOffline: $uid , reason: $reason';
  //     //   // setState(() {
  //     //   //   _c.infoStrings.add(info);
  //     //   //   VideoCallController.agoraUsers.remove(uid);
  //     //   // });
  //     // },
  //
  //     firstRemoteVideoFrame: (uid, width, height, elapsed) {
  //       final info = 'firstRemoteVideoFrame: $uid';
  //       // setState(() {
  //       //   _c.infoStrings.add(info);
  //       // });
  //     },
  //   ));
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              // CameraView(resultsCallback, statsCallback),
              boundingBoxes(results),
              // _remoteVideo(),
              // _buildLocalView(),
              Obx(() => _c.isRecording.value
                  ? WhiteBoard(
                      strokeColor: KColors.darkBlue,
                      controller: _c.whiteBoardController,
                      backgroundColor: Colors.transparent,
                    )
                  : SizedBox()),
              AnimatedSwitcher(
                duration: const Duration(milliseconds: 600),
                child: _isStats
                    ? CoachingController.to.userRole.value == 'coach'
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: VideoCallStats(
                                '${'23'}',
                              //SizedBox(),
                                // _c.faceDetails.isEmpty
                                //     ? 'Too far/near'
                                //     : "${_c.faceDetails[0].distance * 10}"
                                //         .toString(),
                                '${'23'}',
                                '${'233'}',
                                '${'444'}'))
                        : SizedBox()
                    : bottomToolBar(context),
              ),
              _switchForStats()
              // _toolbar(context),
            ],
          ),
        )

        //
        );
  }

  ///SWITCH BUTTON FOR STATS
  SafeArea _switchForStats() {
    return SafeArea(
      child: Align(
        alignment: Alignment.topRight,
        child: Switch.adaptive(
            value: _isStats,
            onChanged: (v) {
              setState(() {
                _isStats = v;
              });
            }),
      ),
    );
  }

  /// Video View For Local USER
  Obx _buildLocalView() {
    return Obx(() => SafeArea(
          child: Align(
            alignment: Alignment.topLeft,
            child: Container(
              width: 100,
              height: 150,
              child: Center(
                child: _c.isUserJoined.value
                    ? SizedBox()//RtcLocalView.SurfaceView()
                    : Loader(color: KColors.lightBlue),
              ),
            ),
          ),
        ));
  }

  /// Remote User View
  Widget _remoteVideo() {
    if (_c.remoteUid != null) {
      return SizedBox();//RtcRemoteView.SurfaceView(uid: _c.remoteUid);
    } else {
      return Image.asset(
        'assets/images/lesson.jpg',
        fit: BoxFit.fitHeight,
      );
    }
  }

  /// Toolbar layout
  Widget _toolbar(BuildContext context) {
    return Container(
      height: Get.height * 0.12,
      // decoration: BoxDecoration(
      //     color: KColors.darkBlue, borderRadius: BorderRadius.circular(12)),
      // padding: EdgeInsets.symmetric(vertical: 30.w),
      child: Obx(() {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            customButtons(
                child: Icon(
                  _c.muted.value ? Icons.mic_off : Icons.mic,
                  color: _c.muted.value ? Colors.white : KColors.lightBlue,
                  size: 20.0,
                ),
                bgColor: _c.muted.value ? KColors.lightBlue : Colors.white,
                onTap: () => _c.onToggleMute()),
            customButtons(
                child: Icon(
                  _c.videoOnOff.value ? Icons.videocam_off : Icons.videocam,
                  color: _c.videoOnOff.value ? Colors.white : KColors.lightBlue,
                  size: 20.0,
                ),
                bgColor: _c.videoOnOff.value ? KColors.lightBlue : Colors.white,
                onTap: () => _c.onToggleVideoOff()),
            customButtons(
                child: Icon(
                  Icons.switch_camera,
                  color: KColors.lightBlue,
                  size: 20.0,
                ),
                bgColor: Colors.white,
                onTap: _c.onSwitchCamera),
            AnimatedSwitcher(
              duration: const Duration(milliseconds: 600),
              child: _c.isRecording.value
                  ? customButtons(
                      child: Icon(
                        Icons.clear,
                        color: KColors.lightBlue,
                        size: 20.0,
                      ),
                      bgColor: Colors.white,
                      onTap: () => _c.whiteBoardController.clear())
                  : SizedBox(),
            ),
            customButtons(
                child: Icon(
                  _c.isRecording.value ? Icons.stop : Icons.fiber_manual_record,
                  color: Colors.red,
                  size: 20.0,
                ),
                bgColor: Colors.white,
                onTap: () {
                  _c.isRecording.toggle();
                }),
            customButtons(
                child: Icon(
                  Icons.call_end,
                  color: Colors.white,
                  size: 35.0,
                ),
                bgColor: Colors.red,
                onTap: () => Get.back()),
          ],
        );
      }),
    );
  }

  ///Toolbar
  Widget bottomToolBar(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: DraggableScrollableSheet(
        initialChildSize: 0.14,
        minChildSize: 0.1,
        maxChildSize: 0.4,
        builder: (_, ScrollController scrollController) => Container(
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: KColors.darkBlue.withOpacity(0.8),
              borderRadius: BorderRadius.circular(10)),
          child: SingleChildScrollView(
            controller: scrollController,
            child: Center(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                _toolbar(context),
                Divider(
                  color: KColors.SHADOW_COLOR,
                )
              ]),

              // Bounding boxes
            ),
          ),
        ),
      ),
    );
  }

  ///TOOLBAR CUSTOM BUTTONS
  Widget customButtons({child, bgColor, onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
          padding: EdgeInsets.all(12.r),
          child: child,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: bgColor,
          )),
    );
  }

  /// Returns Stack of bounding boxes
  Widget boundingBoxes(List<Recognition> results) {
    if (results == null) {
      return Container();
    }
    return Stack(
      children: results.map((e) => BoxWidget(result: e)).toList(),
    );
  }

  /// Callback to get inference results from [CameraView]
  void resultsCallback(List<Recognition> results) {
    setState(() {
      this.results = results;
    });
  }

  /// Callback to get inference stats from [CameraView]
  void statsCallback(Stats stats) {
    setState(() {
      this.stats = stats;
    });
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom, SystemUiOverlay.top]);
    //
    // _c.engine.leaveChannel();
    // _c.engine.destroy();
    super.dispose();
  }
}
