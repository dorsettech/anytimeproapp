import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final storage = GetStorage();
  void onTabTapped(int index) {
    getProfileData(index);
    //controller.currentIndex.value = index;

  }

  getProfileData(int index){
    String name=storage.read('name');
    String email=storage.read('email');
    String gender=storage.read('gender');
    String age=storage.read('age');
    String country=storage.read('country');
    String currentTown=storage.read('town');
    String about=storage.read('about');
    String price=storage.read('price');
    String avatar= storage.read("avatar");

    if (name == null && name.isEmpty) {
      errorSnackbar('Please enter your name');
      return;
    }

    if (email == null && email.isEmpty) {
      errorSnackbar('Please enter your email');
      return;
    }

    if (gender == null || gender.isEmpty) {
      errorSnackbar('Please select your gender');
      return;
    }

    if (age == null || age.isEmpty) {
      errorSnackbar('Please set your age');
      return;
    }

    if (country == null || country.isEmpty) {
      errorSnackbar('Please enter your country');
      return;
    }

    if (currentTown == null || currentTown.isEmpty) {
      errorSnackbar('Please enter your currentTown');
      return;
    }

    if(storage.read('role') == 'coach') {
      if (about == null || about.isEmpty) {
        errorSnackbar('Please give us some information about yourself');
        return;
      }

      if (price == null || price == '0') {
        errorSnackbar('Please enter your price');
        return;
      }

      if (avatar == null || avatar.isEmpty) {
        errorSnackbar('Please choose your profile Image');
        return;
      }
    }
    if(storage.read('customFlag') == "0") {
      Alert(
          style: alertStyle,
          context: Get.context,
          type: AlertType.info,
          buttons: <DialogButton>[
            DialogButton(
              color: KColors.lightBlue,
              child: Text(
                "COOL",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () {
                storage.write('customFlag', '1');
                Get.back();
                controller.currentIndex.value = index;
              },
              width: 120,
            )                ],
          content: Column(
            children: [
              Text("Thanks for filling in your profile! You’re now free to book your first Anytime Pro lesson. For your lesson, you will need a phone stand (tripod) as well as a set of (wireless) headphones. We recommend purchasing the following if you don’t have them already:"),
              TextButton(
                  onPressed: () {
                    launchInBrowser(Uri.parse('https://www.amazon.co.uk/dp/B0B4687Y5C?ref_=cm_sw_r_apin_dp_PG6QGMWMMFY677D8YPQA'));
                  }, child: Text('Phone Stand', style: TextStyle(color: KColors.lightBlue),)),
              TextButton(
                  onPressed: () {
                    launchInBrowser(Uri.parse('https://www.amazon.co.uk/dp/B0876349QT?ref_=cm_sw_r_apin_dp_0Q3Y4284SEEA4RK5Q5E9'));
                  }, child: Text('Wireless headphones', style: TextStyle(color: KColors.lightBlue),)),
            ],
          )
      ).show();
      return;
    }

    controller.currentIndex.value = index;
  }

  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      child: Scaffold(
        bottomNavigationBar: Obx(
          () => BottomNavigationBar(
            selectedItemColor: Colors.white,
            unselectedItemColor: Colors.white38,
            backgroundColor: KColors.lightBlue,
            type: BottomNavigationBarType.fixed,
            showUnselectedLabels: true,
            currentIndex: controller.currentIndex.value,
            onTap: onTabTapped,
            items: [
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/1.jpg',
                  height: 25,
                ),
                label: 'Profile',
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/3.jpg',
                  height: 25,
                ),
                label: 'Coaching',
              ),
              // BottomNavigationBarItem(
              //   icon: Image.asset(
              //     'assets/images/icon4.png',
              //     height: 25,
              //   ),
              //   label: 'Market Place',
              // ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/4.jpg',
                  height: 25,
                ),
                label: 'Practice',
              ),
              // BottomNavigationBarItem(
              //   icon: Image.asset(
              //     'assets/images/6.jpg',
              //     height: 25,
              //   ),
              //   label: 'Settings',
              // ),
            ],
          ),
        ),
        body: Obx(
          () => IndexedStack(
            index: controller.currentIndex.value,
            children: controller.children,
          ),
        ),
      ),
    );
  }
}
