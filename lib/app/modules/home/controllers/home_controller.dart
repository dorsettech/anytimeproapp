import 'package:anytime_pro/app/data/ModelClass/profile_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/Coaching/views/coaching_view.dart';
import 'package:anytime_pro/app/modules/marketPlace/views/market_place_view.dart';
import 'package:anytime_pro/app/modules/practice/views/practice_view.dart';
import 'package:anytime_pro/app/modules/profile/views/profile_view.dart';
import 'package:anytime_pro/app/modules/settings/views/settings_view.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController {
  static HomeController get to => Get.find<HomeController>();

  List<Widget> children = [
    ProfileView(),
    CoachingView(),
    // MarketPlaceView(),
    PracticeView(),
    // SettingsView()
  ].obs;
  RxInt currentIndex=1.obs;
  final storage = GetStorage();
  final repo = Repository();
  RxBool loader = false.obs;

  @override
  void onInit() {
    super.onInit();
    try{
      // currentIndex.value = Get.arguments[0];
      // print("Current Index :"+currentIndex.value.toString());
      var tempIndex= Get.arguments[0];
      getProfile(tempIndex);
    }catch(ex){
      print(ex);
    }
  }

  Future<void> getProfile(tempIndex) async {
    int _id = storage.read('id');
    try {
      loader.value = true;
      await repo.getUserProfile(id: _id).then((ProfileModel value) async {
        loader.value = false;
        if (value.status == 'success') {
          // if (value.data != null) {
          //   await storage.write('profileData',
          //       jsonEncode(ProfileResponse.fromJson(value.data.toJson())));
          // }
          storage.write('avatar', value.data.avatar);
          storage.write('name', value.data.firstName);
          storage.write('lastname', value.data.lastName);
          storage.write('country', value.data.countryOfResidence);
          storage.write('town', value.data.currentTown);
          storage.write('coachType', value.data.coachSubType);
          storage.write('gender', value.data.gender);
          storage.write('age', value.data.age);

          storage.write('lessonTaken', value.data.lessonTaken);
          storage.write('handicap', value.data.handicap);
          storage.write('impFactor', value.data.improvementFactor);

          storage.write('crdHolder', value.data.nameOnCard);
          storage.write('crdNum', value.data.cardNumber);
          storage.write('crdExp', "${value.data.exMonth}/${value.data.exYear}");
          storage.write('curHandicap', value.data.currentHandicap);
          storage.write('glfHouse', value.data.homeGolfHouse);
          storage.write('favCourse', value.data.favoriteCourse);
          storage.write('specialism', value.data.golfingSpecialisms);
          storage.write('driRange', value.data.homeDrivingRange);
          storage.write('about', value.data.about??"");
          storage.write('price', value.data.price);
          getProfileData(tempIndex);
        } else {
          errorSnackbar('Please try again Later');
        }
      });
    } catch (e) {
      print(e);
    }
  }

  getProfileData(int index){
    print("My name++++++----++:"+storage.read('name').toString());
    String name=storage.read('name');
    String email=storage.read('email');
    String gender=storage.read('gender');
    String age=storage.read('age');
    String country=storage.read('country');
    String currentTown=storage.read('town');
    String about=storage.read('about');
    String price=storage.read('price');
    String avatar= storage.read("avatar");
    if(storage.read('role') == 'coach'){
      print("Coach");
      if(name!=null && name.isNotEmpty && email!=null && email.isNotEmpty && gender!=null && gender.isNotEmpty && age!=null && age.isNotEmpty && country!=null && country.isNotEmpty && currentTown!=null && currentTown.isNotEmpty && about!=null && about.isNotEmpty && price!=null && price!="0" && avatar!=null && avatar.isNotEmpty ){
        currentIndex.value = index;
      }else{
        currentIndex.value = 0;
      }
    }
    else
    {
      print("Player");
      if(name!=null && name.isNotEmpty && email!=null && email.isNotEmpty && gender!=null && gender.isNotEmpty && age!=null && age.isNotEmpty && country!=null && country.isNotEmpty && currentTown!=null && currentTown.isNotEmpty ){
        currentIndex.value = index;
      }else{
        currentIndex.value = 0;
      }
    }
  }
}
