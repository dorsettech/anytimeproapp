import 'package:flutter/services.dart';
import 'package:gcloud/storage.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get_utils/src/extensions/dynamic_extensions.dart';
import 'package:get_storage/get_storage.dart';
import 'package:googleapis_auth/auth_io.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../constants.dart';
class PracticeVideoListController extends GetxController{
  static PracticeVideoListController get to => Get.find<PracticeVideoListController>();
  RxString userRole1 = ''.obs;
  final storage = GetStorage();
  RxBool videoLsnListLoader = false.obs;
  RxBool isLoading = false.obs;
  var jsonCredentials;
  var credentials;
  List videoList=[];
  final videoRefresh = RefreshController(initialRefresh: false);
  String folderName  = "";
  Bucket bucket;

  onVideoRecordRefresh() async {
    readJson();
    await Future.delayed(const Duration(milliseconds: 1000));
    videoRefresh.refreshCompleted();
  }
  @override
  void onInit() {
    readJson();
    super.onInit();
  }


  Future<void> readJson() async {
    userRole1.value = storage.read('role');
    videoLsnListLoader.value = true;
    var userRole = storage.read('role');
    var userId = storage.read('id');
    isLoading.value = true;

    try{
      jsonCredentials= await rootBundle.loadString('assets/jsonFiles/credentials.json');
      credentials= ServiceAccountCredentials.fromJson(jsonCredentials);
      var client = await clientViaServiceAccount(credentials, Storage.SCOPES);
      var storage1 = Storage(client, TPartyTokens.GoogleCloudStorageProjectName);
      bucket = storage1.bucket(TPartyTokens.GoogleCloudStorageBucketName,defaultPredefinedObjectAcl: PredefinedAcl.publicRead);
      var objectList =  bucket.list(prefix: "$folderName");

      /// for getting the length of bucket objects
      int objectLast =  await bucket.list(prefix: "$folderName").length;
      int last = 0;
      videoList.clear();
      if(objectLast>0)
      {
        objectList.forEach((element) async {
          print(element.name);
          if(element.name.contains(userId.toString())){
            videoList.add(element.name.toString());
          }
          last = last + 1;
          if(last == objectLast){
            videoLsnListLoader.value = false;
          }
        });
      }
      else if(objectLast<=0) {
        videoLsnListLoader.value = false;
      }
    }
    catch(e){
      printError(info: "Error");
    }
  }
}