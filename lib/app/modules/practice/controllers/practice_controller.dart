import 'package:anytime_pro/app/data/ModelClass/video_folder_list_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/practice/views/video_recording_display_view.dart';
import 'package:flutter/services.dart';
import 'package:gcloud/storage.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get_utils/src/extensions/dynamic_extensions.dart';
import 'package:get_storage/get_storage.dart';
import 'package:googleapis_auth/auth_io.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class PracticeController extends GetxController {
  static PracticeController get to => Get.find<PracticeController>();
  var jsonCredentials;
  var credentials;
  List videoList=[];
  List videoListUrl=[];
  RxBool isLoading = false.obs;
  final videoRefresh = RefreshController(initialRefresh: false);
  RxBool videoLsnListLoader = false.obs;
  RxString userRole1 = ''.obs;
  final storage = GetStorage();
  final repo = Repository();
  List<VideoFolderListModel> allVideoList;

  List<List<String>> drillVideoList=[
    ['assets/videos/Drill1.mp4','Drill 1'],
    ['assets/videos/Drill2.mp4','Drill 2'],
    ['assets/videos/Drill3.mp4','Drill 3']
  ];


  Future<void> readJson() async {
    userRole1.value = await storage.read('role');
    videoLsnListLoader.value = true;
    var userRole = storage.read('role');
    var userId = storage.read('id');
    isLoading.value = true;

    try{
      var userRole = storage.read('role');
      var userId = storage.read('id');
      jsonCredentials= await rootBundle.loadString('assets/jsonFiles/credentials.json');
      credentials= ServiceAccountCredentials.fromJson(jsonCredentials);
      var client = await clientViaServiceAccount(credentials, Storage.SCOPES);
      var storage1 = Storage(client, TPartyTokens.GoogleCloudStorageProjectName);
      var bucket = storage1.bucket(TPartyTokens.GoogleCloudStorageBucketName,defaultPredefinedObjectAcl: PredefinedAcl.publicRead);
      var objectList =  bucket.list();
      /// for getting the length of bucket objects
      int objectLast =  await bucket.list().length;
      int last = 0;
      videoList.clear();
      videoListUrl.clear();
      if(objectLast>0)
        {
          objectList.forEach((element) async {
            print(element.name);
            if(element.name.contains(userId.toString())){
              String folderName;
              var name = element.name.split("_");
              if(name.length == 5){
                if(userRole == "coach"){
                  folderName = name[3].toString();
                }else{
                  folderName = name[1].toString();
                }
                var folderDate = name[4].split("/");
                folderName += " "+folderDate[0];
              }

              // for (int i =0;i<name.length;i++)
              // {
              //   if(userRole == "coach"){
              //       // var name1 =name[i].split(userId.toString());
              //       // print(name1.length.toString());
              //       // print(name1[1].toString());
              //   }else{
              //
              //   }
              //
              // }
              if(folderName!=null){
                videoList.add(folderName);
              }
              videoListUrl.add(element.name.toString());
            }
            last = last + 1;
            if(last == objectLast){
              videoLsnListLoader.value = false;
            }
          });
        }
      else if(objectLast<=0) {
        videoLsnListLoader.value = false;
      }
    }
    catch(e){
      printError(info: "Error");
    }
  }

  Future<void> getVideoListFolder() async {
    int _id = storage.read('id');
    videoLsnListLoader.value = true;
    isLoading.value = true;
    try {
      await repo
          .fetchVideoList()
          .then((List<VideoFolderListModel> value) {
            if(value!=null){
              allVideoList= value;
              print(videoList.toString());
              videoLsnListLoader.value = false;
              isLoading.value = false;
            }else{
              print("No data");
            }
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> fetchVideoUrlData(BuildContext context, String fileName) async{
    try{
      videoLsnListLoader.value= true;
      await repo
          .fetchVideoUrl(fileName)
          .then((value){
          videoLsnListLoader.value = false;
            if(value!=null)
              {
                String tempUrl= value;
                var x= tempUrl.replaceAll('"', "");
                Navigator.push(context, MaterialPageRoute( builder: (context) => VideoRecordingDisplay(x)),);
                print(tempUrl);
              }
            else{
              print("No data Found");
            }
      });
    }catch(ex){
      print(ex);
    }
  }
  @override
  void onInit() {
    //readJson();
    getVideoListFolder();
    super.onInit();
  }

  onHomeRefresh() async {
    //_fetchPlayerHome();
    await Future.delayed(const Duration(milliseconds: 1000));
    videoRefresh.refreshCompleted();
  }

  onVideoRecordRefresh() async {
    //readJson();
    getVideoListFolder();
    await Future.delayed(const Duration(milliseconds: 1000));
    videoRefresh.refreshCompleted();
  }
}


