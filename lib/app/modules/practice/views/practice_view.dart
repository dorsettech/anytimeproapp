import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/ModelClass/video_folder_list_model.dart';
import 'package:anytime_pro/app/modules/practice/controllers/practice_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'drill_videos_view.dart';

class PracticeView extends StatefulWidget {
  @override
  State<PracticeView> createState() => _PracticeViewState();
}

class _PracticeViewState extends State<PracticeView> {
  final controller = Get.put(PracticeController());
  List<String> thumbNailList=['assets/images/Thumbnail_Drill1.png','assets/images/Thumbnail_Drill2.png','assets/images/Thumbnail_Drill3.png'];
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion(
      value: SystemUiOverlayStyle.light
          .copyWith(statusBarColor: KColors.lightBlue),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Practice'),
          automaticallyImplyLeading: false,
        ),
        body: Obx(
              () => Column(
            children: [
              controller.userRole1.value == 'coach'
                  ? _tabs()
                  : _tabs(),//_playerDashboard(),
            ],
          ),
        ),
      ),
    );
  }
  _playerDashboard(){
    return Container(
      child: Text("NO Recordings"),
    );
  }
  _tabs() => Expanded(
    child: SmartRefresher(
      controller: controller.videoRefresh,
      onRefresh: controller.onVideoRecordRefresh,
      header: BezierCircleHeader(
        bezierColor: KColors.lightBlue,
      ),
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      child: Column(
        children: [
          Expanded(
              child: DefaultTabController(
                length: 2,//3
                initialIndex: 0,
                child: Column(
                    children: [
                      TabBar(
                        padding: EdgeInsets.only(left: 20.w),
                        labelPadding: EdgeInsets.only(left: 30.w, right: 30.w),
                        isScrollable: true,
                        labelStyle: KTextStyle.f18w6.copyWith(
                            fontSize: 17.sp
                        ),
                        labelColor: KColors.darkBlue,
                        indicatorColor: KColors.lightBlue,
                        indicatorWeight: 2,// 3,
                        tabs: [
                          Tab(
                            // text: 'VIDEO WALL',
                            text: 'Skill school',
                          ),
                          Tab(
                            // text: 'HOT SHOTS',
                            // text: 'Previous Lessons',
                            text: 'Lesson Hub',
                          ),
                        ],
                      ),
                      Expanded(
                        child: Obx(() {
                          return TabBarView(
                            children: [
                              _videoWall(controller.drillVideoList),
                              controller.videoLsnListLoader.value == true ?
                              Loader(color: KColors.lightBlue)
                                  : (controller.allVideoList.isNotEmpty && controller.allVideoList!=null) ?
                              _recordingWall(controller.allVideoList)
                                  : Center(
                                  child: Text('No Video Available',
                                      style: KTextStyle.f18w6)),
                            ],
                          );
                        }
                        ),
                      )
                    ]
                ),
              )
          )
        ],
      ),
    ),
  );

  _videoWall(List<List<String>> drillVideoList) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8),
      // height: Get.height,
      // width: Get.width,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: MediaQuery.of(context).size.width /
                (MediaQuery.of(context).size.height / 1.3),
            crossAxisCount: 2,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        itemCount: drillVideoList.length,
        padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width20),
        itemBuilder: (ctx, index) => InkWell(
          onTap: (){
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => DrillVideoDisplay(drillVideoList[index][0]),)
            );
          },
          child: Container(
            // width: Get.width * 0.4,
            // height: Get.height * 0.8,
            child: Stack(
              children: [
                Align(
                  alignment: AlignmentDirectional(0, 0),
                  child: Image.asset(
                    thumbNailList[index],
                    width: Get.width,
                    // height: Get.height,
                    fit: BoxFit.fill,
                  ),
                ),
                // Align(
                //   alignment: AlignmentDirectional(0, 1),
                //   child: Container(
                //     padding: EdgeInsets.symmetric(horizontal: 8.w),
                //     width: Get.width,
                //     height: Get.height * 0.06,
                //     decoration: BoxDecoration(
                //       color: const Color(0xFF607d8b),
                //     ),
                //     child: Row(
                //       children: [
                //         Text(
                //           drillVideoList[index][1],
                //           style: KTextStyle.f12w4
                //               .copyWith(color: Colors.white),
                //         ),
                //         Spacer(),
                //         Checkbox(
                //           activeColor: const Color(0xff32bea6),
                //           value: true,
                //           onChanged: (value) {},
                //         )
                //       ],
                //     ),
                //   ),
                // ),
                Align(
                  alignment: AlignmentDirectional(0, 0),
                  child: Container(
                      padding: EdgeInsets.all(8.w),
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(10)),
                      child: Icon(
                        Icons.videocam_outlined,
                        color: Colors.white,
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _recordingWall(List<VideoFolderListModel> allVideoList) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _videoHeading('Recorded Videos'),
            ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              ///Count of List
              itemCount: allVideoList.length,
              padding: EdgeInsets.only(left: KDynamicWidth.width20),
              itemBuilder: (ctx, index) => InkWell(
                onTap: ()
                {
                  //Navigator.push( context, MaterialPageRoute( builder: (context) => VideoRecordingDisplay(allVideoList[index].fileName)),);
                  controller.fetchVideoUrlData(context,allVideoList[index].fileName);
                  //Navigator.push( context, MaterialPageRoute( builder: (context) => PracticeVideoListView(list[index])),);
                  //Navigator.push( context, MaterialPageRoute( builder: (context) => PracticeVideoListView(controller.videoListUrl[index])),);
                },
                child: Container(
                  padding: EdgeInsets.only(top: 10,right: 10),
                  width: Get.width * 0.34,
                  child: Stack(
                    children: [
                      Align(
                        alignment: AlignmentDirectional(0,0),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 8.w),
                          width: Get.width,
                          height: Get.height * 0.1,
                          decoration: BoxDecoration(
                            color: const Color(0xFF607d8b),
                          ),
                          child: Row(
                            children: [
                              Container(
                                  padding: EdgeInsets.all(8.w),
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Icon(
                                    Icons.folder,
                                    //Icons.videocam_outlined,
                                    color: Colors.white,
                                  )),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left:10),
                                  child: Text(
                                    allVideoList[index].coachName+" "+allVideoList[index].date,
                                    // index+1 == 1 ? "${index+1}st Recorded Video (LATEST)"
                                    // : index+1 == 2 ? "${index+1}nd Recorded Video"
                                    //     : index+1 == 3 ? "${index+1}rd Recorded Video"
                                    //     :"${index+1}th Recorded Video",//'Watch Your Recorded Video',
                                    style: KTextStyle.f12w4
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              separatorBuilder: (BuildContext context, int index) =>
                  SizedBox(width: 20),
            ),
          ],
        ),
      ),
    );
  }

  Padding _videoHeading(String s) {
    return Padding(
      padding: EdgeInsets.all(KDynamicWidth.width20),
      child: Text(
        s.toUpperCase(),
        style: KTextStyle.f18w6.copyWith(color: KColors.darkBlue),
      ),
    );
  }
}
