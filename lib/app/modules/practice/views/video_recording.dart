import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gcloud/storage.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:video_player/video_player.dart';
import '../../../constants.dart';

class VideoPlayerScreen extends StatefulWidget {
  String videoUrl;
  Bucket bucket;
  VideoPlayerScreen(this.videoUrl,this.bucket, {Key key}) : super(key: key);
  @override
  _VideoPlayerScreenState createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  //VideoPlayerController _controller;
  final _repository = Repository();
  Future<void> _initializeVideoPlayerFuture;
  List videoList=[];
  List<String> videoSignedList=[];
  RxBool videoLsnListLoader = false.obs;
  final Map<int, VoidCallback> _listeners = {};
  final Map<String, VideoPlayerController> _controllers = {};
  int index = 0;
  double _position = 0;
  double _buffer = 0;
  bool _lock = true,isPlaying= false;
  Future<void> getVideoList() async {
    Bucket bucket=widget.bucket;
    String videoUrl=widget.videoUrl;
    try{
      var objectList =  bucket.list(prefix: videoUrl);
      /// for getting the length of bucket objects
      int objectLast =  await bucket.list(prefix: videoUrl).length;
      int last = 0;
      videoList.clear();
      if(objectLast>0)
      {
        objectList.forEach((element) async {
          if(element.name.contains('.ts') || element.name.contains('.mp4'))
          {
            print(element.name);
            videoList.add(element.name.toString());
          }
          last = last + 1;
          if(last == objectLast){
            signedUrlList();
          }
        });
      }
    }catch(ex)
    {
      print(ex);
    }
  }

  signedUrlList() async {
    try {
      for(int i =0;i<videoList.length;i++){
        await _repository.loadVideoAPI(videoList[i])
            .then((value) async {
          setState(() {
            videoSignedList.add(value.toString());
            print(videoSignedList[i]);
            //playMusic(value);
          });
        });
      }
      videoLsnListLoader.value = false;
      print(videoSignedList.length);
      if (videoSignedList.length > 0) {
        _initController(0).then((_) {
          _playController(0);
        });
      }
      if (videoSignedList.length > 1) {
        _initController(1).whenComplete(() => _lock = false);
      }
      setState(() {
        isPlaying= true;
      });
    }
    catch (e) {
      print(e);
    }
  }
  @override
  void initState() {
    super.initState();
    videoLsnListLoader.value = true;
    try{
      getVideoList();

    }catch(e){
      print(e);
    }

  }
  @override
  void dispose() {
    _controller(index).dispose();
    _controllers.remove(videoSignedList.elementAt(index));
    _listeners.remove(index);
    super.dispose();
  }


  Future<void> _initController(int index) async {
    var controller = VideoPlayerController.network(videoSignedList.elementAt(index));
    _controllers[videoSignedList.elementAt(index)] = controller;
    await controller.initialize();
  }

  void _playController(int index) async {
    if (!_listeners.keys.contains(index)) {
      _listeners[index] = _listenerSpawner(index);
    }
    _controller(index).addListener(_listeners[index]);
    await _controller(index).play();
    setState(() {});
  }

  VideoPlayerController _controller(int index) {
    return _controllers[videoSignedList.elementAt(index)];
  }

  VoidCallback _listenerSpawner(index) {
    return () {
      int dur = _controller(index).value.duration.inMilliseconds;
      int pos = _controller(index).value.position.inMilliseconds;
      int buf = _controller(index).value.buffered.last.end.inMilliseconds;

      setState(() {
        if (dur <= pos) {
          _position = 0;
          return;
        }
        _position = pos / dur;
        _buffer = buf / dur;
      });
      if (dur - pos < 1) {
        if (index < videoSignedList.length - 1) {
          _nextVideo();
        }
      }
    };
  }


  void _nextVideo() async {
    if (_lock || index == videoSignedList.length - 1) {
      return;
    }
    _lock = true;

    _stopController(index);

    if (index - 1 >= 0) {
      _removeController(index - 1);
    }

    _playController(++index);

    if (index == videoSignedList.length - 1) {
      _lock = false;
    } else {
      _initController(index + 1).whenComplete(() => _lock = false);
    }
  }

  void _stopController(int index) {
    _controller(index).removeListener(_listeners[index]);
    _controller(index).pause();
    _controller(index).seekTo(Duration(milliseconds: 0));
  }

  void _removeController(int index) {
    _controller(index).dispose();
    _controllers.remove(videoSignedList.elementAt(index));
    _listeners.remove(index);
  }

  @override
  Widget build(BuildContext context) {
    return videoLsnListLoader.value ?
        Scaffold(
          appBar: AppBar(
            title: Text("Loading..."),
          ),
          body: Center(
             child:  Loader(color: KColors.lightBlue)
          ),
        )
        :Scaffold(
          appBar: AppBar(
            title: Text(isPlaying ? "Playing" : "Paused"),// ${index + 1} of ${videoSignedList.length}
          ),
          body: Stack(
            children: <Widget>[
              GestureDetector(
                onLongPressStart: (_) => _controller(index).pause(),
                onLongPressEnd: (_) => _controller(index).play(),
                child: Center(
                  child: AspectRatio(
                    aspectRatio: _controller(index).value.aspectRatio,
                    child: Center(child: VideoPlayer(_controller(index))),
                  ),
                ),
              ),
            ],
          ),
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                onPressed: () {
                  setState(() {
                    if (_controllers != null && _controllers.values != null) {
                      if (isPlaying) {
                        _controller(index).pause();
                        isPlaying=false;
                      } else {
                        _controller(index).play();
                        isPlaying=true;
                      }
                    }
                  });
                },
                child:  Icon(
                  isPlaying ?  Icons.pause: Icons.play_arrow ,
                ),
              ),
            ],
          ),
    );
  }
}
