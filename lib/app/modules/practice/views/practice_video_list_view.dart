import 'package:anytime_pro/app/modules/practice/controllers/practice_video_list_controller.dart';
import 'package:anytime_pro/app/modules/practice/views/video_recording.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../constants.dart';

class PracticeVideoListView extends StatefulWidget {
  String folderName;
  PracticeVideoListView(this.folderName, {Key key}) : super(key: key);

  @override
  _PracticeVideoListViewState createState() => _PracticeVideoListViewState();
}

class _PracticeVideoListViewState extends State<PracticeVideoListView> {
  final controller = Get.put(PracticeVideoListController());
  @override
  void initState() {
    controller.folderName = widget.folderName;
    // TODO: implement initState
    super.initState();
    //controller.readJson();
  }
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion(
      value: SystemUiOverlayStyle.light
          .copyWith(statusBarColor: KColors.lightBlue),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Practice Video List'),
        ),
        body: Obx(
              () => Column(
            children: [
              controller.userRole1.value == 'coach'
                  ? _tabs()
                  :  _tabs(),//_playerDashboard(),
            ],
          ),
        ),
      ),
    );
  }

  _tabs() => Expanded(
    child: SmartRefresher(
      controller: controller.videoRefresh,
      onRefresh: controller.onVideoRecordRefresh,
      header: ClassicHeader(height: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment:CrossAxisAlignment.center,
        children: [
          controller.videoLsnListLoader.value ?
          Expanded(child: Center(child: Loader(color: KColors.lightBlue)))
              : controller.videoList.isNotEmpty ?
          _recordingWall(controller.videoList)
              : Center(
              child: Text('No Video Available',
                  style: KTextStyle.f18w6)),
        ],
      ),
    ),
  );

  Padding _videoHeading(String s) {
    return Padding(
      padding: EdgeInsets.all(KDynamicWidth.width20),
      child: Text(
        s.toUpperCase(),
        style: KTextStyle.f18w6.copyWith(color: KColors.darkBlue),
      ),
    );
  }
  _recordingWall(List list) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _videoHeading('Recorded Videos'),
            ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              ///Count of List
              itemCount: list.length,
              padding: EdgeInsets.only(left: KDynamicWidth.width20),
              itemBuilder: (ctx, index) => InkWell(
                onTap: ()
                {
                  print(list[index]);
                  //Navigator.push( context, MaterialPageRoute( builder: (context) => VideoPlayerDemo()),);
                  Navigator.push( context, MaterialPageRoute( builder: (context) => VideoPlayerScreen(list[index].toString(),controller.bucket)),);
                },
                child: Container(
                  padding: EdgeInsets.only(top: 10,right: 10),
                  width: Get.width * 0.34,
                  child: Stack(
                    children: [
                      Align(
                        alignment: AlignmentDirectional(0,0),
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 8.w),
                          width: Get.width,
                          height: Get.height * 0.1,
                          decoration: BoxDecoration(
                            color: const Color(0xFF607d8b),
                          ),
                          child: Row(
                            children: [
                              Container(
                                  padding: EdgeInsets.all(8.w),
                                  decoration: BoxDecoration(
                                      color: Colors.grey,
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Icon(
                                    //Icons.folder,
                                    Icons.videocam_outlined,
                                    color: Colors.white,
                                  )),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left:10),
                                  child: Text(
                                    //list[index],
                                    index+1 == 1 ? "${index+1}st Recorded Video"
                                    : index+1 == 2 ? "${index+1}nd Recorded Video"
                                        : index+1 == 3 ? "${index+1}rd Recorded Video"
                                        :"${index+1}th Recorded Video",//'Watch Your Recorded Video',
                                    style: KTextStyle.f12w4
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              separatorBuilder: (BuildContext context, int index) =>
                  SizedBox(width: 20),
            ),
          ],
        ),
      ),
    );
  }


  _playerDashboard(){
    return Container(
      child: Text("NO Recordings"),
    );
  }
}

