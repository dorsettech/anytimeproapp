import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import '../../../constants.dart';
import 'package:auto_orientation/auto_orientation.dart';

class DrillVideoDisplay extends StatefulWidget {
  final String videoUrl;
  DrillVideoDisplay(this.videoUrl,{Key key}) : super(key: key);

  @override
  State<DrillVideoDisplay> createState() => _DrillVideoDisplayState();
}

class _DrillVideoDisplayState extends State<DrillVideoDisplay> {
  VideoPlayerController _videoPlayerController;
  bool isLoaded = false , isFullScreen=false;
  RxBool isPlayingVideo = true.obs;
  @override
  void initState() {
    var tempUrl= widget.videoUrl;
    super.initState();
    _videoPlayerController = VideoPlayerController.asset(tempUrl)
      ..initialize().then((_) {
        setState(() {
          _videoPlayerController.play();
          _videoPlayerController.setLooping(false);
          _videoPlayerController.addListener(checkVideo);
          isLoaded = true;
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Video PLayer"),),
        body: SafeArea(
          child: isLoaded ?
          _videoPlayerController.value.isInitialized ?
          Column(
            children: [
              Stack(
                children: [
                  // FittedBox(
                  //   fit: BoxFit.cover,
                  //   child: SizedBox(
                  //     width: _videoPlayerController.value.size?.width ?? 0,
                  //     height: _videoPlayerController.value.size?.height ?? 0,
                  //     child: AspectRatio(aspectRatio: _videoPlayerController.value.aspectRatio,
                  //         child: VideoPlayer(_videoPlayerController)),
                  //   ),
                  // ),
                  FittedBox(
                    fit: BoxFit.contain,
                    child: SizedBox(
                      width: isFullScreen?MediaQuery.of(context).size.width:_videoPlayerController.value.size?.width ,
                      height: isFullScreen?MediaQuery.of(context).size.height:_videoPlayerController.value.size?.height ?? 0,
                      child: VideoPlayer(_videoPlayerController),
                    ),
                  ),
                  // Positioned(
                  //   right: 0,
                  //   bottom: 5,
                  //   child: IconButton(
                  //     icon:Icon(isFullScreen?Icons.fullscreen_exit:Icons.fullscreen,color:Colors.white,size: 30,),
                  //         onPressed: () {
                  //           if(isFullScreen){
                  //             setState(() {
                  //               exitFullScreen();
                  //               isFullScreen=false;
                  //             });
                  //           }else{
                  //             setState(() {
                  //               fullScreen();
                  //               isFullScreen= true;
                  //             });
                  //           }
                  //         },
                  //   ),
                  // )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {
                      _videoPlayerController.seekTo(Duration(seconds: _videoPlayerController.value.position.inSeconds-5));
                    },
                    icon: Icon(Icons.fast_rewind,size: 30,),
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _videoPlayerController.value.isPlaying
                            ? _videoPlayerController.pause()
                            : _videoPlayerController.play();
                      });
                    },
                    icon: Icon(_videoPlayerController.value.isPlaying ? Icons.pause : Icons.play_arrow,size: 30,),
                  ),
                  IconButton(
                    onPressed: () {
                      _videoPlayerController.seekTo(Duration(seconds: _videoPlayerController.value.position.inSeconds+5));
                    },
                    icon: Icon(Icons.fast_forward,size: 30,),
                  )
                ],
              )
            ],
          )
              : Container()
              : Center(child:  Loader(color: KColors.lightBlue)),
        ),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     setState(() {
        //       _videoPlayerController.value.isPlaying
        //           ? _videoPlayerController.pause()
        //           : _videoPlayerController.play();
        //     });
        //   },
        //   child: Icon(
        //     _videoPlayerController.value.isPlaying
        //         ? Icons.pause
        //         : Icons.play_arrow,
        //   ),
        // )
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    AutoOrientation.portraitAutoMode();
    super.dispose();
  }
  void checkVideo(){
    if(_videoPlayerController.value.position == _videoPlayerController.value.duration) {
      setState(() {
        isPlayingVideo.value=false;
      });
      print('video Ended');
    }

  }

  void fullScreen(){
    setState(() {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
      ]);
      AutoOrientation.landscapeAutoMode();
    });

  }
  void exitFullScreen(){
    setState(() {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
      AutoOrientation.portraitAutoMode();
    });
  }

}