import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:video_player/video_player.dart';

import '../../../constants.dart';

class VideoRecordingDisplay extends StatefulWidget {
  String videoUrl;
   VideoRecordingDisplay(this.videoUrl,{Key key}) : super(key: key);

  @override
  State<VideoRecordingDisplay> createState() => _VideoRecordingDisplayState();
}

class _VideoRecordingDisplayState extends State<VideoRecordingDisplay> {
  VideoPlayerController _videoPlayerController;
  bool isLoaded = false;
  RxBool isPlayingVideo = true.obs;
  @override
  void initState() {
    var tempUrl= widget.videoUrl;
    super.initState();
    _videoPlayerController = VideoPlayerController.network(tempUrl)
      ..initialize().then((_) {
        setState(() {
          _videoPlayerController.play();
          _videoPlayerController.setLooping(false);
          _videoPlayerController.addListener(checkVideo);
          isLoaded = true;
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Video Player"),),
      body: SafeArea(
        child: isLoaded ?
        _videoPlayerController.value.isInitialized ?
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child:VideoPlayer(_videoPlayerController),
        )
            : Container()
            : Center(child:  Loader(color: KColors.lightBlue)),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 35.0),
            child: FloatingActionButton(
              onPressed: () {
                setState(() {
                  _videoPlayerController.seekTo(Duration(seconds: _videoPlayerController.value.position.inSeconds-5));
                });
              },
              child: Icon(Icons.replay_5_rounded),
            ),
          ),
          FloatingActionButton(
            onPressed: () {
              setState(() {
                _videoPlayerController.value.isPlaying
                    ? _videoPlayerController.pause()
                    : _videoPlayerController.play();
              });
            },
            child: Icon(
              _videoPlayerController.value.isPlaying
                  ? Icons.pause
                  : Icons.play_arrow,
            ),
          ),
          FloatingActionButton(
            onPressed: () {
              setState(() {
                _videoPlayerController.seekTo(Duration(seconds: _videoPlayerController.value.position.inSeconds+5));
              });
            },
            child: Icon(Icons.forward_5_rounded),
          )
        ],
      )
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    super.dispose();
  }
  void checkVideo(){
    if(_videoPlayerController.value.position == _videoPlayerController.value.duration) {
      setState(() {
        isPlayingVideo.value=false;
      });
      print('video Ended');
    }

  }

}
