import 'package:anytime_pro/app/data/ModelClass/login_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/controllers/biometric_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:double_back_to_close/toast.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoggedInController extends GetxController {
  final storage = GetStorage();
  String name;
  bool isLoggedIn;
  final _repo = Repository();
  final bioCtrl = Get.put(AuthController(), permanent: true);

  @override
  void onReady() {
    storage.read('isLogin') == true
        ? Future.delayed(const Duration(seconds: 0), () {
            biometricPopup();
          })
        : print('User Not Logged in');
    super.onReady();
  }

  @override
  void onInit() {
    super.onInit();
    name = storage.read('name');
    isLoggedIn = storage.read('isLogin');
  }

  biometricPopup() async {
    bool isLogin = storage.read('isLogin') ?? false;
    if (isLogin) {
      var bool = await AuthController.authenticateUser();
      if (bool) {
        await loggedInSubmit();
      }
      // } else {
      //   Get.toNamed(Routes.LOGIN);
      // }
    } else {
      Toast.show('Login first', Get.context);
    }
  }

  Future<dynamic> loggedInSubmit() async {
    Get.showOverlay(asyncFunction: () => _doLogin(), loadingWidget: Loader());
  }

  Future<void> _doLogin() async {
    String existEmail = storage.read('email');
    var existPass = storage.read('pass');

    try {
      await _repo
          .loginAPI(loginEmail: existEmail, loginPass: existPass)
          .then((LoginModel value) async {
        if (value.status == 'success') {
          await localStorageDuringLogin(value);
          // Get.offAllNamed(Routes.HOME);
          Get.offAllNamed(Routes.HOME,arguments: [1]);
        } else {
          errorSnackbar(value.message);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  localStorageDuringLogin(LoginModel value) async {
    storage.write('isLogin', true);
    storage.write('pass', storage.read('pass'));
    storage.write('token', value.data.token);
    storage.write('role', value.data.role);
    storage.write('name', value.data.metaData.firstName);
    storage.write('lastname', value.data.metaData.lastName);
    storage.write('id', value.data.iD);
    storage.write('email', value.data.userEmail);
    storage.write('cookie', value.cookie);
  }
}
