import 'package:get/get.dart';

import '../controllers/logged_in_controller.dart';

class LoggedInBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoggedInController>(
      () => LoggedInController(),
    );
  }
}
