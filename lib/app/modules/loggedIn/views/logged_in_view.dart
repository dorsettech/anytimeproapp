import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/modules/loggedIn/controllers/logged_in_controller.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

class LoggedInView extends GetView<LoggedInController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width16),
        child: SafeArea(
          child: Column(
            children: [
              SizedBox(height: Get.height / 5),
              Image.asset(
                'assets/images/small-logo.jpg',
                height: Get.height / 3,
                width: Get.width,
                fit: BoxFit.contain,
              ),
              SizedBox(height: Get.height / 15),
              MainButton(
                onPress: () => controller.loggedInSubmit(),
                title: 'LOG IN as ${controller.name}',
                buttonColor: KColors.lightBlue,
              ),
              SizedBox(height: 20.h),
              InkWell(
                  onTap: () {
                    Get.toNamed(Routes.LOGIN);
                  },
                  child: Text('NOT YOU ?')),
              SizedBox(height: 20.h)
            ],
          ),
        ),
      ),
    );
  }
}
