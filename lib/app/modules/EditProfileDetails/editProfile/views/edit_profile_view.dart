import 'dart:convert';
import 'package:age_calculator/age_calculator.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../constants.dart';
import '../../../../data/mixins/validation_mixins.dart';
import '../controllers/edit_profile_controller.dart';

class EditProfileView extends StatefulWidget {
  @override
  State<EditProfileView> createState() => _EditProfileViewState();
}

class _EditProfileViewState extends State<EditProfileView> with ValidationMixin {
  final controller = Get.put(EditProfileController());

  final storage = GetStorage();

  @override
  Widget build(BuildContext context) {
    return Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: controller.editProfileFormKey,
        child: Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
      ),
      body: Obx(() {
        return ListView(
            padding: EdgeInsets.symmetric(
                vertical: KDynamicWidth.width20,
                horizontal: KDynamicWidth.width20),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        buildInputField(
                            textController: controller.nameController,
                            title: 'Name',
                            isIcon: false),
                        buildInputField(
                            textController: controller.lastNameController,
                            title: 'Last Name',
                            isIcon: false),
                      ],
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                        onTap: () {
                          buildBottomSheet(context);
                        },
                        child: Column(
                          children: [
                            Stack(
                              children: [
                                Container(
                                  width: 120,
                                  height: 120,
                                  clipBehavior: Clip.antiAlias,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: controller.selectedImageB64.value != ''
                                      ? Image.memory(
                                    base64Decode(
                                        controller.selectedImageB64.value),
                                    fit: BoxFit.cover,
                                  )
                                      : Image.network(
                                    storage.read('avatar'),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Positioned(
                                    bottom: 0,
                                    right: 0,
                                    child: Container(
                                        width: 40,
                                        height: 40,
                                        clipBehavior: Clip.antiAlias,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: KColors.darkBlue
                                        ),
                                        child: Icon(
                                            Icons.edit,
                                            color:KColors.DARK_GREY
                                        )
                                    )
                                ),
                              ],
                            ),
                          ],
                        )),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Text(
                    'Gender',
                    style: KTextStyle.f16w5.copyWith(
                        color: Colors.grey,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1),
                  ),
                  Radio(
                    value: 1,
                    groupValue: controller.genderVal.value,
                    onChanged: (value) {
                      controller.genderVal.value = value;
                    },
                    activeColor: KColors.darkBlue,
                  ),
                  Text('MALE'),
                  Radio(
                    value: 2,
                    groupValue: controller.genderVal.value,
                    onChanged: (value) {
                      setState(() {
                        controller.genderVal.value = value;
                      });
                    },
                    activeColor: KColors.darkBlue,
                  ),
                  Text('FEMALE'),
                ],
              ),
              // buildInputField(
              //     textController: controller.nickname,
              //     title: 'Nickname',
              //     isIcon: false),
              GestureDetector(
                onTap: () async {
                  final date = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(1900), lastDate: DateTime.now());
                  DateDuration duration = AgeCalculator.age(date);
                  if (duration.years < 4) {
                    errorSnackbar('Selected date is not correct you are too young to use this app');
                    return;
                  }
                  controller.age = date.toString();
                  setState(() {
                    controller.ageController.text = duration.years.toString();
                  });
                },
                child: buildInputField(
                        validator: validateReqFields,
                        keyboardType: TextInputType.number,
                        textController: controller.ageController,
                        title: 'Age',
                        isIcon: false),

              ),
              buildInputField(
                  validator: validateReqFields,
                  textController: controller.countryController,
                  title: 'Country',
                  isIcon: false),
              buildInputField(
                  validator: validateReqFields,
                  textController: controller.currentTownController,
                  title: 'Current Town',
                  isIcon: false),
              storage.read('role') == 'coach' ?buildInputTextArea(
                  validator: validateReqFields,
                  textController: controller.aboutController,
                  title: 'About',
                  isIcon: false):SizedBox(),
              const SizedBox(
                height: 10,
              ),
              storage.read('role') == 'coach' ?buildInputField(
                  validator: validatePriceFields,
                  keyboardType: TextInputType.number,
                  textController: controller.priceController,
                  title: 'Price',
                  isIcon: false):SizedBox(),
              // DropdownButton<String>(
              //   isExpanded: true,
              //   hint: Text(
              //     'COACH TYPE',
              //     style: KTextStyle.f16w5.copyWith(
              //         color: KColors.DARK_GREY, fontWeight: FontWeight.w600),
              //   ),
              //   value: controller.coachType,
              //   items:
              //       <String>['pros', 'mid_tier', 'basic'].map((String value) {
              //     return DropdownMenuItem<String>(
              //       value: value,
              //       child: Text(
              //         value,
              //         style: KTextStyle.f16w5.copyWith(
              //             color: KColors.darkBlue, fontWeight: FontWeight.w600),
              //       ),
              //     );
              //   }).toList(),
              //   onChanged: (v) {
              //     setState(() {
              //       controller.coachType = v;
              //     });
              //   },
              // ),
              const SizedBox(
                height: 20,
              ),
              MainButton(
                title: 'update',
                buttonColor: KColors.lightBlue,
                onPress: () {
                  controller.submitUpdate(context);
                },
              )
            ]);
      }),
    ));
  }

  Future<dynamic> buildBottomSheet(BuildContext context) {
    return Get.bottomSheet(
        Container(
          padding: EdgeInsets.all(KDynamicWidth.width20),
          height: Get.height / 4,
          width: Get.width,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Divider(
                  indent: Get.width / 5,
                  endIndent: Get.width / 5,
                  thickness: 4,
                ),
                SizedBox(
                  height: KDynamicWidth.width10,
                ),
                customListTile("Choose Photo from Gallery", () async {
                  Navigator.pop(context);
                  await controller.pickImage(ImageSource.gallery);
                }),
                Divider(
                  thickness: 1,
                ),
                customListTile("Click with Camera", () async {
                  await controller.pickImage(ImageSource.camera);
                }),
              ],
            ),
          ),
        ),
        backgroundColor: Colors.white,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30))));
  }
}

Widget customListTile(_text, _onPress) {
  return ListTile(
    dense: true,
    title: Text(
      _text,
      style: KTextStyle.f18w6,
    ),
    onTap: _onPress,
  );
}
