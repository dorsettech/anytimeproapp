import 'dart:convert';
import 'dart:io';

import 'package:age_calculator/age_calculator.dart';
import 'package:anytime_pro/app/data/ModelClass/edit_profile_personal.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/profile/controllers/profile_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../../../constants.dart';

class EditProfileController extends GetxController {
  File image;
  RxString selectedImageB64 = ''.obs;
  RxString selectedImageSize = ''.obs;
  final picker = ImagePicker();
  final _repo = Repository();
  GlobalKey<FormState> editProfileFormKey = GlobalKey<FormState>();

  final storage = GetStorage();
  final isFileSelected = false.obs;
  String age = '';
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<FormState> dropDownForm = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController currentTownController = TextEditingController();
  TextEditingController nicknameController = TextEditingController();
  TextEditingController aboutController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  RxInt genderVal = 1.obs; //1 -- Male //2 -- female

  var coachType; // For editing coach type

  var avtr; //for default image

  pickImage(ImageSource imageSource) async {
    final pickedImage = await picker.pickImage(
        source: imageSource, maxWidth: 1800, maxHeight: 1800, imageQuality: 10);

    if (pickedImage != null) {
      if (!GetUtils.isImage(pickedImage.path)) {
        errorSnackbar('Invalid Image type');
      }
      isFileSelected.value = true;
      final bytes = File(pickedImage.path).readAsBytesSync();
      selectedImageB64.value = base64Encode(bytes);
      await storage.write('b64DP', selectedImageB64.value);
    } else {
      isFileSelected.value = false;
      errorSnackbar('No Image Selected');
    }
  }

  Future<dynamic> submitUpdate(BuildContext context) async {
    await hideKeyboard(context);
    if (editProfileFormKey.currentState.validate()) {
      Get.showOverlay(
          asyncFunction: () => _updateProfile(), loadingWidget: Loader());
    }
  }


  Future<void> _updateProfile() async {
    try {
        await _repo
            .editProfile(
                id: storage.read('id'),
                name: nameController.text.trim(),
                lastName: lastNameController.text.trim(),
                nickname: nicknameController.text.trim(),
                age: age,
                country: countryController.text,
                town: currentTownController.text,
                coachType: coachType,
                dp: selectedImageB64.value == ''
                    ? null
                    : selectedImageB64.value,
                about: aboutController.text.trim(),
                price: priceController.text,
                gender: genderVal.value == 1 ? 'Male' : 'Female')
            .then((EditProfilePersonal value) async {
          if (value.status == 'success') {
            Alert(
                style: alertStyle,
                context: Get.context,
                type: AlertType.info,
                buttons: <DialogButton>[
                  DialogButton(
                    color: KColors.lightBlue,
                    child: Text(
                      "COOL",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    onPressed: () {
                      Get.back();
                      Get.back();
                      successSnackbar('Successfully Updated');
                    },
                    width: 120,
                  )                ],
                content: Column(
                  children: [
                    Text("Thanks for filling in your profile! You’re now free to book your first Anytime Pro lesson. For your lesson, you will need a phone stand (tripod) as well as a set of (wireless) headphones. We recommend purchasing the following if you don’t have them already:"),
                    TextButton(
                        onPressed: () {
                          launchInBrowser(Uri.parse('https://www.amazon.co.uk/dp/B0B4687Y5C?ref_=cm_sw_r_apin_dp_PG6QGMWMMFY677D8YPQA'));
                        }, child: Text('Phone Stand', style: TextStyle(color: KColors.lightBlue),)),
                    TextButton(
                        onPressed: () {
                          launchInBrowser(Uri.parse('https://www.amazon.co.uk/dp/B0876349QT?ref_=cm_sw_r_apin_dp_0Q3Y4284SEEA4RK5Q5E9'));
                        }, child: Text('Wireless headphones', style: TextStyle(color: KColors.lightBlue),)),
                  ],
                )
            ).show();
          } else {
            errorSnackbar(value.message);
          }
        });
    } catch (e) {
      print(e);
    }
  }

  @override
  void onInit() {
    try {
      nameController.text = storage.read('name');
      lastNameController.text = storage.read('lastname') ?? '';
      avtr = storage.read('avatar') ?? '';
      age = storage.read('age') ?? '';
      ageController.text = AgeCalculator.age(DateTime.parse(age)).years.toString();
      countryController.text = storage.read('country') ?? '';
      currentTownController.text = storage.read('town') ?? '';
      selectedImageB64.value = storage.read('b64DP') ?? '';
      coachType = storage.read('coachType') ?? 'basic';

      genderVal.value = storage.read('gender') == 'Male' ? 1 : 2;
      aboutController.text = storage.read('about') ?? '';
      priceController.text = storage.read('price') ?? '';
      super.onInit();
    } catch (ex) {
      print(ex);
    }
  }

  @override
  void onClose() {
    nameController.dispose();
    lastNameController.dispose();
    ageController.dispose();
    countryController.dispose();
    currentTownController.dispose();
    nicknameController.dispose();
    ProfileController.to.getProfile();
    super.onClose();
  }
}
