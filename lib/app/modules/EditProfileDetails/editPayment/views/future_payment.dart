import 'dart:convert';
import 'dart:developer';

import 'package:anytime_pro/app/modules/EditProfileDetails/editPayment/views/response_card.dart';
import 'package:flutter/material.dart' hide Card;
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

import '../../../../constants.dart';

class SetupFuturePaymentScreen extends StatefulWidget {
  @override
  _SetupFuturePaymentScreenState createState() =>
      _SetupFuturePaymentScreenState();
}

class _SetupFuturePaymentScreenState extends State<SetupFuturePaymentScreen> {
  PaymentIntent _retrievedPaymentIntent;
  CardFieldInputDetails _card;
  SetupIntent _setupIntentResult;
  String _email = 'email@stripe.com';
  final storage = GetStorage();

  int step = 0;

  @override
  Widget build(BuildContext context) {
    return ExampleScaffold(
      title: 'Setup Future Payment',
      children: [
        Padding(
          padding: EdgeInsets.all(16),
          child: TextFormField(
            initialValue: _email,
            decoration: InputDecoration(hintText: 'Email', labelText: 'Email'),
            onChanged: (value) {
              setState(() {
                _email = value;
              });
            },
          ),
        ),
        Padding(
          padding: EdgeInsets.all(16),
          child: CardField(
            onCardChanged: (card) {
              setState(() {
                _card = card;
              });
            },
          ),
        ),
        Stepper(
          controlsBuilder: emptyControlBuilder,
          currentStep: step,
          steps: [
            Step(
              title: Text('Save card'),
              content: ElevatedButton(
                onPressed: _card?.complete == true ? _handleSavePress : null,
                child: Text('Save'),
              ),
            ),
            Step(
              title: Text('Pay with saved card'),
              content: ElevatedButton(
                onPressed: _setupIntentResult != null
                    ? _handleOffSessionPayment
                    : null,
                child: Text('Pay with saved card off-session'),
              ),
            ),
            Step(
              title: Text('[Extra] Recovery Flow - Authenticate payment'),
              content: Column(
                children: [
                  Text(
                      'If the payment did not succeed. Notify your customer to return to your application to complete the payment. We recommend creating a recovery flow in your app that shows why the payment failed initially and lets your customer retry.'),
                  SizedBox(height: 8),
                  ElevatedButton(
                    onPressed: _retrievedPaymentIntent != null
                        ? _handleRecoveryFlow
                        : null,
              child: Text('Authenticate payment (recovery flow)'),

    ),
                ],
              ),
            ),
          ],
        ),
        if (_setupIntentResult != null)
          Padding(
            padding: EdgeInsets.all(16),
            child: ResponseCard(
              response: _setupIntentResult.toJson().toPrettyString(),
            ),
          ),
      ],
    );
  }

  Future<void> _handleSavePress() async {
    if (_card == null) {
      return;
    }
    try {
      // 1. Create setup intent on backend
      final clientSecret = await _createSetupIntentOnBackend(_email);

      // 2. Gather customer billing information (ex. email)
      final billingDetails = BillingDetails(
        name: "Test User",
        email: 'email@stripe.com',
        phone: '+48888000888',
        address: Address(
          city: 'Houston',
          country: 'US',
          line1: '1459  Circle Drive',
          line2: '',
          state: 'Texas',
          postalCode: '77063',
        ),
      ); // mo/ mocked data for tests

      // 3. Confirm setup intent

      print('secret ${jsonDecode(clientSecret)}');
      final setupIntentResult = await Stripe.instance.confirmSetupIntent(
        jsonDecode(clientSecret),
         PaymentMethodParams.card(
          paymentMethodData: PaymentMethodData(
            billingDetails: billingDetails,
          ),
        ),
      );
      log('Setup Intent created $setupIntentResult');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Success: Setup intent created.',
          ),
        ),
      );
      setState(() {
        step = 1;
        _setupIntentResult = setupIntentResult;
      });
    } catch (error, s) {
      log('Error while saving payment', error: error, stackTrace: s);
      print(error);
      print(s);
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Error code: $error')));
    }
  }

  Future<void> _handleOffSessionPayment() async {
    final res = await _chargeCardOffSession();
    if (res['error'] != null) {
      // If the PaymentIntent has any other status, the payment did not succeed and the request fails.
      // Notify your customer e.g., by email, text, push notification) to complete the payment.
      // We recommend creating a recovery flow in your app that shows why the payment failed initially and lets your customer retry.
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
              'Error!: The payment could not be completed! ${res['error']}')));
      await _handleRetrievePaymentIntent(res['clientSecret']);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Success!: The payment was confirmed successfully!')));
      setState(() {
        step = 2;
      });
    }

    log('charge off session result: $res');
  }

  // When customer back to the App to complete the payment, retrieve the PaymentIntent via clientSecret.
  // Check the PaymentIntent’s lastPaymentError to inspect why the payment attempt failed.
  // For card errors, you can show the user the last payment error’s message. Otherwise, you can show a generic failure message.
  Future<void> _handleRetrievePaymentIntent(String clientSecret) async {
    final paymentIntent =
    await Stripe.instance.retrievePaymentIntent(clientSecret);

    final paymentMethodId = paymentIntent.paymentMethodId == null
        ? _setupIntentResult?.paymentMethodId
        : paymentIntent.paymentMethodId;

    setState(() {
      _retrievedPaymentIntent =
          paymentIntent.copyWith(paymentMethodId: paymentMethodId);
    });
  }

  //  https://stripe.com/docs/payments/save-and-reuse?platform=ios#start-a-recovery-flow
  Future<void> _handleRecoveryFlow() async {
    // TODO lastPaymentError
    if (_retrievedPaymentIntent?.paymentMethodId != null && _card != null) {
      await Stripe.instance.confirmPayment(
        _retrievedPaymentIntent.clientSecret,
         PaymentMethodParams.cardFromMethodId(
          paymentMethodData: PaymentMethodDataCardFromMethod(
              paymentMethodId: _retrievedPaymentIntent.paymentMethodId),
        ),
      );
    }
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Success!: The payment was confirmed successfully!')));
  }

  Future<String> _createSetupIntentOnBackend(String email) async {

    final url = Uri.parse('${KApiTexts.BASE_URL}${KApiTexts.saveCardFuture}');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({
        'player': storage.read('id'),
      }),
    );
    // final Map<String, dynamic> bodyResponse = json.decode(response.body);
    // final clientSecret = bodyResponse['clientSecret'] as String;
    // log('Client token  $clientSecret');

    return response.body;
  }

  Future<Map<String, dynamic>> _chargeCardOffSession() async {
    final url = Uri.parse('${KApiTexts.BASE_URL}/charge-card-off-session');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({'email': _email}),
    );
    return json.decode(response.body);
  }
}

extension PrettyJson on Map<String, dynamic> {
  String toPrettyString() {
    var encoder = new JsonEncoder.withIndent("     ");
    return encoder.convert(this);
  }
}


class ExampleScaffold extends StatelessWidget {
  final List<Widget> children;
  final List<String> tags;
  final String title;
  final EdgeInsets padding;
  const ExampleScaffold({
    Key key,
    this.children = const [],
    this.tags = const [],
    this.title = '',
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 60),
            Padding(
              child: Text(title, style: Theme.of(context).textTheme.headline5),
              padding: EdgeInsets.symmetric(horizontal: 20),
            ),
            SizedBox(height: 4),
            Padding(
              child: Row(
                children: [
                  for (final tag in tags) Chip(label: Text(tag)),
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 20),
            ),
            SizedBox(height: 20),
            if (padding != null)
              Padding(
                padding: padding,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: children,
                ),
              )
            else
              ...children,
          ],
        ),
      ),
    );
  }
}






class PaymentSheetScreenWithCustomFlow extends StatefulWidget {
  @override
  _PaymentSheetScreenState createState() => _PaymentSheetScreenState();
}

class _PaymentSheetScreenState extends State<PaymentSheetScreenWithCustomFlow> {
  int step = 0;

  @override
  Widget build(BuildContext context) {
    return ExampleScaffold(
      title: 'Payment Sheet',
      tags: ['Custom Flow'],
      children: [
        Stepper(
          controlsBuilder: emptyControlBuilder,
          currentStep: step,
          steps: [
            Step(
              title: Text('Init payment'),
              content: ElevatedButton(
                onPressed: initPaymentSheet,
                child: Text('Init payment sheet'),
              ),
            ),
            Step(
              title: Text('Select payment method'),
              content: ElevatedButton(
                onPressed: presentPaymentSheet,
                child: Text('Select payment method'),
              ),
            ),
            Step(
              title: Text('Confirm payment'),
              content: ElevatedButton(
                onPressed: confirmPayment,
                child: Text('Pay now'),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Future<void> initPaymentSheet() async {
    try {
      // 1. create payment intent on the server
      final data = await createTestPaymentSheet();

      // 2. initialize the payment sheet
      await Stripe.instance.initPaymentSheet(
        paymentSheetParameters: SetupPaymentSheetParameters(
          // Enable custom flow
          customFlow: true,
          // Main params
          merchantDisplayName: 'Flutter Stripe Store Demo',
          paymentIntentClientSecret: data['paymentIntent'],
          // Customer keys
          customerEphemeralKeySecret: data['ephemeralKey'],
          customerId: data['customer'],
          // Extra options
          applePay: PaymentSheetApplePay(
            merchantCountryCode: 'DE',
          ),
          googlePay: PaymentSheetGooglePay(merchantCountryCode: 'DE'),
          style: ThemeMode.dark,
        ),
      );
      setState(() {
        step = 1;
      });
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Error: $e')),
      );
      rethrow;
    }
  }

  Future<void> presentPaymentSheet() async {
    try {
      // 3. display the payment sheet.
      await Stripe.instance.presentPaymentSheet();

      setState(() {
        step = 2;
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Payment option selected'),
        ),
      );
    } on Exception catch (e) {
      if (e is StripeException) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Error from Stripe: ${e.error.localizedMessage}'),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Unforeseen error: ${e}'),
          ),
        );
      }
    }
  }

  Future<void> confirmPayment() async {
    try {
      // 4. Confirm the payment sheet.
      await Stripe.instance.confirmPaymentSheetPayment();

      setState(() {
        step = 0;
      });

      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Payment succesfully completed'),
        ),
      );
    } on Exception catch (e) {
      if (e is StripeException) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Error from Stripe: ${e.error.localizedMessage}'),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Unforeseen error: ${e}'),
          ),
        );
      }
    }
  }

  Future<Map<String, dynamic>> createTestPaymentSheet() async {
    final url = Uri.parse('${KApiTexts.BASE_URL}/payment-sheet');
    final response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode({
        'a': 'a',
      }),
    );
    final body = json.decode(response.body);

    if (body['error'] != null) {
      throw Exception('Error code: ${body['error']}');
    }

    return body;
  }
}

final ControlsWidgetBuilder emptyControlBuilder = (_, __) => Container();