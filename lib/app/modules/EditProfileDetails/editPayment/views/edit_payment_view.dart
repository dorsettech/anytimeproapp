import 'package:anytime_pro/app/constants.dart';

import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_stripe/flutter_stripe.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../controllers/edit_payment_controller.dart';
import 'card_utils.dart';

class EditPaymentView extends GetView<EditPaymentController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Payment'),
      ),
      body: Obx(() {
        return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: Get.width,
                child: CreditCardWidget(
                    cardNumber: controller.cardNo.value,
                    expiryDate:
                        "${controller.expMonth.value}/${controller.expYear.value}",
                    cardHolderName: controller.name.value,
                    cvvCode: '',
                    showBackView: false,
                    isHolderNameVisible: true,
                    cardBgColor: KColors.darkBlue,
                    obscureCardNumber: true,
                    obscureCardCvv: false,
                    animationDuration: const Duration(milliseconds: 800),
                    onCreditCardWidgetChange: (CreditCardBrand) {}
                    // cardType:,
                    ),
              ),
              _textField(
                  ctrller: controller.cardHolderName,
                  isEnable: true,
                  title: 'Card Holder Name',
                  inputFormat: [
                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]'))
                  ],
                  onChange: (val) {
                    controller.name.value = val;
                  },
                  type: TextInputType.name),
              SizedBox(height: 8,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18.0),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black54,
                          width: 1.5
                      ),
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  child:
                  CardField(
                    onCardChanged: (card) {
                      controller.cardIsComplete.value = card.complete;
                      controller.cardNo.value = card.last4;
                      controller.expMonth.value = card.expiryMonth.toString();
                      controller.expYear.value = card.expiryYear.toString();
                    },
                  ),
                ),
              ),
              // _textField(
              //     ctrller: controller.cardNumber,
              //     title: 'Card Number',
              //     maxLength: 18,
              //     inputFormat: [FilteringTextInputFormatter.digitsOnly],
              //     icon: Padding(
              //       padding: const EdgeInsets.only(right: 8.0),
              //       child: getCardTypeIcon(controller.cardNo.value),
              //     ),
              //     onChange: (val) {
              //       controller.cardNo.value = val;
              //     },
              //     type: TextInputType.number),
              // Row(
              //   children: [
              //     Expanded(
              //       child: _textField(
              //           ctrller: controller.expiryMonth,
              //           title: 'Month',
              //           maxLength: 2,
              //           inputFormat: [FilteringTextInputFormatter.digitsOnly],
              //           onChange: (val) {
              //             controller.expMonth.value = val;
              //           },
              //           type: TextInputType.number),
              //     ),
              //     Expanded(
              //       child: _textField(
              //           ctrller: controller.expiryYearCtrl,
              //           title: 'Year',
              //           maxLength: 2,
              //           inputFormat: [FilteringTextInputFormatter.digitsOnly],
              //           onChange: (val) {
              //             controller.expYear.value = val;
              //           },
              //           type: TextInputType.number),
              //     ),
              //   ],
              // ),

              SizedBox(height: 8,),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 8),
                    child: Transform.scale(
                      scale: 1.5,
                      child: Checkbox(value: controller.rememberMyDetail.value, onChanged: (bool value) {
                        controller.rememberMyDetail.value = value;
                      },
                      checkColor: Colors.white, fillColor: MaterialStateColor.resolveWith((states)  {
                          if (states.contains(MaterialState.selected)) {
                            return KColors.lightBlue; // the color when checkbox is selected;
                          }
                          return Colors.grey;
                        }),
                        shape: CircleBorder(),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text('Remember my details'),
                  ),
                ],
              ),

              // CreditCardForm(
              //   formKey: controller.cardFormKey,
              //   obscureCvv: true,
              //   obscureNumber: false,
              //   cardNumber: controller.cardNumber.value,
              //   expiryDate: controller.expiryDate.value,
              //   cardHolderName: controller.cardHolderName.value,
              //   cvvCode: controller.cvvCode.value,
              //   themeColor: KColors.lightBlue,
              //   textColor: KColors.lightBlue,
              //   cvvValidationMessage: 'Invalid CVV',
              //   cardNumberDecoration: InputDecoration(
              //     focusedBorder: OutlineInputBorder(),
              //     border: OutlineInputBorder(),
              //     suffixIcon: Padding(
              //       padding: const EdgeInsets.only(right: 8.0),
              //       child: getCardTypeIcon(controller.cardNumber.value),
              //     ),
              //     labelText: 'Card Number',
              //     labelStyle:
              //         KTextStyle.f14w4.copyWith(color: KColors.lightBlue),
              //     hintText: 'XXXX XXXX XXXX XXXX',
              //   ),
              //   expiryDateDecoration: InputDecoration(
              //     border: OutlineInputBorder(),
              //     labelStyle:
              //         KTextStyle.f14w4.copyWith(color: KColors.lightBlue),
              //     focusedBorder: OutlineInputBorder(),
              //     labelText: 'Expired Date',
              //     hintText: 'XX/XX',
              //   ),
              //   cvvCodeDecoration: InputDecoration(
              //     labelStyle:
              //         KTextStyle.f14w4.copyWith(color: KColors.lightBlue),
              //     border: OutlineInputBorder(),
              //     focusedBorder: OutlineInputBorder(),
              //     labelText: 'CVV',
              //     hintText: 'XXX',
              //   ),
              //   cardHolderDecoration: InputDecoration(
              //     border: OutlineInputBorder(),
              //     labelStyle:
              //         KTextStyle.f14w4.copyWith(color: KColors.lightBlue),
              //     focusedBorder: OutlineInputBorder(),
              //     labelText: 'Card Holder',
              //   ),
              //   onCreditCardModelChange: onCreditCardModelChange,
              // ),

              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MainButton(
                    title: 'Save',
                    buttonColor: controller.cardIsComplete.isTrue ? KColors.lightBlue : KColors.lightishGrey,
                    onPress: () => controller.cardIsComplete.isTrue == true ? controller.submitPaymentUpdate(context) : null),
              )
            ],
          ),
        );
      }),
    );
  }

  _textField(
      {TextEditingController ctrller,
        bool isEnable = false,
      Function onChange,
      String title,
      int maxLength,
      TextInputType type,
      List<TextInputFormatter> inputFormat,
      String Function(String) validation,
      icon}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.w),
      child: TextFormField(
        enabled: isEnable,
        controller: ctrller,
        minLines: 1,
        maxLength: maxLength,
        onChanged: onChange,
        textInputAction: TextInputAction.next,
        validator: validation,
        inputFormatters: inputFormat,
        keyboardType: type,
        decoration: InputDecoration(
          suffixIcon: icon,
          labelText: title,
          counterText: '',
          labelStyle: KTextStyle.f14w4.copyWith(color: KColors.lightBlue),
          contentPadding: EdgeInsets.fromLTRB(10.w, 0, 0, 0),
          enabledBorder: OutlineInputBorder(),
          disabledBorder: OutlineInputBorder(),
          focusedBorder: OutlineInputBorder(),
          errorBorder: OutlineInputBorder(),
        ),
      ),
    );
  }
}

//   void onCreditCardModelChange(CreditCardModel creditCardModel) {
//     controller.cardNumber.value = creditCardModel.cardNumber;
//     controller.expiryDate.value = creditCardModel.expiryDate;
//     controller.cardHolderName.value = creditCardModel.cardHolderName;
//     controller.cvvCode.value = creditCardModel.cvvCode;
//     controller.isCvvFocused.value = creditCardModel.isCvvFocused;
//   }
// }
