import 'package:anytime_pro/app/data/ModelClass/edit_profile_personal.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/paymentPage/controllers/payment_page_controller.dart';
import 'package:anytime_pro/app/modules/profile/controllers/profile_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class EditPaymentController extends GetxController {
  var cardNumber = TextEditingController();
  var expiryYearCtrl = TextEditingController();
  var expiryMonth = TextEditingController();
  var cardHolderName = TextEditingController();
  final _repo = Repository();

  final storage = GetStorage();
  RxString cardNo = ''.obs;
  RxString expMonth = ''.obs;
  RxString expYear = ''.obs;
  RxString name = ''.obs;
  RxBool rememberMyDetail = false.obs;
  RxBool cardIsComplete = false.obs;
  var secretKey = '';

  @override
  void onInit() {
    super.onInit();
    cardNumber.text = storage.read('crdNum') ?? '';

    cardHolderName.text = storage.read('crdHolder') ?? '';

    print('storage.read('') ${storage.read('rememberCardDetail')}');
    rememberMyDetail.value = storage.read('rememberCardDetail') ?? false;
    saveCard();
  }

  saveCard() async =>  await _repo.getSaveCard(storage.read('id'));


  Future<dynamic> submitPaymentUpdate(BuildContext context) async {
    await hideKeyboard(context);
    Get.showOverlay(
        asyncFunction: () => _updatePayment(), loadingWidget: Loader());
  }

  Future<void> _updatePayment() async {
    try {
      await _repo
          .editPaymentInfo(
        id: storage.read('id'),
        ex_year: expiryYearCtrl.text,
        ex_month: expiryMonth.text,
        card_number: cardNumber.text,
        name_on_card: cardHolderName.text.trim().capitalizeFirst,
        rememberCardDetail: rememberMyDetail.value
      )
          .then((EditProfilePersonal value) async {
        if (value.status == 'success') {
          Get.back();
          successSnackbar('Successfully Updated');
        } else {
          errorSnackbar(value.message, sec: 6);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    ProfileController.to.getProfile();
    PaymentPageController.to.saveCard();
    expiryYearCtrl.dispose();
    expiryMonth.dispose();
    cardHolderName.dispose();
    cardNumber.dispose();
    super.onClose();
  }
}
