import 'package:anytime_pro/app/data/ModelClass/edit_profile_personal.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/modules/profile/controllers/profile_controller.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class EditGolfDetailController extends GetxController {
  final _repo = Repository();
  final storage = GetStorage();
  final isFileSelected = false.obs;

  TextEditingController homeGolfHouse = TextEditingController();
  TextEditingController favCourse = TextEditingController();
  TextEditingController curntHandicapCtrl = TextEditingController();
  String golfingSpecialism = "shot power";
  var item = ['shot power', 'driver length', 'putting accuracy'];
  RxInt drivingRange = 1.obs; //1 -- Home //2 -- Secondary
  RxBool isTestimonial = false.obs;

  @override
  void onInit() {
    super.onInit();
    homeGolfHouse.text = storage.read('glfHouse');
    favCourse.text = storage.read('favCourse') ?? '';
    curntHandicapCtrl.text = storage.read('curHandicap') ?? '';
    drivingRange.value = storage.read('driRange') == 'Home' ? 1 : 2;
    // storage.read('specialism') != ''
    //     ? golfingSpecialism = storage.read('specialism')
    //     : print('coach type not set');
    isTestimonial.value = storage.read('testimonial') == 'true' ? true : false;
  }

  Future<dynamic> submitGolfingUpdate(BuildContext context) async {
    await hideKeyboard(context);
    Get.showOverlay(
        asyncFunction: () => _updateGolfingDetails(), loadingWidget: Loader());
  }

  Future<void> _updateGolfingDetails() async {
    try {
      await _repo
          .editGolfingDetails(
              id: storage.read('id'),
              handicap: curntHandicapCtrl.text.trim(),
              homeDrivingRange: drivingRange.value == 1 ? 'Home' : 'Secondary',
              homeGolfHouse: homeGolfHouse.text.trim(),
              favoriteCourse: favCourse.text.trim(),
              golfingSpecialisms: golfingSpecialism,
              testimonial: '' // TODO : ask
              )
          .then((EditProfilePersonal value) async {
        if (value.status == 'success') {
          Get.back();
          successSnackbar('Successfully Updated');
        } else {
          errorSnackbar(value.message);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void onClose() {
    super.onClose();
    ProfileController.to.getProfile();
    homeGolfHouse.dispose();
    favCourse.dispose();
    curntHandicapCtrl.dispose();
  }
}
