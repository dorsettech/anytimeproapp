import 'package:get/get.dart';

import '../controllers/edit_golf_detail_controller.dart';

class EditGolfDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EditGolfDetailController>(
      () => EditGolfDetailController(),
    );
  }
}
