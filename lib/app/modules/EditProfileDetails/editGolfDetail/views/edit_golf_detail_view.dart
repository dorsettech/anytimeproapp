import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../../../../constants.dart';
import '../controllers/edit_golf_detail_controller.dart';

class EditGolfDetailView extends StatefulWidget {
  @override
  State<EditGolfDetailView> createState() => _EditGolfDetailViewState();
}

class _EditGolfDetailViewState extends State<EditGolfDetailView> {
  final _c = Get.put(EditGolfDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit GolfDetails'),
        ),
        body: Obx(() {
          return ListView(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            shrinkWrap: true,
            children: [
              buildInputField(
                  textController: _c.curntHandicapCtrl,
                  title: 'Current Handicap',
                  isIcon: false),
              const SizedBox(height: 20),
              Text(
                'Home Driving Range',
                style: KTextStyle.f16w5.copyWith(
                    color: KColors.DARK_GREY, fontWeight: FontWeight.w600),
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  Radio(
                    visualDensity: VisualDensity.compact,
                    value: 1,
                    groupValue: _c.drivingRange.value,
                    onChanged: (value) {
                      _c.drivingRange.value = value;
                    },
                  ),
                  Text('HOME'),
                  const SizedBox(width: 40),
                  Radio(
                    value: 2,
                    groupValue: _c.drivingRange.value,
                    onChanged: (value) {
                      _c.drivingRange.value = value;
                    },
                  ),
                  Text('SECONDARY'),
                ],
              ),
              buildInputField(
                  keyboardType: TextInputType.number,
                  textController: _c.homeGolfHouse,
                  title: 'Home Golf House',
                  isIcon: false),
              buildInputField(
                  textController: _c.favCourse,
                  title: 'Favourite Course',
                  isIcon: false),
              const SizedBox(height: 10),
              DropdownButton<String>(
                isExpanded: true,
               /* hint: Text(
                  'shot power',
                  style: KTextStyle.f16w5.copyWith(
                      color: KColors.DARK_GREY, fontWeight: FontWeight.w600),
                ),*/
                value: _c.golfingSpecialism,
                items:_c.item.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(
                      value,
                      style: KTextStyle.f16w5.copyWith(
                          color: KColors.darkBlue, fontWeight: FontWeight.w600),
                    ),
                  );
                }).toList(),
                onChanged: (v) {
                  setState(() {
                    _c.golfingSpecialism = v;
                  });
                },
              ),
              SizedBox(height: 20.w),
              // Row(
              //   children: [
              //     Expanded(
              //         child: Checkbox(
              //             fillColor:
              //                 MaterialStateProperty.all(KColors.lightBlue),
              //             value: _c.isTestimonial.value,
              //             onChanged: (bool value) {
              //               _c.isTestimonial.value = value;
              //               print(_c.isTestimonial.value);
              //             })),
              //     Expanded(
              //         flex: 4,
              //         child: Text(
              //             'this might be dictator, at least, regularly vetted against user testimonials and rating against each specialism'
              //                 .toUpperCase(),
              //             style: KTextStyle.f16w5))
              //   ],
              // ),
              // SizedBox(height: Get.height / 6),
              MainButton(
                title: 'update',
                buttonColor: KColors.lightBlue,
                onPress: () {
                  _c.submitGolfingUpdate(context);
                },
              )
            ],
          );
        }));
  }
}
