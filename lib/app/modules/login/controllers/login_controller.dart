import 'package:anytime_pro/app/data/ModelClass/login_model.dart';
import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  RxString onChangePass = ''.obs;
  RxBool isLogin = false.obs;
  static LoginController get to => Get.find<LoginController>();

  final loginEmailCont = TextEditingController();
  final loginPassCont = TextEditingController();
  final GlobalKey<FormState> loginFormKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> loginScaffoldKey = GlobalKey<ScaffoldState>();
  final storage = GetStorage();
  final _repository = Repository();
  RxBool passwordVisibility = true.obs;

  Future<dynamic> loginSubmit(context) async {
    if (loginFormKey.currentState.validate()) {
      await hideKeyboard(context);
      Get.showOverlay(asyncFunction: () => _doLogin(), loadingWidget: Loader());
    }
  }

  Future<void> _doLogin() async {
    try {
      await _repository
          .loginAPI(
              loginEmail: loginEmailCont.text.trim(),
              loginPass: loginPassCont.text)
          .then((LoginModel value) async {
            if(value != null){
              if (value.status == 'success') {
                await localStorageDuringLogin(value);
                //Get.offNamed(Routes.WALKTHROUGH);
                if(value.data.metaData.userCustomFlag!=null){
                  print("Custom Flag Value:${value.data.metaData.userCustomFlag}");
                  if(value.data.metaData.userCustomFlag=="0"){
                    Get.offNamed(Routes.Landing);
                  }else{
                    // Get.offAllNamed(Routes.HOME);
                    Get.offAllNamed(Routes.HOME,arguments: [1]);
                  }
                }
                else{
                  // Get.offAllNamed(Routes.HOME);
                  Get.offAllNamed(Routes.HOME,arguments: [1]);
                }
              } else {
                errorSnackbar(value.message);
              }
            }else{
              errorSnackbar(value.message);
            }
      });
    } catch (e) {
      print(e);
    }
  }

  localStorageDuringLogin(LoginModel value) async {
    storage.write('isLogin', true);
    storage.write('pass', loginPassCont.text);
    storage.write('token', value.data.token);
    storage.write('role', value.data.role);
    storage.write('name', value.data.metaData.firstName);
    storage.write('lastname', value.data.metaData.lastName);
    storage.write('id', value.data.iD);
    storage.write('email', value.data.userEmail);
    storage.write('cookie', value.cookie);
  }

  @override
  void onClose() {
    loginPassCont.dispose();
    loginEmailCont.dispose();
    super.onClose();
  }

  @override
  void onInit() {
    super.onInit();
  }
}
