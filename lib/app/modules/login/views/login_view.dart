import 'package:animate_do/animate_do.dart';
import 'package:anytime_pro/app/constants.dart';
import 'package:anytime_pro/app/data/mixins/validation_mixins.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> with ValidationMixin {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: controller.loginFormKey,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Scaffold(
        key: controller.loginScaffoldKey,
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: KDynamicWidth.width16),
          child: SafeArea(
            child: Column(
              children: [
                Hero(
                  tag: 'logo',
                  child: Image.asset(
                    'assets/images/small-logo.jpg',
                    height: Get.height / 3,
                    width: Get.width,
                    fit: BoxFit.contain,
                  ),
                ),
                Obx(() {
                  return FadeInUp(
                    child: Column(
                      children: [
                        buildInputEmailField(
                            validator: validateEmail,
                            textController: controller.loginEmailCont,
                            title: 'Email Address',
                            isIcon: false),
                        buildInputField(
                            validator: validateReqFields,
                            textController: controller.loginPassCont,
                            title: 'Password',
                            onChange: (val) {
                              controller.onChangePass.value = val;
                            },
                            isIcon: true),
                        SizedBox(height: 30.h),
                        MainButton(
                          onPress: () => controller.loginSubmit(context),
                          title: 'LOG IN',
                          buttonColor: KColors.lightBlue,
                        ),
                        SizedBox(height: 20.h),
                        /// Social Media Login button
                        // Row(
                        //   mainAxisSize: MainAxisSize.max,
                        //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        //   crossAxisAlignment: CrossAxisAlignment.start,
                        //   children: [
                        //     Expanded(
                        //       child: MainButton(
                        //           onPress: () {},
                        //           title: 'Google',
                        //           buttonColor: KColors.lightBlue),
                        //     ),
                        //     SizedBox(width: 10.h),
                        //     Expanded(
                        //       child: MainButton(
                        //           onPress: () {},
                        //           title: 'facebook',
                        //           buttonColor: KColors.lightBlue),
                        //     ),
                        //   ],
                        // ),
                        SizedBox(height: 30.h),
                        InkWell(
                            onTap: () => Get.toNamed(Routes.FORGOT_PASS),
                            child: Text('FORGOT YOUR PASSWORD ?')),
                        SizedBox(height: 30.h),
                        MainButton(
                            onPress: () => Get.toNamed(Routes.SIGNUP),
                            title: 'Register'.toUpperCase(),
                            buttonColor: KColors.darkBlue),
                      ],
                    ),
                  );
                })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
