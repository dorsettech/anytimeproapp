import 'package:anytime_pro/app/modules/landingPage/controllers/lading_controller.dart';
import 'package:anytime_pro/app/routes/app_pages.dart';
import 'package:anytime_pro/app/utils/global_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../constants.dart';

class LandingView extends GetView<LandingController>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(left: 20,right: 20),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                /// Space
                const SizedBox(height: 30),

                ///Icon
                Image.asset(
                  "assets/images/small-logo.jpg",
                  height: Get.height / 6,
                  width: Get.width,
                  fit: BoxFit.contain,
                ),

                /// Space
                const SizedBox(height: 30),

                /// Welcome Text
                Text(
                  "Welcome to Anytime Pro",
                  style: KTextStyle.f20w5.copyWith(color: KColors.lightBlue,fontWeight: FontWeight.bold),
                ),

                /// Space
                SizedBox(height: KDynamicWidth.width20),

                ///Text
                Text(
                  "You now have access to the new age of golf coaching...",
                  style: KTextStyle.f16w5.copyWith(color: Colors.black),
                ),

                /// Space
                SizedBox(height: KDynamicWidth.width20),

                /// first
                appIconAndText("Search for golf coaches available at times you want, even within the hour"),

                /// second
                appIconAndText("Read reviews and specialism to decide which coach is best for you."),

                /// third
                appIconAndText("Book a one-to-one lesson and join your coach live, via your phone."),

                /// forth
                appIconAndText("Use the Skill School and Lesson Hub to access a wealth of offline coaching."),

                ///Text
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        "Fill in your personal profile to get started ...",
                        style: KTextStyle.f16w5.copyWith(color: Colors.black),
                      ),
                    ),
                  ],
                ),

                /// Space
                SizedBox(height: KDynamicWidth.width50),

                /// button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: MainButton(
                    buttonColor: KColors.darkBlue,
                    onPress: (){
                      // /// leading default screen
                      //Get.offNamed(Routes.WALKTHROUGH);
                      controller.updateWalkThroughFlag();
                      /// navigation home screen
                      // Get.offAllNamed(Routes.HOME);
                      Get.offAllNamed(Routes.HOME,arguments: [0]);
                    },
                    title: "Let's Go",
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Rows
  appIconAndText(String textVal){
    return  Container(
      padding: EdgeInsets.only(bottom: KDynamicWidth.width20),
      child: Row(
        children: [
          Image.asset(
            "assets/images/appIcon.png",
            width: Get.width / 9,
            fit: BoxFit.contain,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              textVal,
              style: KTextStyle.f16w5.copyWith(color: Colors.black),
              // style: KTextStyle.f16w5.copyWith(color: Colors.grey),
            ),
          ),
        ],
      ),
    );
  }

}