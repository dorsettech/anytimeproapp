import 'package:anytime_pro/app/data/Repository/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';


class LandingController extends GetxController {
  final repo = Repository();
  static LandingController get to => Get.find<LandingController>();
  PageController pageViewController;
  @override
  void onInit() {
    super.onInit();
  }

  Future<void> updateWalkThroughFlag() async{
    try{
      await repo.updateTourViewFlag();
    }catch(ex){
      print(ex);
    }
  }


}