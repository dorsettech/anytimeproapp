import 'package:anytime_pro/app/modules/landingPage/controllers/lading_controller.dart';
import 'package:get/get.dart';


class LandingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LandingController>(
          () => LandingController(),
    );
  }
}
