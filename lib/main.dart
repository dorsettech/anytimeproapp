import 'dart:io';

import 'package:anytime_pro/app/constants.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_stripe/flutter_stripe.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:wakelock/wakelock.dart';
import 'app/routes/app_pages.dart';

Future<void> _messageHandler(RemoteMessage message) async {
  print('background message $message');
  print("Handling a background message: ${message.messageId}");
  await Firebase.initializeApp();
}
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() async {
  try{
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    Wakelock.enable();
    HttpOverrides.global = MyHttpOverrides();
    await GetStorage.init();
    /// Required to display a heads up notification
    // await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    //   alert: true,
    //   badge: true,
    //   sound: true,
    // );
    FirebaseMessaging.onBackgroundMessage(_messageHandler);
    await initializeDateFormatting();
    Stripe.publishableKey = TPartyTokens.stripePublishableKey;
    await SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom, SystemUiOverlay.top]);
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    /*SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent,
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.dark));*/

    runApp(MyApp());
  }
  catch(ex){
    print(ex);
  }

}

/*==========================================================================*/
class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _storage = GetStorage();

  FirebaseMessaging _messaging;
  bool status;
  Future<void> notificationPermission() async {
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }
  @override
  void initState() {
    super.initState();
    status = _storage.read('isLogin') ?? false;
    _messaging = FirebaseMessaging.instance;
    notificationPermission();
    _messaging.getToken().then((value) {
      print('FIREBASE TOKEN------>   $value');
      _storage.write('fcm_token', value);

      ///firebase token
    });

    ///notification when appOpen
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message received");
      print(event.notification.body);
      if(event.notification.body == null || event.notification.body == ""){
        Get.defaultDialog(
            title: "",
            middleText: event.notification.title ?? ''
        );
      }else {
        Get.defaultDialog(
            title: event.notification.title ?? '',
            middleText: event.notification.body ?? ''
        );
      }
    });

    ///notification when appClosed [terminated]
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: Size(Get.width, Get.height),
        builder: (BuildContext context, child) => GetMaterialApp(
              theme: customTheme,
              title: "Anytime Pro",
              initialRoute: status ? AppPages.ISLOGGEDIN : AppPages.INITIAL,
              getPages: AppPages.routes,
              debugShowCheckedModeBanner: false,
        )
    );
    // return ScreenUtilInit(
    //   designSize: Size(Get.width, Get.height),
    //   builder: () => GetMaterialApp(
    //     theme: customTheme,
    //     title: "Anytime Pro",
    //     initialRoute: status ? AppPages.ISLOGGEDIN : AppPages.INITIAL,
    //     getPages: AppPages.routes,
    //     debugShowCheckedModeBanner: false,
    //   ),
    // );
  }
}

ThemeData customTheme = ThemeData(
  fontFamily: 'futura',
  visualDensity: VisualDensity.adaptivePlatformDensity,
  scaffoldBackgroundColor: Colors.white,
  appBarTheme: AppBarTheme(
    systemOverlayStyle: SystemUiOverlayStyle.light,
    centerTitle: true,
    titleTextStyle: KTextStyle.f18w6.copyWith(
      color: Colors.white,
      fontWeight: FontWeight.w400,
      letterSpacing: 0.9,
      fontFamily: 'futura',
    ),
    backgroundColor: KColors.lightBlue,
    elevation: 0,
    iconTheme: IconThemeData(color: Colors.white),
  ),
  radioTheme: RadioThemeData(
    fillColor: MaterialStateProperty.all(KColors.lightBlue),
  ),
  inputDecorationTheme: InputDecorationTheme(
    focusColor: KColors.darkBlue,
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: KColors.DARK_GREY,
        width: 1,
      ),
    ),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(
        color: KColors.darkBlue,
        width: 1,
      ),
    ),
  ),
  indicatorColor: KColors.darkBlue,
  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: KColors.darkBlue),
);
